﻿#SingleInstance Force
#NoEnv

;^1::
  SetBatchLines, -1
  mylist:=Listflh(A_ScriptFullPath)
  MsgBox,% mylist[1]
Return

Listflh(scriptpath)
{
  FileRead script, %scriptpath%
  
  identifierRE = ][#@$?\w                  ; Legal characters for AHK identifiers (variables and function names)
  parameterListRE = %identifierRE%,=".\s-  ; Legal characters in function definition parameter list
  lineCommentRE = \s*(?:\s;.*)?$           ; Legal line comment regex
  
  functionNb := 1
  labelNb := 1
  hotkeyNb := 1
  hotstringNb := 1
  state = DEFAULT
  
  time1 := A_TickCount
  Loop Parse, script, `n, `r
    {
      line := A_LoopField
      If RegExMatch(line, "^\s*(?:;.*)?$")            ; Empty line or line with comment, skip it
          Continue
    
      Else If InStr(state, "COMMENT"){                    ; In a block comment
          If RegExMatch(line, "S)^\s*\*/")                  ; End of block comment
              StringReplace state, state, COMMENT         ; Remove state
              ; "*/ function_def()" is legal but quite perverse... I won't support this
    
      }Else If InStr(state,"CONTSECTION") {               ; In a continuation section
          If RegExMatch(line, "^\s*\)")                   ; End of continuation section
              state = DEFAULT
    
      }Else If RegExMatch(line, "^\s*/\*")                ; Start of block comment, to skip
          state = %state% COMMENT
    
      Else If RegExMatch(line, "^\s*\(")                  ; Start of continuation section, to skip
          state = CONTSECTION
    
      Else If RegExMatch(line, "i)^\s*(?P<Name>[^ \s:]+?(?:\s+&\s+[^\s:]+?)?(?:\s+up)?)::", hotkey){  ;Hotkey
          hotkeyList#%hotkeyNb%@name := hotkeyName
          hotkeyList#%hotkeyNb%@line := A_Index
          hotkeyNb++
          state = DEFAULT
    
      }Else If RegExMatch(line, "i)^\s*(?P<Name>:(?:\*0?|\?0?|B0?|C[01]?|K(?:-1|\d+)|O0?|P\d+|R0?|S[IPE]|Z0?)*:.+?)::", hotstring){ ;HotString
          hotstringList#%hotstringNb%@name := hotstringName
          hotstringList#%hotstringNb%@line := A_Index
          hotstringNb++
          state = DEFAULT
      
      }Else If RegExMatch(line, "^\s*(?P<Name>[^\s,```%]+):" . lineCommentRE, label){   ; Label are very tolerant...
          labelList#%labelNb%@name := labelName
          labelList#%labelNb%@line := A_Index
          labelNb++
          state = DEFAULT
    
      }Else If InStr(state,"DEFAULT"){
          If RegExMatch(line, "^\s*(?P<Name>[" . identifierRE . "]+)"         ; Found a function call or a function definition
                            . "\((?P<Parameters>[" . parameterListRE . "]*)"
                            . "(?P<ClosingParen>\)\s*(?P<OpeningBrace>\{)?)?"
                            . lineCommentRE, function){
              state = FUNCTION
              functionList#%functionNb%@name := functionName
              functionList#%functionNb%@parameters := functionParameters
              functionList#%functionNb%@line := A_Index
              If functionClosingParen{        ; With closed parameter list
                  If functionOpeningBrace {     ; Found! This is a function definition
                      functionNb++                ; Validate the finding
                      state = DEFAULT
                  }Else                         ; List of parameters is closed, just search for opening brace
                      state = %state% TOBRACE
              }Else                           ; With open parameter list
                  state = %state% INPARAMS      ; Search for closing parenthesis
            }
    
      }Else If InStr(state,"FUNCTION"){
          If InStr(state, "INPARAMS") {     ; After a function definition or call
              ; Looking for ending parenthesis
              If (RegExMatch(line, "^\s*(?P<Parameters>,[" . parameterListRE . "]+)"
                                 . "(?P<ClosingParen>\)\s*(?P<OpeningBrace>\{)?)?" . lineCommentRE, function) > 0){
                  functionList#%functionNb%@parameters := functionList#%functionNb%@parameters . functionParameters
                  If functionClosingParen {            ; List of parameters is closed
                      If (functionOpeningBrace != ""){   ; Found! This is a function definition
                        functionNb++                     ; Validate the finding
                        state = DEFAULT
                      }Else                              ; Just search for opening brace
                        StringReplace state, state, INPARAMS, TOBRACE ; Remove state
                    }                                    ; Otherwise, we continue
              }Else   
                  ; Incorrect syntax for a parameter list, it was probably just a function call
                  ;??? does this ever happen???
                  state = DEFAULT
          }Else If InStr(state,"TOBRACE"){ ; Looking for opening brace. There can be only empty lines and comments, which are already processed
              If (RegExMatch(line, "^\s*(?:\{)" . lineCommentRE) > 0){  ; Found! This is a function definition
                  functionNb++  ; Validate the finding
                  state = DEFAULT
              }Else  ; Incorrect syntax between closing parenthesis and opening brace,
                  state = DEFAULT     ; it was probably just a function call
          }
      }
    }
  time2 := A_TickCount
  time := Time2 - Time1

  ; Compile the collected information
  functionNb--
  rf =
   list:={}
  Loop %functionNb%{
      pl := RegExReplace(functionList#%A_Index%@parameters, "\s\s*", " ")  ; Replace multiple blank chars with simple space
      rf := rf . "[" . functionList#%A_Index%@line . "] " . functionList#%A_Index%@name . "(" . pl . ")`n"     
    }
  list[1]:=rf
  labelNb--
  rl =

  Loop %labelNb%
      rl := rl . "[" . labelList#%A_Index%@line . "] " . labelList#%A_Index%@name . " "
      rl := RTrim(rl , " ")
      list[2] := rl 
  hotkeyNb--
  rk =
  Loop %hotkeyNb%
      rk :=rk . "[" . hotkeyList#%A_Index%@line . "] " . hotkeyList#%A_Index%@name . " "
      rk  := RTrim(rk , " ")
      list[3] := rk 
  hotstringNb--
  rs =
  Loop %hotstringNb%
      rs := rs . "[" . hotstringList#%A_Index%@line . "] " . hotstringList#%A_Index%@name . " "
      rs  := RTrim(rs , " ")
      list[4] := rs
      
  ;MsgBox, Found in %time% ms in '%scriptName%':
    ;(LTrim
      ;# %functionNb% functions:`n`n%rf%
      ;# %labelNb% labels:`n`n%rl%
      ;# %hotkeyNb% hotkeys:`n`n%rk%
      ;# %hotstringNb% hotstrings:`n`n%rs%
    ;)
    return,list
}