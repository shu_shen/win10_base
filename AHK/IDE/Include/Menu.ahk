﻿; File Menu
;AddMenu("AutoFileMenu", "&New File`tCtrl+N", "NewTab", IconLib, 7)
AddMenu("AutoFileMenu", "从模板新建(&T)`tCtrl+T", "NewFromTemplate", IconLib, 100)
AddMenu("AutoFileMenu", "脚本指令（&D）`tCtrl+P", "ScriptDirectives", IconLib, -2)
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "新建窗口(&G)`tAlt+G", "NewGUI", IconLib, 6)
AddMenu("AutoFileMenu", "导入GUI(G&)...`tCtrl+I", "ImportGUI", IconLib, 40)
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "打开文件(&O)...`tCtrl+O", "Open", IconLib, 9)
AddMenu("AutoFileMenu", "打开 Included 文件...", "ShowIncludesDialog", IconLib, 101)
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "最近打开的文件(&F)",, IconLib, 42)
Menu AutoFileMenu, Disable, 最近打开的文件(&F)


Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "保存(&S)`tCtrl+S", "Save", IconLib, 10)
AddMenu("AutoFileMenu", "保存为(&A)...`tCtrl+Shift+S", "SaveAs")
AddMenu("AutoFileMenu", "保存所有", "SaveAll", IconLib, 125)
Menu AutoFileMenu, Add, 保存一个副本为..., SaveCopy
Menu AutoEncodingMenu, Add, UTF-8, SetSaveEncoding
Menu AutoEncodingMenu, Check, UTF-8
Menu AutoEncodingMenu, Add, UTF-8 不带 BOM, SetSaveEncoding
Menu AutoEncodingMenu, Add, UTF-16 LE, SetSaveEncoding
Menu AutoFileMenu, Add, 编码保存, :AutoEncodingMenu
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "加载会话",, IconLib, 102)
AddMenu("AutoFileMenu", "保存会话...", "SaveSession")
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "关闭文件(&C)`tCtrl+W", "CloseTab", IconLib, 8)
AddMenu("AutoFileMenu", "关闭所有文件", "CloseAllTabs")
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "编译(&p)...", "Compile", IconLib, 13)
Menu AutoFileMenu, Add
AddMenu("AutoFileMenu", "退出(&x)`tAlt+Q", "AutoClose")

; Edit Menu
;Menu AutoEditMenu, Add, &Undo`tCtrl+Z, Undo
Menu AutoEditMenu, Add, 重做(&e)`tCtrl+Y, Redo
Menu AutoEditMenu, Add
Menu AutoEditMenu, Add, 剪贴(&t)`tCtrl+X, Cut
Menu AutoEditMenu, Add, 复制(&C)`tCtrl+C, Copy
Menu AutoEditMenu, Add, 粘贴(&P)`tCtrl+V, Paste
Menu AutoEditMenu, Add, 删除(&D)`tDel, Clear
Menu AutoEditMenu, Add, 全选(&A)`tCtrl+A, SelectAll
Menu AutoEditMenu, Add
Menu AutoEditMenu, Add, 映射一行`tCtrl+Down, DuplicateLine
Menu AutoEditMenu, Add, 本行上移`tCtrl+Shift+Up, MoveLineUp
Menu AutoEditMenu, Add, 本行下移`tCtrl+Shift+Down, MoveLineDown
Menu AutoEditMenu, Add
Menu AutoEditMenu, Add, 自动完成关键字`tCtrl+Enter, M_AutoComplete
Menu AutoEditMenu, Add, 显示函数调用提示`tCtrl+Space, M_ShowCalltip
Menu AutoEditMenu, Add, 插入参数`tCtrl+Insert, M_InsertParameters
Menu AutoEditMenu, Add
Menu AutoEditMenu, Add, 插入日期时间(&I)`tCtrl+D, InsertDateTime
Menu AutoEditMenu, Add
Menu AutoEditMenu, Add, 设为只读(&R), ToggleReadOnly

; Search Menu
;Menu AutoSearchMenu, Add, &Find...`tCtrl+F, ShowSearchDialog
Menu AutoSearchMenu, Add, 查找下一个(&N)`tF3, FindNext
Menu AutoSearchMenu, Add, 查找上一个(&P)`tShift+F3, FindPrev
Menu AutoSearchMenu, Add, 替换(&R)...`tCtrl+H, ShowReplaceDialog
Menu AutoSearchMenu, Add
AddMenu("AutoSearchMenu", "替换(&R)...`tCtrl+H", "ShowReplaceDialog", IconLib, 84)
Menu AutoSearchMenu, Add
AddMenu("AutoSearchMenu", "在文件中查找...`tCtrl+Alt+F", "FindInFiles", IconLib, -23)
Menu AutoSearchMenu, Add
AddMenu("AutoSearchMenu", "到行(&G)...`tCtrl+G", "ShowGoToLineDialog", IconLib, 127)
Menu AutoSearchMenu, Add
AddMenu("AutoSearchMenu", "标记当前行`tF2", "ToggleBookmark", IconLib, 130)
Menu AutoSearchMenu, Add, 标记选中行(&M)`tCtrl+M, MarkSelectedText
AddMenu("AutoSearchMenu", "标记选中文本(&M)`tCtrl+M", "MarkSelectedText", IconLib, 131)
Menu AutoSearchMenu, Add, 转到下一个标记`tCtrl+PgDn, GoToNextMark
Menu AutoSearchMenu, Add, 转到前一个标记`tCtrl+PgUp, GoToPreviousMark
Menu AutoSearchMenu, Add, 清除所有标记(&k)`tAlt+M, ClearAllMarks
Menu AutoSearchMenu, Add
AddMenu("AutoSearchMenu", "转到匹配的大括号`tCtrl+B", "GoToMatchingBrace", IconLib, 132)

; Convert Menu
;Menu AutoConvertMenu, Add, &UPPERCASE`tCtrl+Shift+U, Uppercase
Menu AutoConvertMenu, Add, 小写(&l)`tCtrl+Shift+L, Lowercase
Menu AutoConvertMenu, Add, 首字母大写(&T)`tCtrl+Shift+T, TitleCase
Menu AutoConvertMenu, Add
Menu AutoConvertMenu, Add, 十进制到十六进制(&H)`tCtrl+Shift+H, Dec2Hex
Menu AutoConvertMenu, Add, 十六进制到十进制(&D)`tCtrl+Shift+D, Hex2Dec
Menu AutoConvertMenu, Add
Menu AutoConvertMenu, Add, Win32 常量: 声明, ReplaceConstant
Menu AutoConvertMenu, Add, Win32 常量: SendMessage, ReplaceConstant
Menu AutoConvertMenu, Add, Win32 常量: OnMessage, ReplaceConstant
Menu AutoConvertMenu, Add
Menu AutoConvertMenu, Add, 注释/取消注释`tCtrl+Q, ToggleComment



; Control Menu (menu bar)
;AddMenu("AutoControlMenu", "Change Text...", "ChangeText", IconLib, 14)
Menu AutoControlMenu, Add
AddMenu("AutoControlMenu", "剪切", "CutControl", IconLib, 15)
AddMenu("AutoControlMenu", "复制", "CopyControl", IconLib, 16)
AddMenu("AutoControlMenu", "粘贴", "PasteControl", IconLib, 17)
AddMenu("AutoControlMenu", "删除", "DeleteSelectedControls", IconLib, 18)
Menu AutoControlMenu, Add
AddMenu("AutoControlMenu", "全选(&A)", "SelectAllControls", IconLib, 41)
AddMenu("AutoControlMenu", "取消全选(&N)", "DestroySelection", IconLib, -134)
Menu AutoControlMenu, Add
AddMenu("AutoControlMenu", "位置和大小...", "ShowAdjustPositionDialog", IconLib, 75)
AddMenu("AutoControlMenu", "字体...", "ShowFontDialog", IconLib, 20)
AddMenu("AutoControlMenu", "风格...", "ShowStylesDialog", IconLib, 19)
AddMenu("AutoControlMenu", "选项...", "ShowOptionsTab", IconLib, 91)
Menu AutoControlMenu, Add
AddMenu("AutoControlMenu", "属性", "ShowProperties", IconLib, 25)

; Layout Menu
;AddMenu("AutoLayoutMenu", "Align &Lefts", "AlignLefts", IconLib, 26)
AddMenu("AutoLayoutMenu", "右对齐(&R)", "AlignRights", IconLib, 27)
AddMenu("AutoLayoutMenu", "顶对齐(&T)", "AlignTops", IconLib, 28)
AddMenu("AutoLayoutMenu", "底对齐(&B)", "AlignBottoms", IconLib, 29)
Menu AutoLayoutMenu, Add
AddMenu("AutoLayoutMenu", "水平居中(&C)", "CenterHorizontally", IconLib, 30)
AddMenu("AutoLayoutMenu", "垂直居中(&V)", "CenterVertically", IconLib, 31)
Menu AutoLayoutMenu, Add
AddMenu("AutoLayoutMenu", "水平等距(&Z)", "HorizontallySpace", IconLib, 33)
AddMenu("AutoLayoutMenu", "垂直等距(&E)", "VerticallySpace", IconLib, 32)
Menu AutoLayoutMenu, Add
AddMenu("AutoLayoutMenu", "等宽(&W)", "MakeSameWidth", IconLib, 34)
AddMenu("AutoLayoutMenu", "等高(&H)", "MakeSameHeight", IconLib, 35)
AddMenu("AutoLayoutMenu", "大小相同(&S)", "MakeSameSize", IconLib, 36)
Menu AutoLayoutMenu, Add
AddMenu("AutoLayoutMenu", "水平拉伸", "StretchControl", IconLib, 97)
AddMenu("AutoLayoutMenu", "垂直拉伸", "StretchControl", IconLib, 98)

; Window Menu (menu bar)
;AddMenu("AutoWindowMenu", "Change &Title...", "ChangeTitle", IconLib, 37)
Menu AutoWindowMenu, Add
AddMenu("AutoWindowMenu", "根据内容调整窗口大小", "AutoSizeWindow", IconLib, 99)
;~ AddMenu("AutoWindowMenu", "字体...", "ShowFontDialog", IconLib, 20)
AddMenu("AutoWindowMenu", "改变控件字体...", "WinShowFontDialog", IconLib, -20)
AddMenu("AutoWindowMenu", "风格...", "ShowStylesDialog", IconLib, 19)
AddMenu("AutoWindowMenu", "属性(&P)...", "ShowWindowProperties", IconLib, 25)
Menu AutoWindowMenu, Add
AddMenu("AutoWindowMenu", "显示/隐藏前一个窗口(&S)`tF11", "ShowChildWindow", IconLib, 38)
AddMenu("AutoWindowMenu", "重绘(&R)", "RedrawWindow", IconLib, 39)
Menu AutoWindowMenu, Add
If (SysTrayIcon) {
    Menu AutoWindowMenu, Add
    AddMenu("AutoWindowMenu", "从脚本重建(&C)", "RecreateFromScript", IconLib, 40)
}
;~ AddMenu("AutoWindowMenu", "&Properties", "ShowWindowProperties", IconLib, -25)

; View Menu
; Menu AutoViewMenu, Add, &Design Mode, SwitchToDesignMode, Radio
Menu AutoViewMenu, Add, 编辑模式(&E), SwitchToEditorMode, Radio
Menu AutoViewMenu, Add
Menu AutoViewTabBarMenu, Add, 顶部, SetTabBarPos, Radio
Menu AutoViewTabBarMenu, Add, 底部, SetTabBarPos, Radio
Menu AutoViewTabBarMenu, Add
Menu AutoViewTabBarMenu, Add, 标准, SetTabBarStyle, Radio
Menu AutoViewTabBarMenu, Add, 按钮, SetTabBarStyle, Radio
Menu AutoViewMenu, Add, 选项卡, :AutoViewTabBarMenu
Menu AutoViewMenu, Add
Menu AutoViewMenu, Add, 行号(&L), ToggleLineNumbers
Menu AutoViewMenu, Add, 符号留白, ToggleSymbolMargin
Menu AutoViewMenu, Add, 折叠留白(&F), ToggleCodeFolding
Menu AutoViewMenu, Add
Menu AutoViewMenu, Add, 全部折叠, CollapseFolds
Menu AutoViewMenu, Add, 全部展开, ExpandFolds
Menu AutoViewMenu, Add
AddMenu("AutoViewMenu", "自动换行(&W)", "ToggleWordWrap")
Menu AutoViewMenu, Add,显示白空格(&S), ToggleWhiteSpaces
Menu AutoViewMenu, Add
AddMenu("AutoViewMenu", "语法高亮(&H)", "ToggleSyntaxHighlighting")
AddMenu("AutoViewMenu", "高亮激活行(&A)", "ToggleHighlightActiveLine")
AddMenu("AutoViewMenu", "高亮相同文本(&X)", "ToggleHighlightIdenticalText")
Menu AutoViewMenu, Add
Menu AutoViewMenu, Add, 切换暗主题, ToggleTheme
Menu AutoViewMenu, Add
AddMenu("AutoViewZoomMenu", "放大`tCtrl+Num +", "ZoomIn", IconLib, 88)
AddMenu("AutoViewZoomMenu", "缩小`tCtrl+Num -", "ZoomOut", IconLib, 89)
Menu AutoViewZoomMenu, Add, 重置缩放`tCtrl+Num 0, ResetZoom
Menu AutoViewMenu, Add, 缩放, :AutoViewZoomMenu
Menu AutoViewMenu, Add
AddMenu("AutoViewMenu", "改变编辑器字体...", "ChangeEditorFont", IconLib, 20)
; Lexer Menu
;Menu AutoLexerMenu, Add, AutoHotkey, SetLexer
Menu AutoLexerMenu, Add, 纯文本, SetLexer
Menu AutoLexerMenu, Check, AutoHotkey




; Options Menu

AddMenu("AutoOptionsMenu", "代码工具提示(&T)", "ToggleCalltips")
AddMenu("AutoOptionsMenu", "自动补全括号(&B)", "ToggleAutoBrackets")
AddMenu("AutoOptionsMenu", "缩进设置...", "ShowIndentationDialog")
Menu AutoOptionsMenu, Add, 插入符号设置..., ShowCaretDialog
Menu AutoOptionsMenu, Add
Menu AutoOptionsGuiMenu, Add, 显示网格(&G), ToggleGrid
Menu AutoOptionsGuiMenu, Add, 吸附到网格(&N), ToggleSnapToGrid
Menu AutoOptionsMenu, Add, 界面设计器, :AutoOptionsGuiMenu
Menu AutoOptionsMenu, Add

;Menu AutoOptionsMenu, Add, Remember Session, MenuHandler
;Menu AutoOptionsMenu, Add
Menu AutoOptionsMenu, Add, 调试设置..., SetDebugPort
Menu AutoOptionsMenu, Add
Menu AutoOptionsMenu, Add, 备份设置..., ShowBackupDialog
Menu AutoOptionsMenu, Add
Menu AutoOptionsMenu, Add, 记住会话, ToggleRememberSession
Menu AutoOptionsMenu, Add
;Menu AutoOptionsMenu, Add, Ask to Save on Close Tab, ToggleAskToSaveOnCloseTab ; ?
Menu AutoOptionsMenu, Add, 退出时提示保存, ToggleAskToSaveOnExit
If (SysTrayIcon) {
    Menu AutoOptionsMenu, Add
    Menu AutoOptionsMenu, Add, 保存设置, SaveSettings
}
;Menu AutoOptionsMenu, Add
;AddMenu("AutoOptionsMenu", "&Settings...", "ShowSettings", IconLib, 43)
; Run Menu
;AddMenu("AutoRunMenu", "&AutoHotkey 32-bit`tF9", "RunScript", IconLib, 12)
;AddMenu("AutoRunMenu", "AutoHotkey 64-&bit`tShift+F9", "RunScript", IconLib, 92)
AddMenu("AutoRunMenu", "运行选中的文本(&S)`tCtrl+F9", "RunSelectedText", IconLib, 95)
AddMenu("AutoRunMenu", "选择可执行文件`tAlt+F9", "BrowseForAltRun", IconLib, -94)

Menu AutoRunMenu, Add
Menu AutoRunMenu, Add, 捕获标准错误输出(&E), ToggleCaptureStdErr
AddMenu("AutoRunMenu", "命令行参数(&P)...", "ShowParamsDlg", IconLib, 91)
Menu AutoRunMenu, Add
Menu AutoRunMenu, Add, 资源管理器上下文菜单..., ShowShellMenuDlg
Menu AutoRunMenu, Add
AddMenu("AutoRunMenu", "运行外部应用(&E)...", "RunFileDlg", "shell32.dll", 25)

;调试

AddMenu("AutoDebugMenu", "开始调试`tF5", "DebugRun", IconLib, 104)
AddMenu("AutoDebugMenu", "暂停`tBreak", "DebugBreak", IconLib, 105)
AddMenu("AutoDebugMenu", "停止调试`tF8", "DebugStop", IconLib, 106)
Menu AutoDebugMenu, Add
AddMenu("AutoDebugMenu", "附加调试器...", "ShowAttachDialog", IconLib, 107)
Menu AutoDebugMenu, Add
AddMenu("AutoDebugMenu", "步入`tF6", "StepInto", IconLib, 109)
AddMenu("AutoDebugMenu", "单步执行`tF7", "StepOver", IconLib, 110)
AddMenu("AutoDebugMenu", "步出`tShift+F6", "StepOut", IconLib, 111)
;AddMenu("AutoRunMenu", "Run to &Cursor`tCtrl+F5", "DebugRunToCursor", IconLib, 96)
Menu AutoDebugMenu, Add
AddMenu("AutoDebugMenu", "设置断点`tF4", "ToggleBreakpoint", IconLib, 112)
AddMenu("AutoDebugMenu", "删除所有断点", "DeleteBreakpoints", IconLib, 113)
Menu AutoDebugMenu, Add
AddMenu("AutoDebugMenu", "变量", "ShowVarList", IconLib, 115)
AddMenu("AutoDebugMenu", "调用栈", "ShowCallStack", IconLib, 123)
AddMenu("AutoDebugMenu", "错误流", "ShowStderr", IconLib, 124)
; Tools Menu
;AddMenu("AutoToolsMenu", "&Window Cloning Tool", "ShowCloneDialog", IconLib, 44)
Menu AutoToolsMenu, Add

; Help Menu
;AddMenu("AutoHelpMenu", "AutoHotkey &Help File`tF1", "HelpMenuHandler", IconLib, 78)
AddMenu("AutoHelpMenu", "快捷键列表", "ShowKeyboardShortcuts", A_ScriptDir . "\Icons\Keyboard.ico")
Menu AutoHelpMenu, Add ; Separator
AddMenu("AutoHelpMenu", "关于(&A)", "ShowAbout", IconLib, 80)

; Preview Window: Insert: Context Menu
;~ AddMenu("InsertMenu", "Button", "InsertControl", IconLib, 47)
;~ AddMenu("InsertMenu", "CheckBox", "InsertControl", IconLib, 48)
;~ AddMenu("InsertMenu", "ComboBox", "InsertControl", IconLib, 49)
;~ AddMenu("InsertMenu", "Date Time Picker", "InsertControl", IconLib, 51)
;~ AddMenu("InsertMenu", "DropDownList", "InsertControl", IconLib, 50)
;~ AddMenu("InsertMenu", "Edit Box", "InsertControl", IconLib, 52)
;~ AddMenu("InsertMenu", "GroupBox", "InsertControl", IconLib, 53)
;~ AddMenu("InsertMenu", "Hotkey Box", "InsertControl", IconLib, 54)
;~ AddMenu("InsertMenu", "Link", "InsertControl", IconLib, 55)
;~ AddMenu("InsertMenu", "ListBox", "InsertControl", IconLib, 56)
;~ AddMenu("InsertMenu", "ListView", "InsertControl", IconLib, 57)
;~ AddMenu("InsertMenu", "Month Calendar", "InsertControl", IconLib, 59)
;~ AddMenu("InsertMenu", "Picture", "InsertControl", IconLib, 60)
;~ AddMenu("InsertMenu", "Progress Bar", "InsertControl", IconLib, 61)
;~ AddMenu("InsertMenu", "Radio Button", "InsertControl", IconLib, 62)
;~ AddMenu("InsertMenu", "Separator", "InsertControl", IconLib, 63)
;~ AddMenu("InsertMenu", "Slider", "InsertControl", IconLib, 64)
;~ AddMenu("InsertMenu", "Tab", "InsertControl", IconLib, 66)
;~ AddMenu("InsertMenu", "Text", "InsertControl", IconLib, 67)
;~ AddMenu("InsertMenu", "TreeView", "InsertControl", IconLib, 68)
;~ AddMenu("InsertMenu", "UpDown", "InsertControl", IconLib, 70)

AddMenu("InsertMenu", "按钮", "InsertControl", IconLib, 47)
AddMenu("InsertMenu", "复选框", "InsertControl", IconLib, 48)
AddMenu("InsertMenu", "组合框", "InsertControl", IconLib, 49)
AddMenu("InsertMenu", "日期时间选择器", "InsertControl", IconLib, 51)
AddMenu("InsertMenu", "下拉列表", "InsertControl", IconLib, 50)
AddMenu("InsertMenu", "编辑框", "InsertControl", IconLib, 52)
AddMenu("InsertMenu", "分组框", "InsertControl", IconLib, 53)
AddMenu("InsertMenu", "热键", "InsertControl", IconLib, 54)
AddMenu("InsertMenu", "链接", "InsertControl", IconLib, 55)
AddMenu("InsertMenu", "列表框", "InsertControl", IconLib, 56)
AddMenu("InsertMenu", "列表视图", "InsertControl", IconLib, 57)
AddMenu("InsertMenu", "分隔符", "InsertControl", IconLib, 63)
AddMenu("InsertMenu", "月历", "InsertControl", IconLib, 59)
AddMenu("InsertMenu", "图片", "InsertControl", IconLib, 60)
AddMenu("InsertMenu", "进度条", "InsertControl", IconLib, 61)
AddMenu("InsertMenu", "单选按钮", "InsertControl", IconLib, 62)
AddMenu("InsertMenu", "滑块", "InsertControl", IconLib, 64)
AddMenu("InsertMenu", "标签", "InsertControl", IconLib, 66)
AddMenu("InsertMenu", "文本框", "InsertControl", IconLib, 67)
AddMenu("InsertMenu", "树视图", "InsertControl", IconLib, 68)
AddMenu("InsertMenu", "上下", "InsertControl", IconLib, 70)

; Preview Window: Context Menu
AddMenu("WindowContextMenu", "添加控件", ":InsertMenu", IconLib, 44)
AddMenu("WindowContextMenu", "粘贴", "PasteControl", IconLib, 17)
AddMenu("WindowContextMenu")
AddMenu("WindowContextMenu", "修改标题...", "ChangeTitle", IconLib, 37)
AddMenu("WindowContextMenu")
AddMenu("WindowContextMenu", "根据控件内容调整窗口", "AutoSizeWindow", IconLib, 99)
AddMenu("WindowContextMenu", "字体...", "ShowFontDialog", IconLib, 20)
AddMenu("WindowContextMenu", "风格...", "ShowStylesDialog", IconLib, 19)
AddMenu("WindowContextMenu", "选项...", "ShowWindowOptions", IconLib, 91)
AddMenu("WindowContextMenu")
AddMenu("WindowContextMenu", "网格", "ToggleGrid", IconLib, 72)
AddMenu("WindowContextMenu", "重绘", "RedrawWindow", IconLib, 39)
AddMenu("WindowContextMenu")
AddMenu("WindowContextMenu", "属性", "ShowProperties", IconLib, 25)
Menu WindowContextMenu, Color, 0xFAFAFA

;~ ; Child Window Context Menu(子窗口上下文菜单)
;~ AppendMenu("WindowContextMenu", "插入", ":InsertMenu", IconLib, 45)
;~ AppendMenu("WindowContextMenu", "粘贴", "Paste", IconLib, 17)
;~ AppendMenu("WindowContextMenu")
;~ AppendMenu("WindowContextMenu", "修改标题...", "ChangeTitle", IconLib, 37)
;~ AppendMenu("WindowContextMenu", "选项...", "MenuHandler", IconLib, 91)
;~ AppendMenu("WindowContextMenu", "风格...", "ShowStylesDialog", IconLib, 19)
;~ AppendMenu("WindowContextMenu", "修改字体...", "ShowFontDialog", IconLib, 20)
;~ AppendMenu("WindowContextMenu")
;~ AppendMenu("WindowContextMenu", "网格线开关", "ToggleGrid", IconLib, 72)
;~ AppendMenu("WindowContextMenu", "重绘", "RedrawWindow", IconLib, 39)
;~ AppendMenu("WindowContextMenu", "重建", "RecreateFromSource", IconLib, 40)
;~ AppendMenu("WindowContextMenu")
;~ AppendMenu("WindowContextMenu", "属性", "ShowProperties", IconLib, 25)
;~ Menu WindowContextMenu, Color, White

;~ ; Control Context Menu
;~ AddMenu("ControlContextMenu", "Change &Text...", "ChangeText", IconLib, 14)
;~ Menu ControlContextMenu, Add
;~ AddMenu("ControlContextMenu", "C&ut", "CutControl", IconLib, 15)
;~ AddMenu("ControlContextMenu", "&Copy", "CopyControl", IconLib, 16)
;~ AddMenu("ControlContextMenu", "&Paste", "PasteControl", IconLib, 17)
;~ AddMenu("ControlContextMenu", "&Delete", "DeleteSelectedControls", IconLib, 18)
;~ Menu ControlContextMenu, Add
;~ AddMenu("ControlContextMenu", "Position a&nd Size...", "ShowAdjustPositionDialog", IconLib, 75)
;~ AddMenu("ControlContextMenu", "&Font...", "ShowFontDialog", IconLib, 20)
;~ AddMenu("ControlContextMenu", "&Styles...", "ShowStylesDialog", IconLib, 19)
;~ AddMenu("ControlOptionsMenu", "None", "MenuHandler")
;~ AddMenu("ControlContextMenu", "&Options", ":ControlOptionsMenu", IconLib, 91)
;~ Menu ControlContextMenu, Add
;~ AddMenu("ControlContextMenu", "Prop&erties", "ShowProperties", IconLib, 25)

; Control Context Menu(控件上下文菜单)
AddMenu("ControlContextMenu", "修改文本...", "ChangeText", IconLib, 14)
Menu ControlContextMenu, Add
AddMenu("ControlContextMenu", "剪切", "CutControl", IconLib, 15)
AddMenu("ControlContextMenu", "复制", "CopyControl", IconLib, 16)
AddMenu("ControlContextMenu", "粘贴",  "PasteControl", IconLib, 17)
AddMenu("ControlContextMenu", "删除", "DeleteSelectedControls", IconLib, 18)
Menu ControlContextMenu, Add
AddMenu("ControlContextMenu", "矫正位置...", "ShowAdjustPositionDialog", IconLib, 75)
AddMenu("ControlContextMenu", "字体...", "ShowFontDialog", IconLib, 20)
AddMenu("ControlContextMenu", "风格...", "ShowStylesDialog", IconLib, 19)
AddMenu("ControlOptionsMenu", "None", "MenuHandler")
AddMenu("ControlContextMenu", "选项", ":ControlOptionsMenu", IconLib, 91)
Menu ControlContextMenu, Add
AddMenu("ControlContextMenu", "属性", "ShowProperties", IconLib, 25)

; Tab Context Menu
AddMenu("TabContextMenu", "关闭标签", "CloseTabN", IconLib, 8)
Menu TabContextMenu, Add
AddMenu("TabContextMenu", "重置到新选项卡", "DuplicateTab", IconLib, 7)
AddMenu("TabContextMenu", "打开脚本所在文件夹", "OpenFolder", IconLib, 9)
AddMenu("TabContextMenu", "复制路径到剪贴板", "CopyFilePath", IconLib, 11)
AddMenu("TabContextMenu", "新窗口中重新打开", "OpenNewInstance", IconLib, 1)
Menu TabContextMenu, Add
AddMenu("TabContextMenu", "文件属性", "ShowFileProperties", IconLib, 25)

LoadHelpMenu() {
    g_HelpMenuXMLObj := LoadXML(A_ScriptDir . "\Include\HelpMenu.xml")
    Nodes := g_HelpMenuXMLObj.selectSingleNode("HelpMenu").childNodes

    StartPos := 2
    For Node in Nodes {
        Index := StartPos + A_Index

        If (Node.hasChildNodes()) {
            SubMenu := True
            MenuName := "AutoHelpMenu" . Index

            ChildNodes := Node.childNodes
            For ChildNode in ChildNodes {
                MenuItemText := ChildNode.getAttribute("name")
                Icon := GetHelpMenuItemIcon(ChildNode)
                AddMenu(MenuName, MenuItemText, "HelpMenuHandler", Icon[1], Icon[2])
            }
        } Else {
            SubMenu := False
        }

        MenuItemText := Node.getAttribute("name")
        Menu AutoHelpMenu, Insert, %Index%&, %MenuItemText%, % (SubMenu) ? ":" . MenuName : "HelpMenuHandler"

        If (SubMenu) {
            Menu AutoHelpMenu, Icon, %MenuItemText%, %IconLib%, 9
        } Else {
            Icon := GetHelpMenuItemIcon(Node)
            Menu AutoHelpMenu, Icon, %MenuItemText%, % Icon[1], % Icon[2]
        }
    }

    Index++
    Menu AutoHelpMenu, Insert, %Index%&
}

GetHelpMenuItemIcon(Node) {
    URL := Node.getAttribute("url")

    If (SubStr(URL, 1, 1) == "/") {
        Icon := IconLib
        IconIndex := 79
    } Else {
        Icon := IconLib
        IconIndex := 133
    }

    Return [Icon, IconIndex]
}
ShowKeyboardShortcuts() 
{
    Run %A_ScriptDir%\Include\Keyboard.ahk    
}

;**************web库****************

;~ https://diymediahome.org/windows-icons-reference-list-with-details-lo/0cations-images/
;******************************
;***********点*******************
;******************************
Menu,WebScraping_Pointer,Add,创建 IE 对象, WebScraping_Pointer_Create_IE
Menu,WebScraping_Pointer,Add,获取 IE 窗口的切入点, WebScraping_Pointer_wbGet
Menu,WebScraping_Pointer,Add,获取 IE 窗口的切入点***函数***, WebScraping_Pointer_wbGet_FUNCTION
Menu,WebScraping_Pointer,Add,通过标题和链接获取 IE 窗口的切入点, WebScraping_Pointer_Get_IE_Title_URL
Menu,WebScraping_Pointer,Add,通过标题和链接获取 IE 窗口的切入点 ***函数***, WebScraping_Pointer_Get_IE_Title_URL_FUNCTION


;~ Menu,WebScraping_Pointer,Icon,Create IE Object,         %A_WinDir%\system32\wpdshext.dll,25 ;not in Win10
Menu,WebScraping_Pointer,Icon,获取 IE 窗口的切入点, %A_WinDir%\system32\shell32.dll,92

Menu,Webscraping, Add, 点, :WebScraping_Pointer ;***********pointer*******************
Menu,Webscraping,Icon,点, %A_WinDir%\system32\imageres.dll, 78

;******************************
;***********Page*******************
;******************************
Menu,Webscraping_Page,Add,拾取当前链接, WebScraping_Page_Location_URL
Menu,Webscraping_Page,Add,通过ID滚动到页面某一元素位置, WebScraping_Page_Scroll_to_Element_on_Page
Menu,Webscraping_Page,Add,获取当前名字 / 标题, WebScraping_Page_Location_Name
Menu,Webscraping_Page,Add,导航到某一页面, WebScraping_Page_Navigate
Menu,Webscraping_Page,Add,等待页面加载, WebScraping_Page_Wait_Page_Load
Menu,Webscraping_Page,Add,等待IE加载(函数), WebScraping_Page_Wait_IE_Load
Menu,WebScraping_Page,Add,等待某一元素呈现,WebScraping_Page_Wait_for_Element
Menu,Webscraping_Page,Add,刷新 / 重载, WebScraping_Page_Reload
Menu,Webscraping_Page,Add,历史记录长度,WebScraping_Page_History_Count
Menu,Webscraping_Page,Add,导航至前一页,WebScraping_Page_Backward
Menu,Webscraping_Page,Add,导航至后一页,WebScraping_Page_Forward
Menu,Webscraping_Page,Add,,
Menu,Webscraping_Page,Add,循环导航实例,WebScraping_Page_Loop


Menu,Webscraping,Add ,页面 / 导航, :Webscraping_Page ;***********Page*******************
Menu,Webscraping,Icon,页面 / 导航,     %A_WinDir%\system32\compstui.dll,55

Menu,Webscraping_Page,Icon,等待页面加载, %A_WinDir%\system32\shell32.dll,200
Menu,Webscraping_Page,Icon,等待页面加载, %A_WinDir%\system32\shell32.dll,240
Menu,Webscraping_Page,Icon,等待某一元素呈现, %A_WinDir%\system32\shell32.dll,241

Menu,Webscraping_Page,Icon,导航到某一页面, %A_WinDir%\system32\comres.dll,5
Menu,Webscraping_Page,Icon,刷新 / 重载, %A_WinDir%\system32\mmcndmgr.dll,47
;~ Menu,Webscraping_Page,Icon,Navigate Backward in History,%A_WinDir%\system32\wmploc.dll,202 ;not in Win 10
;~ Menu,Webscraping_Page,Icon,Navigate Forward in History ,%A_WinDir%\system32\wmploc.dll,201 ;Not in Win 10
Menu,WebScraping,Add, ;***********Spacer*******************

;******************************
;***********GET*******************
;******************************
;~  Menu,WebScraping_Get,Add,Get ID,      WebScraping_Get_ID_Unique
Menu,WebScraping_Get,Add,获取指定ID的值, WebScraping_Get_ID_Unique_Dashes
Menu,WebScraping_Get,Add,获取指定名字的某项值,     WebScraping_Get_Name_Array
Menu,WebScraping_Get,Add,获取类标签的某项值,WebScraping_Get_ClassName
Menu,WebScraping_Get,Add,获取导航标签的某项值,  WebScraping_Get_TagName
Menu,WebScraping_Get,Add,获取下拉列表某项值, WebScraping_Get_DropDowns
Menu,WebScraping_Get,Add,获取多选框的值,    WebScraping_Get_CheckBoxes ;radio buttons
Menu,WebScraping_Get,Add ;***********Spacer*******************
Menu,WebScraping_Get,Add,获取属性,     WebScraping_Get_Attributes ;
Menu,WebScraping_Get,Add,获取页面内所有文本, WebScraping_Get_All_Text_on_Page
Menu,WebScraping_Get,Add,获取页面的完整链接, WebScraping_Get_All_HTML_on_Page
Menu,WebScraping_Get,Add,获取页内所有链接,WebScraping_Get_Links_on_Page

;~  Menu,WebScraping_Get,Icon,Get ID,              %A_WinDir%\system32\compstui.dll,32
Menu,WebScraping_Get,Icon,获取指定ID的值,              %A_WinDir%\system32\compstui.dll,32
Menu,WebScraping_Get,Icon,获取指定名字的某项值,      %A_WinDir%\system32\compstui.dll,33
Menu,WebScraping_Get,Icon,获取下拉列表某项值,  %A_WinDir%\system32\imageres.dll,157
Menu,WebScraping_Get,Icon,获取多选框的值,     %A_WinDir%\system32\compstui.dll,5
Menu,WebScraping_Get,Icon,获取页面内所有文本,%A_WinDir%\system32\imageres.dll,98

Menu,WebScraping_Get_Misc,Add,Get -loop over examples,WebScraping_Get_Misc_Loop
Menu,WebScraping_Get_Misc,Add,Get -loop over Table ,  WebScraping_Get_Misc_Loop_over_Table
Menu,WebScraping_Get_Misc,Add,Get Parameter after ?,  WebScraping_Get_Misc_Search
Menu,WebScraping_Get_Misc,Add,Get Path,               WebScraping_Get_Misc_Path
Menu,WebScraping_Get_Misc,Add,Get Host,               WebScraping_Get_Misc_Host
Menu,WebScraping_Get_Misc,Add,Get Hash,               WebScraping_Get_Misc_Hash
Menu,WebScraping_Get_Misc,Add,Get User Agent,         WebScraping_Get_Misc_UserAgent
Menu,WebScraping_Get_Misc,Add,Get Protocol,           WebScraping_Get_Misc_Protocol
Menu,Webscraping_Get,Add, 其他 , :WebScraping_Get_Misc ;***********Miscellaneous*******************

Menu,Webscraping,Add, 获取, :Webscraping_Get ;***********Get values*******************
Menu,WebScraping,Icon,获取,     %A_WinDir%\system32\mmcndmgr.dll,49

;******************************
;***********SET*******************
;******************************
Menu,WebScraping_Set,Add,设置指定ID的值,             WebScraping_Set_ID_Unique_Dashes
Menu,WebScraping_Set,Add,设置指定名字的某项值,     WebScraping_Set_Name_Array
Menu,WebScraping_Set,Add,设置类标签的某项值,WebScraping_Set_ClassName_Array
Menu,WebScraping_Set,Add,设置导航标签的某项值,  WebScraping_Set_TagName
Menu,WebScraping_Set,Add,设置下拉列表某项值, WebScraping_Set_DropDowns
Menu,WebScraping_Set,Add,设置多选框的值,    WebScraping_Set_CheckBoxes
Menu,WebScraping_Set,Add,
Menu,WebScraping_Set,Add,设置属性,     WebScraping_Set_Attributes ;
Menu,WebScraping_Set_Misc,Add,设置用户代理,    WebScraping_Set_Misc_UserAgent

Menu,WebScraping_Set,Icon,设置指定ID的值,              %A_WinDir%\system32\compstui.dll,32
Menu,WebScraping_Set,Icon,设置指定名字的某项值,      %A_WinDir%\system32\compstui.dll,33
Menu,WebScraping_Set,Icon,设置下拉列表某项值,  %A_WinDir%\system32\imageres.dll,157
Menu,WebScraping_Set,Icon,设置多选框的值,     %A_WinDir%\system32\compstui.dll,5

Menu,WebScraping_Set,Add,其他, :WebScraping_Set_Misc ;***********Set Miscellaneous*******************

Menu,WebScraping,Add,设值, :WebScraping_Set ;***********Set values*******************
Menu,WebScraping,Icon,设值,     %A_WinDir%\system32\mmcndmgr.dll,58

;******************************
;***********Click*******************
;******************************
Menu,WebScraping_Trigger_No_EventListener,Add,通过ID点击,          	WebScraping_Click_BY_ID
Menu,WebScraping_Trigger_No_EventListener,Add,通过name点击,          	WebScraping_Click_BY_name
Menu,WebScraping_Trigger_No_EventListener,Add,通过classname点击,          	WebScraping_Click_BY_classname
Menu,WebScraping_Trigger_No_EventListener,Add,Click Link,          	WebScraping_Click_Click
Menu,WebScraping_Trigger_No_EventListener,Add,Click text on page,  	WebScraping_Click_Click_Link_Specific_Text
Menu,WebScraping_Trigger_No_EventListener,Add,Fire Event-Change,   	WebScraping_Click_Fire_Event_Change
Menu,WebScraping_Trigger_No_EventListener,Add,Fire Event-Click,   	WebScraping_Click_Fire_Event_Click
Menu,WebScraping_Trigger_No_EventListener,Add,Focus (place cursor),	WebScraping_Click_Focus

Menu,WebScraping_Trigger,Add,No Event Listeners,:WebScraping_Trigger_No_EventListener
Menu,WebScraping_Trigger,Add
;~ Menu,WebScraping_Trigger,Icon,Click Link,           %A_WinDir%\system32\wpdshext.dll,25 ;Not in Win 10
Menu,WebScraping_Trigger_No_EventListener,Icon,Click text on page,   %A_WinDir%\system32\wpdshext.dll,22
Menu,WebScraping_Trigger_No_EventListener,Icon,Focus (place cursor), %A_WinDir%\system32\wmploc.dll,28

;********************Mouse Events***********************************
Menu,WebScraping_Trigger_Mouse,Add,Mouse Mouse Over,  		WebScraping_Trigger_Mouse_MouseOver
Menu,WebScraping_Trigger_Mouse,Add,Mouse Down,      	 	WebScraping_Trigger_Mouse_Down
Menu,WebScraping_Trigger_Mouse,Add,Mouse Up,         		WebScraping_Trigger_Mouse_Up
Menu,WebScraping_Trigger_Mouse,Add,Mouse Click,      		WebScraping_Trigger_Mouse_Click
Menu,WebScraping_Trigger_Mouse,Add,Mouse Double Click,  		WebScraping_Trigger_Mouse_DoubleClick
Menu,WebScraping_Trigger,Add,Mouse Events,:WebScraping_Trigger_Mouse
Menu,WebScraping_Trigger,Add

;********************Keyboard Events***********************************

Menu,WebScraping_Trigger_Keyboard,Add, Keyboard Up, 		WebScraping_Trigger_Keyboard_Up
Menu,WebScraping_Trigger_Keyboard,Add, Keyboard Down, 		WebScraping_Trigger_Keyboard_Down
Menu,WebScraping_Trigger_Keyboard,Add, Key Press, 		WebScraping_Trigger_KeyboardPress
Menu,WebScraping_Trigger,Add,Keyboard Events,:WebScraping_Trigger_Keyboard
Menu,WebScraping_Trigger,Add

;********************Miscellaneous Events***********************************

;~Menu,WebScraping_Trigger_Misc,Add,Cut, 					WebScraping_Trigger_Misc_Cut
;~Menu,WebScraping_Trigger_Misc,Add,Copy,					WebScraping_Trigger_Misc_Copy
;~Menu,WebScraping_Trigger_Misc,Add,Paste,				WebScraping_Trigger_Misc_Paste
Menu,WebScraping_Trigger_Misc,Add,Input,				WebScraping_Trigger_Misc_Input
Menu,WebScraping_Trigger_Misc,Add,Change,				WebScraping_Trigger_Misc_Change
Menu,WebScraping_Trigger_Misc,Add,Focus,				WebScraping_Trigger_Misc_Focus
Menu,WebScraping_Trigger_Misc,Add,Focusin,				WebScraping_Trigger_Misc_Focus_In
Menu,WebScraping_Trigger,Add,Miscellaneous Events,:WebScraping_Trigger_Misc



Menu,WebScraping,Add,事件, :WebScraping_Trigger ;***********Click *******************
Menu,WebScraping,Icon,事件,     %A_WinDir%\system32\mmcndmgr.dll,51

Menu,WebScraping,Add, ;***********Spacer*******************

;******************************
;***********Advanced*******************
;******************************
;***********Frames*******************
;******************************
;***********Single level frames*******************
Menu,Frames_One_Level,Add, Get Length of Frames, Advanced_Frame_One_level_Length
Menu,Frames_One_Level,Add, Frame has Name or ID- Get URL,  Advanced_Frame_One_level_Name_or_ID_URL
Menu,Frames_One_Level,Add, Frame has Name or ID- Get Text, Advanced_Frame_One_level_Name_or_ID_Get_Text
Menu,Frames_One_Level,Add,
Menu,Frames_One_Level,Add, Frame NO Name or ID- Get URL,  Advanced_Frame_One_level_NO_Name_or_ID_URL
Menu,Frames_One_Level,Add, Frame NO Name or ID- Get Text, Advanced_Frame_One_level_NO_Name_or_ID_Get_Text
Menu,Frames_One_Level,Add, Frame NO Name or ID- 'Access Denied' issue, Advanced_Frame_One_level_NO_Name_or_ID_Access_Denied
Menu,WebScraping_Advanced,Add, Frames-One Level, :Frames_One_Level ;***********Advanced*******************

;***********multi level frames*******************
Menu,Frames_Multi_Level,Add, Frame has Name or ID- Get URL, Advanced_Frame_Multi_level_Name_or_ID_URL
Menu,Frames_Multi_Level,Add, Frame has Name or ID- Get Text, Advanced_Frame_Multi_level_Name_or_ID_Get_Text
Menu,Frames_Multi_Level,Add,
Menu,Frames_Multi_Level,Add, Frame NO Name or ID- Get URL, Advanced_Frame_Multi_level_NO_Name_or_ID_URL
Menu,Frames_Multi_Level,Add, Frame NO Name or ID- Get Text, Advanced_Frame_Multi_level_NO_Name_or_ID_Get_Text

Menu,WebScraping_Advanced,Add, Frames-Multi-Level, :Frames_Multi_Level ;***********Advanced*******************


Menu,WebScraping,Add, 高级, :WebScraping_Advanced ;***********Advanced*******************

;******************************
;***********Misc*******************
;******************************
Menu,WebScraping_Misc,Add,Maximize IE Window   ,  WebScraping_Misc_Maximize_IE_Window
Menu,WebScraping_Misc,Add,Hide Toolbars, WebScraping_Misc_Hide_Toolbars
Menu,WebScraping_Misc,Add,Show Toolbars, WebScraping_Misc_Show_Toolbars

Menu,WebScraping_Misc,Icon,Hide Toolbars,     %A_WinDir%\system32\mmcndmgr.dll,40
Menu,WebScraping_Misc,Icon,Show Toolbars,     %A_WinDir%\system32\mmcndmgr.dll,7

Menu,WebScraping,Add, 其他, :WebScraping_Misc ;***********Miscellaneous*******************

;******************************
;***********Help*******************
;******************************
Menu,WebScraping,Add, ;***********Spacer*******************
Menu,Webscraping,Add, 帮助链接, Helpful_Links




;******************************
;***********常用结构*******************
;******************************
;选择结构
Menu,ifstructure,Add,表达式,ifexpression
Menu,ifstructure,Add,简易写法,Simplecode
Menu,ifstructure,Add,变量is,varcheck
Menu,ifstructure,Add,变量in,varin
Menu,ifstructure,Add,变量between,varbetween
Menu,ifstructure,Add,变量Equal(基本淘汰),VarEqual
Menu,ifstructure,Add,If[Not]InString,If[Not]InString
Menu,ifstructure,Add,If[Not]Exist,If[Not]Exist

Menu,loopstructure,Add,普通,normalloop
Menu,loopstructure,Add,文件和文件夹,fileloop
Menu,loopstructure,Add,注册表,regloop
Menu,loopstructure,Add,文件内容,contentloop
Menu,loopstructure,Add,Parse,parseloop
Menu,loopstructure,Add,while,whileloop
Menu,loopstructure,Add,for,forloop


Menu normalstructure, Add, 标签, CreateLabel
Menu normalstructure, Add, 函数, CreateMyFunc
Menu normalstructure, Add, 类, CreateClass
Menu normalstructure, Add, 选择结构, :ifstructure
Menu normalstructure, Add, 循环结构, :loopstructure
Menu normalstructure, Add, Trycatch ,trystructure


;******************************
;***********函数库*******************
;******************************
;科学计算|界面|文件|字符串|图形|其它
Menu DllCallcode, Add, 获取电脑名, functionscraping_dllcallcode_getcomputername


Menu PostMessagecode, Add, 启动屏幕保护程序, functionscraping_PostMessagecode_启动屏幕保护程序

Menu computecode, Add, 绝对值(abs), functionscraping_computecode_绝对值

Menu filecode, Add, 追加文本(FileAppend), functionscraping_filecode_追加文本

Menu stringcode, Add, 字符串切割(SubStr), functionscraping_stringcode_字符串切割

Menu graphicscode, Add, 找字找图(findtext), functionscraping_graphicscode_findtext

Menu wincode, Add, 一键关闭同类窗口(WinExist/winclose/wingetclass/while), functionscraping_wincode_WinExist/WinClose

Menu comcode, Add, 电脑说话(speak), functionscraping_comcode_speak

Menu funcionothercode, Add, 数字转字符(Chr), functionscraping_funcionothercode_chr

Menu functionscraping, Add, DllCall, :DllCallcode
Menu functionscraping, Add, PostMessage/SendMessage, :PostMessagecode
Menu functionscraping, Add, win, :wincode
Menu functionscraping, Add, com, :comcode
Menu functionscraping, Add, 科学计算, :computecode

Menu functionscraping, Add, 文件, :filecode
Menu functionscraping, Add, 字符串, :stringcode
Menu functionscraping, Add, 图形图像, :graphicscode
Menu functionscraping, Add, 其它, :funcionothercode

;******************************
;***********命令库*******************
;******************************
Menu wincommandcode, Add, 一键关闭当前窗口(WinGetActiveTitle/winclose), commandscraping_wincode_WinGetActiveTitle

Menu graphicscommandcode, Add, 取色宏(PixelGetColor/MouseGetPos), commandscraping_graphicscommandcode_PixelGetColor

Menu commandscraping, Add, win,:wincommandcode
Menu commandscraping, Add, 图形图像,:graphicscommandcode
;******************************
;***********word库*******************
;******************************
Menu wordscraping, Add, 建设中, MenuHandler

;******************************
;***********execl库*******************
;******************************
Menu execlscraping, Add, 建设中, MenuHandler
;******************************
;***********sqlite库*******************
;******************************
Menu sqlitescraping, Add, 建设中, MenuHandler
;******************************
;***********其它类库*******************
;******************************


Menu otherscraping, Add, 以管理员身份运行, runasadmin

;~ Menu ContextMenu, Icon, 类`tCtrl+S, shell32.dll, 259
Menu,autoscripting,Add,常用,:normalstructure
Menu,autoscripting,Icon,常用,%iconlib%,168
Menu,autoscripting,Add,函数库,:functionscraping
Menu,autoscripting,Icon,函数库,%iconlib%,154
Menu,autoscripting,Add,命令库,:commandscraping
Menu,autoscripting,Icon,命令库,%iconlib%,240
Menu,autoscripting,Add,web,:Webscraping
Menu,autoscripting,Icon,web,%iconlib%,158
Menu,autoscripting,Add,word,:wordscraping
Menu,autoscripting,Icon,word,%iconlib%,284
Menu,autoscripting,Add,execl,:execlscraping
Menu,autoscripting,Icon,execl,%iconlib%,285
Menu,autoscripting,Add,sqlite,:sqlitescraping
Menu,autoscripting,Icon,sqlite,%iconlib%,286
Menu,autoscripting,Add,other,:otherscraping
Menu,autoscripting,Icon,other,%iconlib%,156
Menu,wincommandcode,Add,统一快捷键（WinGetClass）,commandscraping_wincommandcode_统一快捷键（WinGetClass）
Menu,stringcode,Add,instr,functionscraping_stringcode_instr
Menu,stringcode,Add,RegExMatch,functionscraping_stringcode_RegExMatch
Menu,stringcode,Add,RegExReplace,functionscraping_stringcode_RegExReplace
Menu,stringcode,Add,StrLen,functionscraping_stringcode_StrLen
Menu,stringcode,Add,StrReplace,functionscraping_stringcode_StrReplace
Menu,stringcode,Add,StrSplit,functionscraping_stringcode_StrSplit
Menu,stringcode,Add,trim,functionscraping_stringcode_trim
Menu,filecode,Add,FileExist,functionscraping_filecode_FileExist
Menu,funcionothercode,Add,F-GetKeyState,functionscraping_funcionothercode_F-GetKeyState
Menu,wincode,Add,WinActive,functionscraping_wincode_WinActive
Menu,wincode,Add,WinExist,functionscraping_wincode_WinExist
Menu,computecode,Add,向上取整后的整数-Ceil,functionscraping_computecode_Ceil
Menu,computecode,Add,e的n次幂-Exp,functionscraping_computecode_e的n次幂Exp
Menu,computecode,Add,向下取整后的整数-floor,functionscraping_computecode_向下取整后的整数-floor
Menu,computecode,Add,对数(10 为底),functionscraping_computecode_对数10为底
Menu,computecode,Add,底数为e的自然对数,functionscraping_computecode_底数为e的自然对数
Menu,computecode,Add,最大值,functionscraping_computecode_最大值
Menu,computecode,Add,最小值,functionscraping_computecode_最小值
Menu,computecode,Add,余数,functionscraping_computecode_余数
Menu,computecode,Add,四舍五入,functionscraping_computecode_四舍五入
Menu,computecode,Add,平方根,functionscraping_computecode_平方根
Menu,computecode,Add,正弦,functionscraping_computecode_正弦
Menu,computecode,Add,余弦,functionscraping_computecode_余弦
Menu,computecode,Add,正切,functionscraping_computecode_正切
Menu,computecode,Add,反正弦,functionscraping_computecode_反正弦
Menu,computecode,Add,反余弦,functionscraping_computecode_反余弦
Menu,computecode,Add,反正切,functionscraping_computecode_反正切
Menu,funcionothercode,Add,抛出异常Exception,functionscraping_funcionothercode_抛出异常Exception
Menu,filecode,Add,打开文件（fileopen）,functionscraping_filecode_打开文件（fileopen）
Menu,funcionothercode,Add,格式化（Format ）,functionscraping_funcionothercode_格式化Format 
Menu,funcionothercode,Add,函数引用（Func ）,functionscraping_funcionothercode_函数引用Func
Menu,funcionothercode,Add,GetKeyName\GetKeyVK\GetKeySC,functionscraping_funcionothercode_GetKeyName\GetKeyVK\GetKeySC
Menu,funcionothercode,Add,IsByRef,functionscraping_funcionothercode_IsByRef
Menu,funcionothercode,Add,IsFunc,functionscraping_funcionothercode_IsFunc
Menu,funcionothercode,Add,IsLabel,functionscraping_funcionothercode_IsLabel
Menu,funcionothercode,Add,IsObject,functionscraping_funcionothercode_IsObject
Menu,graphicscode,Add,listview系列,functionscraping_graphicscode_listview系列
Menu,PostMessagecode,Add,监听在 GUI 窗口中的鼠标点击OnMessage,functionscraping_PostMessagecode_监听在GUI窗口中的鼠标点击OnMessage
Menu,funcionothercode,Add,返回指定字符串中首个字符的序号值,functionscraping_funcionothercode_返回指定字符串中首个字符的序号值
Menu,funcionothercode,Add,从内存地址复制字符串strget,functionscraping_funcionothercode_从内存地址复制字符串strget
Menu,funcionothercode,Add,将字符串复制到内存地址strput,functionscraping_funcionothercode_将字符串复制到内存地址strput
Menu,funcionothercode,Add,显示了所有顶层窗口的摘要RegisterCallback,functionscraping_funcionothercode_显示了所有顶层窗口的摘要RegisterCallback
Menu,funcionothercode,Add,文本控件的背景颜色被改变为自定义颜色RegisterCallback,functionscraping_funcionothercode_文本控件的背景颜色被改变为自定义颜色RegisterCallback
Menu,graphicscode,Add,treeview系列,functionscraping_graphicscode_treeview系列
Menu,funcionothercode,Add,计算字符串需要的缓冲空间VarSetCapacity,functionscraping_funcionothercode_计算字符串需要的缓冲空间VarSetCapacity