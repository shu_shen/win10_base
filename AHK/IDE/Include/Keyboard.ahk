﻿;  Keyboard Shortcuts

#SingleInstance Force
#NoEnv
#NoTrayIcon
SetBatchLines -1
SetWorkingDir %A_ScriptDir%

Menu Tray, Icon, %A_ScriptDir%\..\Icons\Keyboard.ico

Gui Font, s9, Segoe UI

Gui Add, Tab3, hWndhTab x8 y8 w426 h477
SendMessage 0x1329, 0, 0x00170055,, ahk_id %hTab% ; TCM_SETITEMSIZE
GuiControl,, %hTab%, 编辑器|GUI设计器|调试器

IconLib := A_ScriptDir . "\..\Icons\AutoAHK.icl"
IL := IL_Create(3)
IL_Add(IL, IconLib, 2)
IL_Add(IL, IconLib, 5)
IL_Add(IL, IconLib, 103)
SendMessage 0x1303, 0, IL,, ahk_id %hTab% ; TCM_SETIMAGELIST

SetTabIcon(hTab, 1, 1)
SetTabIcon(hTab, 2, 2)
SetTabIcon(hTab, 3, 3)

SendMessage 0x132B, 0, 5 | (4 << 16),, ahk_id %hTab% ; TCM_SETPADDING

Gui Tab, 1
    Gui Add, ListView, hWndhLVEditor x18 y44 w404 h428 +LV0x14000, 操作|快捷键
    SetExplorerTheme(hLVEditor)

    LV_Add("", "新建文件", "Ctrl + N")
    LV_Add("", "从模板新建文件", "Ctrl + T")
    LV_Add("", "脚本指令", "Ctrl + P")
    LV_Add("", "打开文件", "Ctrl + O")
    LV_Add("", "保存", "Ctrl + S")
    LV_Add("", "保存为", "Ctrl + Shift + S")
    LV_Add("", "关闭文件", "Ctrl + W")
    LV_Add("", "撤销", "Ctrl + Z")
    LV_Add("", "重做", "Ctrl + Y")
    LV_Add("", "剪切", "Ctrl + X")
    LV_Add("", "复制", "Ctrl + C")
    LV_Add("", "粘贴", "Ctrl + V")
    LV_Add("", "删除", "Del")
    LV_Add("", "全选", "Ctrl + A")
    LV_Add("", "复制行", "Ctrl + Down")
    LV_Add("", "上移行", "Ctrl + Shift + Up")
    LV_Add("", "下移行", "Ctrl + Shift + Down")
    LV_Add("", "自动完成关键字", "Ctrl + Enter")
    LV_Add("", "显示函数提示", "Ctrl + Space")
    LV_Add("", "插入参数", "Ctrl + Insert")
    LV_Add("", "开关重载函数提示", "Ctrl + Shift + PgDn / PgUp")
    LV_Add("", "插入日期和时间", "Ctrl + D")
    LV_Add("", "查找", "Ctrl + F")
    LV_Add("", "查找下一个", "F3")
    LV_Add("", "查找上一个", "Shift + F3")
    LV_Add("", "替换", "Ctrl + H")
    LV_Add("", "在文件中查找", "Ctrl + Alt + F")
    LV_Add("", "转到行", "Ctrl + G")
    LV_Add("", "标记当前行", "F2")
    LV_Add("", "用错误符号标记行", "Shift + F2")
    LV_Add("", "标记选中文本", "Ctrl + M")
    LV_Add("", "到下一个标记", "Ctrl + PgDn")
    LV_Add("", "到当前标记", "Ctrl + PgUp")
    LV_Add("", "清除所有标记", "Alt + M")
    LV_Add("", "转到匹配括号", "Ctrl + B")
    LV_Add("", "将选择转换为大写", "Ctrl + Shift + U")
    LV_Add("", "将选择转换为小写", "Ctrl + Shift + L")
    LV_Add("", "将选择转换为标题大小写", "Ctrl + Shift + T")
    LV_Add("", "十进制到十六进制", "Ctrl + Shift + H")
    LV_Add("", "十六进制到十进制", "Ctrl + Shift + D")
    LV_Add("", "注释/去注释", "Ctrl + Q")
    LV_Add("", "放大", "Ctrl + Numpad +")
    LV_Add("", "缩小", "Ctrl + Numpad -")
    LV_Add("", "重置缩放", "Ctrl + Numpad 0")
    LV_Add("", "用 AHK 64-位运行", "F9")
    LV_Add("", "用AHK 32-位运行", "Shift + F9")
    LV_Add("", "用适用的程序运行", "Alt + F9")
    LV_Add("", "帮助 (AHK 帮助文件)", "F1")
    LV_Add("", "切换到下一个标签", "Ctrl + Tab")
    LV_Add("", "切换到当前标签", "Ctrl + Shift + Tab")
    LV_Add("", "新建文件 (鼠标)", "Double-click the tab bar")
    LV_Add("", "关闭文件 (鼠标)", "Middle-click the tab")
    LV_Add("", "退出", "Alt + Q")
    ;LV_Add("", "", "")

    LV_ModifyCol(1, 200)
    LV_ModifyCol(2, "AutoHdr")

Gui Tab, 2
    Gui Add, ListView, hWndhLVGUIDesigner x18 y44 w404 h428 +LV0x14000, Action|Key
    SetExplorerTheme(hLVGUIDesigner)

    LV_Add("", "新建GUI", "Alt + G")
    LV_Add("", "导入GUI", "Ctrl + I")
    LV_Add("", "选择所有控件", "Ctrl + A")
    LV_Add("", "移动 1px", "方向键")
    LV_Add("", "移动 8px", "Ctrl + 方向键")
    LV_Add("", "大小调整 1px", "Shift + 方向键")
    LV_Add("", "大小调整 8px", "Ctrl + Shift +方向键")
    LV_Add("", "删除控件", "Del")
    LV_Add("", "改变文本/标题", "F2 or double-click")
    LV_Add("", "属性", "F10 or middle-click")
    LV_Add("", "显示/隐藏上一个窗口", "F11")
    LV_Add("", "切换至编辑模式", "F12")
    ;LV_Add("", "", "")

    LV_ModifyCol(1, 200)
    LV_ModifyCol(2, "AutoHdr")

Gui Tab, 3
    Gui Add, ListView, hWndhLVDebugger x18 y44 w404 h428 +LV0x14000, Action|Key
    SetExplorerTheme(hLVDebugger)

    LV_Add("", "设置断点", "F4")
    LV_Add("", "开始调试", "F5")
    LV_Add("", "步入", "F6")
    LV_Add("", "步进", "F7")
    LV_Add("", "步出", "Shift + F6")
    LV_Add("", "停止调试", "F8")
    LV_Add("", "暂停", "Pause/Break")
    ;LV_Add("", "", "")

    LV_ModifyCol(1, 200)
    LV_ModifyCol(2, "AutoHdr")

Gui Tab

Gui Add, Button, gGuiClose x346 y494 w86 h24 Default, 关闭(&C)

Gui Show, w440 h528, AutoAHK快捷键
Return

GuiEscape:
GuiClose:
    ExitApp

SetExplorerTheme(hWnd) {
    Return DllCall("UxTheme.dll\SetWindowTheme", "Ptr", hWnd, "WStr", "Explorer", "Ptr", 0)    
}

SetTabIcon(hTab, Item, IconIndex) {
    Static OffImg := (3 * 4) + (A_PtrSize - 4) + A_PtrSize + 4
    Static Size := (5 * 4) + (2 * A_PtrSize) + (A_PtrSize - 4)
    VarSetCapacity(TCITEM, Size, 0)
    NumPut(0x2, TCITEM, 0, "UInt") ; 0x2 = TCIF_IMAGE
    NumPut(IconIndex - 1, TCITEM, OffImg, "Int")
    SendMessage 0x133D, Item - 1, &TCITEM,, ahk_id %hTab% ; TCM_SETITEM
}
