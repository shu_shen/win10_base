﻿
NewGUI:
    If (g_GuiTab) 
    {
        Gui Auto: +OwnDialogs
        MsgBox 0x31, New GUI, Only one GUI can be created at a time. Proceed?
        IfMsgBox Cancel
        {
            Return
        }
        ; Change the icon of the previous GUI tab
        If (Sci[g_GuiTab].FileName != "") 
        {
            TabEx.SetIcon(g_GuiTab, 2)
        } 
        Else 
        {
            TabEx.SetIcon(g_GuiTab, 1)
        }
        SetReadOnly(g_GuiTab, 0)
    }

    ; Reset GUI properties
    g := New GuiClass
    g.Window := New g.Window
    ResetMenu()
    DestroyProperties()

    Gui %Child%: Destroy
    Child++

    ; Create a new tab if the current document is not empty
    n := TabEx.GetSel()
    If (Sci[n].Filename == "" && !Sci[n].GetModify()) 
    {
        g_GuiTab := n
    } 
    Else 
    {
        g_GuiTab := NewTab()
    }
    TabEx.SetIcon(g_GuiTab, 3)

    If (!g_DesignMode) 
    {
        GoSub SwitchToDesignMode
    }

    ; Initial position, size and title
    VarSetCapacity(RECT, 16, 0)
    DllCall("GetWindowRect", "Ptr", Sci[n].hWnd, "Ptr", &RECT)
    DllCall("MapWindowPoints", "Ptr", 0, "Ptr", GetParent(hAutoWnd), "Ptr", &RECT, "UInt", 1)
    X := NumGet(RECT, 0, "Int") + 2
    Y := NumGet(RECT, 4, "Int") + 2
    W := 620
    H := 420
    g.Window.Title := "窗口"

    Gui %Child%: New, +LabelChild +hWndhChildWnd +Resize +OwnerAuto -DPIScale
    Gui %Child%: Show, x%X% y%Y% w%W% h%H%, % g.Window.Title
    SetWindowIcon(hChildWnd, IconLib, 5)

    GenerateCode()

    SendMessage TB_CHECKBUTTON, 2170, 1,, ahk_id %hMainToolbar%
    Menu AutoEditMenu, Check, 设为只读(&R)
    SetDocumentStatus(g_GuiTab)

    CreateResizers()
Return

Class GuiClass {
    ControlList := []
    Selection := []
    ControlFuncs := ""
    WinEvents1 := ""
    WinEvents2 := ""
    Anchor := 0
    ControlColor := 0
    Clipboard := ""
    LastControl := 0

    Class Window {
        Title := "Window"
        hWndVar := ""
        Name := ""
        Label := ""
        Options := ""
        Extra := ""
        Styles := ""
        FontName := ""
        FontOptions := ""
        Color := ""
        GuiClose := 1
        GuiEscape := 1
        GuiSize := 0
        GuiContextMenu := 0
        GuiDropFiles := 0
        OnClipboardChange := 0
        Icon := ""
        IconIndex := 1
        Center := 1
    }

    Class Control {
        Handle := -1
        Class := ""
        ClassNN := ""
        Text := ""
        x := 0
        y := 0
        w := 0
        h := 0
        hWndVar := ""
        vVar := ""
        gLabel := ""
        LabelIsFunc := 0
        Anchor := ""
        Options := ""
        Extra := ""
        Styles := ""
        FontName := ""
        FontOptions := ""
    }
}


AddControl(ControlType) {
	Gui %Child%: Default

	; ControlType is the display name in the toolbox.
	; Type is the AHK name (except Separator, Toolbar and CommandLink)

	Type := GetControlType(ControlType)
	if (Type = "按钮")
	{
		Type :="Button"
	}
	if (Type = "复选框")
	{
		Type :="CheckBox"
	}
	if (Type = "组合框")
	{
		Type :="ComboBox"
	}
	if (Type = "日期时间选择器")
	{
		Type :="Date Time Picker"
	}
	if (Type = "下拉列表")
	{
		Type :="DropDownList"
	}
	if (Type = "编辑框")
	{
		Type :="Edit"
	}
	if (Type = "分组框")
	{
		Type :="GroupBox"
	}
	if (Type = "热键")
	{
		Type :="Hotkey"
	}
	if (Type = "链接")
	{
		Type :="Link"
	}
	if (Type = "列表框")
	{
		Type :="ListBox"
	}If (Type = "列表视图")
	{
		Type :="ListView"
	}
    if (Type = "月历")
    {
        Type :="Month Calendar"
    }
    if (Type = "图片")
    {
        Type :="Picture"
    }
    if (Type = "进度条")
    {
        Type :="Progress Bar"
    }
    if (Type = "单选按钮")
    {
        Type :="Radio Button"
    }
    if (Type = "分隔符")
    {
        Type :="Separator"
    }
    if (Type = "标签")
    {
        Type :="Tab"
    }
    if (Type = "文本框")
    {
        Type :="Text"
    }
    if (Type = "树视图")
    {
        Type :="TreeView"
    }
    if (Type = "上下")
    {
        Type :="UpDown"
    }
    if (Type = "滑块")
    {
        Type :="Slider"
    }
    if (Type = "命令行")
    {
        Type :="Command Link"
    }
    if (Type = "自定义")
    {
        Type :="Custom"
    }


    If Type Not In Edit,Hotkey,ListBox,ListView,TreeView
    {
        g_Adding := True
    }
    If Type Not In TreeView,Hotkey,DateTime
    {
        Text := Default[Type].Text
    }
    Options := Default[Type].Options
    Size := " w" . Default[Type].Width . " h" . Default[Type].Height
    Position := " x" . g_X . " y" . g_Y

    If (Type == "Picture") 
    {
        Icon := 0
        If (ChoosePicture(g_PicturePath, Icon)) 
        {
            Text := g_PicturePath
            If (Icon) 
            {
                Options := "+Icon" . Icon
            }
        }

        Size := ""
    } 
    Else If (Type == "Separator") 
    {
        Type := "Text"
    } 
    Else If (Type == "UpDown") 
    {
        If (g[g.LastControl].Type != "Edit") 
        {
            Options .= " -16"
        }
    } 
    Else If (Type == "StatusBar") 
    {
        Size := Position := ""
    } 
    Else If (Type == "CommandLink") 
    {
        Type := "Custom"
    }

    TabPos := IsInTab(g_X, g_Y)
    If (TabPos[1]) {
        Gui %Child%: Tab, % TabPos[1], % TabPos[2]
    }

    _Options := ""
    If (Type == "Tab2") {
        _Options := " AltSubmit +Theme"
    } Else If (Type == "Text" || Type == "Picture") {
        _Options := " +0x100" ; SS_NOTIFY (for WM_SETCURSOR)
    }

    Gui %Child%: Default
    Gui Add, %Type%, % "hWndhWnd " . Options . Size . Position . _Options, %Text%

    If (TabPos[1] || Type == "Tab2") {
        Gui %Child%: Tab
    }

    If (Type == "ListView") {
        LV_Add("", "Item")

    } Else If (Type == "TreeView") {
        Gui %Child%: Default
        Parent := TV_Add("TreeView")
        TV_Add("Child", Parent)

    } Else If (DisplayName == "Command Link") {
        Type := "CommandLink"
        Options := ""
        Extra := Default[Type].Options

    } Else If (DisplayName == "Separator") {
        Type := "Separator"
        Options := ""
        Extra := Default[Type].Options

    } Else If (Type == "Custom" || Type == "Picture") {
        Extra := Options
        Options := ""
    }

    ApplyGUIFont(hWnd)

    g.ControlList.Push(hWnd)
    ClassNN := GetClassNN(hWnd)

    ;~ Register(hWnd, Type, ClassNN, Text, "", "", "", Options, Extra, "", "", "", "", TabPos)
    Register(hWnd, Type, ClassNN, Text, cx, cy, cw, ch, "", "", "", Options, "", "", "", "", TabPos, Extra)
    GenerateCode()

    Properties_AddItem(ClassNN)
    If (IsWindowVisible(hPropWnd)) {
        GoSub ShowProperties ; ?
    }

    DestroySelection()
    Return hWnd
}

ApplyGUIFont(hCtl) {
    If (g.Window.FontName != "" || g.Window.FontOptions != "") {
        Gui %Child%: Font, % g.Window.FontOptions, % g.Window.FontName
        GuiControl Font, %hCtl%
        Gui %Child%: Font
    }
}

MoveControl() {
    If (g_Adding) {
        Return
    }

    Gui %Child%: Default

    CoordMode Mouse, Screen
    MouseGetPos mx1, my1,, g_Control, 2

    Selection := GetSelectedItems()

    Controls := []
    For Each, Item In Selection {
        GuiControlGet c, Pos, %Item%
        Controls.Push({x: cx, y: cy})
    }

    ;While (GetKeyState("LButton", "P")) {
    While (g_LButtonDown) {
        MouseGetPos mx2, my2

        If (mx2 == PrevX && my2 == PrevY) {
            Continue
        }

        PrevX := mx2
        PrevY := my2

        For Index, Item In Selection {
            GuiControlGet c, Pos, % Item
            If (g_SnapToGrid) {
                PosX := RoundTo(Controls[Index].x + (mx2 - mx1), g_GridSize)
                PosY := RoundTo(Controls[Index].y + (my2 - my1), g_GridSize)
            } Else {
                PosX := Controls[Index].x + (mx2 - mx1)
                PosY := Controls[Index].y + (my2 - my1)
            }
            GuiControl MoveDraw, %Item%, % "x" . PosX . " " . "y" . PosY
        }

        If (g_GuiVis) {
            Gui Auto: Default
            SB_SetText("Position: " . (g[Selection[1]].x + (mx2 - mx1)) . ", " . g[Selection[1]].y + (my2 - my1), 2)
            Gui %Child%: Default
        }

        Sleep 1
    }

    If (mx2 == mx1 && my2 == my1) {
        Return
    }

    Properties_UpdateCtlPos()
    GenerateCode()
    UpdateSelection()
    Return
}

ResizeControl(hCtl) {
    HideResizers()

    MouseGetPos mx, my
    ControlGetPos,,, w, h,, ahk_id %hCtl%
    xOffset := w - mx
    yOffset := h - my

    Gui %Child%: Default
    Selection := GetSelectedItems()

    ;While (GetKeyState("LButton", "P")) {
    While (g_LButtonDown) {
        MouseGetPos mx, my

        h := my + yOffset
        w := mx + xOffset

        If (g_SnapToGrid) {
            w := RoundTo(w, g_GridSize)
            h := RoundTo(h, g_GridSize)
        }

        For Each, Item In Selection {
            GuiControl MoveDraw, %Item%, w%w% h%h%
        }

        Gui Auto: Default
        SB_SetText("Size: " . w . " x " . h, 3)
        Gui %Child%: Default

        Sleep 1
    }

    Properties_UpdateCtlPos()
    GenerateCode()
    UpdateSelection()
}

RemoveControl(hCtl) {
    If (hCtl == hChildWnd) {
        Return
    }

    If (g[hCtl].Type == "StatusBar") {
        GuiControl Hide, %hCtl%
        g[hCtl].Deleted := True
        GoSub ReloadControlList
        GenerateCode()
        Return
    }

    If (g[hCtl].Type == "Tab2") {
        Gui %Child%: Tab
    }

    hParent := GetParent(hCtl)
    If (hParent != hChildWnd) {
        hCtl := hParent
    }

    g[hCtl].Deleted := DestroyWindow(hCtl)
    g.ControlList.Delete(hCtl)
}

GetSelectedItems() {
    If (g.Selection.Length()) {
        Return g.Selection
    } Else {
        If (g[g_Control].Handle == "") {
            g_Control := GetParent(g_Control)
        }
        SelectedItems := []
        SelectedItems.Push(g_Control)
        Return SelectedItems
    }
}

CutControl() {
    g.Clipboard := g_Control
    GuiControl Hide, %g_Control%
    g[g_Control].Deleted := True
    Menu WindowContextMenu, Enable, Paste
    GenerateCode()
    DestroySelection()
    If (g_Control == hReszdCtl) {
        HideResizers()
    }
}

CopyControl() {
    g.Clipboard := g[g_Control].Clone()
    GuiControlGet Pos, %Child%: Pos, %g_Control%
    g.Clipboard.W := PosW
    g.Clipboard.H := PosH

    Menu WindowContextMenu, Enable, Paste
}

PasteControl() {
    ; From Copy
    If (g.Clipboard.HasKey("Handle")) {
        Type := g.Clipboard.Type
        /*
        If (Type == "Tab2") {
            Type := "Tab3"
        } Else If (Type == "CommandLink") {
            Type := "Custom"
        } Else If (Type == "Separator") {
            Type := "Text"
        }
        */

        x := g_X
        y := g_Y
        w := g.Clipboard.W
        h := g.Clipboard.H
        Text := g.Clipboard.Text
        Options := g.Clipboard.Options
        Extra := g.Clipboard.Extra
        Styles := g.Clipboard.Styles
        FontName := g.Clipboard.FontName
        FontOptions := g.Clipboard.FontOptions
        Anchor := g.Clipboard.Anchor

        TabPos := IsInTab(x, y)
        If (TabPos[1]) {
            Gui %Child%: Tab, % TabPos[1], % TabPos[2]
        }

        Gui %Child%: Add, %Type%, hWndhWnd x%x% y%Y% w%w% h%h% %Options% %Extra% %Styles%, %Text%

        ClassNN := GetClassNN(hWnd)
        ;Register(hWnd, Type, ClassNN, Text,,,, Options, Extra, Styles, FontName, FontOptions, Anchor, TabPos)
        Register(hWnd, Type, ClassNN, Text, cx, cy, cw, ch, "", "", "", Options, "", "", "", "", TabPos, Extra)
        g.ControlList.Push(hWnd)
        Properties_AddItem(ClassNN)

        fFont := False
        If (FontOptions != "" || FontName != "") {
            Gui %Child%: Font, %FontOptions%, %FontName%
            fFont := True
        } Else If (g.Window.FontOptions != "" || g.Window.FontName != "") {
            Gui %Child%: Font, % g.Window.FontOptions, % g.Window.FontName
            fFont := True
        }

        If (fFont) {
            GuiControl Font, %hWnd%
            Gui %Child%: Font
        }

        If (TabPos[1]) {
            Gui %Child%: Tab
        }
    ; From Cut
    } Else {
        GuiControl Move, % g.Clipboard, x%g_X% y%g_Y%
        GuiControl Show, % g.Clipboard
        g[g.Clipboard].Deleted := False
    }

    GenerateCode()
}

ResetMenu() {
    m.Code := ""
    g_MenuFuncs := ""

    hMenu := GetMenu(hChildWnd)
    TopMenuCount := GetMenuItemCount(hMenu)
    Loop %TopMenuCount% {
        hSubMenu := GetSubMenu(hMenu, A_Index - 1)
        SubMenuCount := GetMenuItemCount(hSubMenu)
        Loop %SubMenuCount% {
            DeleteMenu(hSubMenu, 0)
        }
    }

    Try {
        Menu MenuBar, Delete
    }
}

AddStatusBar() {
    If (StatusBarExist()) {
        Gui %Child%: Default
        GuiControlGet SBVis, Visible, msctls_statusbar321
        GuiControl Hide%SBVis%, msctls_statusbar321
        GuiControlGet hStatusBar, Hwnd, msctls_statusbar321
        g[hStatusBar].Deleted := SBVis
        GenerateCode()
        GoSub ReloadControlList
    } Else {
        AddControl("Status Bar")
    }
}

StatusBarExist() {
    GuiControlGet SBExist, %Child%: hWnd, msctls_statusbar321
    Return SBExist
}

MenuBarExist() {
    Return GetMenu(hCHildWnd)
}

ToolbarExist() {
    Return IsWindow(hChildToolbar)
}

; Mark selected controls
Select(SelectedControls) 
{
    If (!SelectedControls.Length()) 
    {
        Return
    }

    If (!WinExist("ahk_id " . hSelWnd)) 
    {
        Gui SelWnd: New, hWndhSelWnd +ToolWindow -Border -Caption +E0x20 +OwnerAuto +LastFound -DPIScale
        Gui SelWnd: Color, 0xF0F0F0
        WinSet TransColor, 0xF0F0F0 100
        WinGetPos wX, wY, wW, wH, ahk_id %hChildWnd%
        Gui SelWnd: Show, NA x%wX% y%wY% w%wW% h%wH%
    }

    For Each, Item In SelectedControls 
    {
        ControlGetPos cX, cY, cW, cH,, ahk_id %Item%
        Gui SelWnd: Add, Progress, c1BCDEF x%cX% y%cY% w%cW% h%cH%, 100
    }
}

SelectAllControls() 
{
    g.Selection := []
    For Each, Item In g.ControlList 
    {
        If (g[Item].Deleted == False) 
        {
            g.Selection.Push(Item)
        }
    }
    Select(g.Selection)
}

SelectTabItems(hTabControl) 
{
    g.Selection.Push(hTabControl)
    ControlGetPos tx, ty, tw, th,, ahk_id %hTabControl%
    TabItems := GetControlsFromRegion(tx + 1, ty + 1, (tx + tw), (ty + th))
    For Each, Item In TabItems 
    {
        g.Selection.Push(Item)
    }
    Select(g.Selection)
}

DestroySelection() 
{
    DestroyWindow(hSelWnd)
    g.Selection := []
}

UpdateSelection() 
{
    DestroyWindow(hSelWnd)
    Select(g.Selection)
}

GetControlsFromRegion(x1, y1, x2, y2) 
{
    ControlsFromRegion := []

    If (x1 > x2) 
    { ; Selection is from right to left
        x1 ^= x2, x2 ^= x1, x1 ^= x2
    }

    If (y1 > y2) 
    { ; Selection is from bottom to top
        y1 ^= y2, y2 ^= y1, y1 ^= y2
    }

    WinGet Children, ControlListHwnd, ahk_id %hChildWnd%
    Loop Parse, Children, `n
    {
        If (g[A_LoopField].Class == "") {
            Continue
        }

        ControlGetPos CtrlX, CtrlY,,,, ahk_id %A_LoopField%

        If (IfBetween(CtrlX, x1, x2) And (IfBetween(CtrlY, y1, y2))) {
            ControlsFromRegion.Push(A_LoopField)
        }
    }

    Return ControlsFromRegion
}

IsInTab(x, y) {
    TabControls := []
    WinGet Controls, ControlList, ahk_id %hChildWnd%
    Loop Parse, Controls, `n
    {
        If (InStr(A_LoopField, "Tab")) {
            TabControls.Push(A_LoopField)
        }
    }

    For TabControl, Item In TabControls {
        ControlGetPos tx, ty, tw, th, %Item%, ahk_id %hChildWnd%
        If (IfBetween(x, tx, (tx + tw)) && (IfBetween(y, (ty + 1), (ty + th)))) {
            GuiControlGet TabIndex, %Child%:, %Item%
            Return [TabIndex, TabControl]
        }
    }
}

OrderTabItems() {
    TabControls := []
    TabItems := []
    Items := []

    For Index, Item In g.ControlList {
        If (g[Item].Type == "Tab2") {
            TabControls.Push(Item)
        } Else If (g[Item].Tab[1] != "") {
            TabItems[g[Item].Tab[2], g[Item].Tab[1], Index] := Item
        } Else {
            Items.Push(Item)
        }
    }

    OrderedList := []
    For i, TabControl In TabControls {
        OrderedList.Push(TabControl)
        For j, TabPage In TabItems[i] {
            For k, TabItem In TabItems[i][j] {
                OrderedList.Push(TabItems[i][j][k])
            }
        }
    }

    g.ControlList := Items

    For Each, Item In OrderedList {
        g.ControlList.Push(Item)
    }
}

DeleteSelectedControls() {
    Selection := GetSelectedItems()

    For Each, Item In Selection {
        RemoveControl(Item)
        If (Item == hReszdCtl) {
            HideResizers()
        }
    }

    GoSub ReloadControlList
    GenerateCode()
    g.Selection := []
    DestroySelection()
}

AlignLefts() {
    Gui %Child%: Default
    GuiControlGet p, Pos, % g.Selection[1]
    For Each, Item In g.Selection {
        GuiControl Move, %Item%, % "x" . px
        g[Item].x := px
    }
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

AlignRights() {
    Gui %Child%: Default
    GuiControlGet p, Pos, % g.Selection[1]
    For Each, Item In g.Selection {
        ControlGetPos,,, w,,, ahk_id %Item%
        x := (px + pw) - w
        GuiControl Move, % Item, % "x" . x
        g[Item].x := x
    }
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

AlignTops() {
    Gui %Child%: Default
    GuiControlGet p, Pos, % g.Selection[1]
    For Each, Item In g.Selection {
        GuiControl Move, % Item, % "y" . py
        g[Item].y := py
    }
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

AlignBottoms() {
    Gui %Child%: Default
    GuiControlGet p, Pos, % g.Selection[1]
    For Each, Item In g.Selection {
        ControlGetPos, ,,, h,, ahk_id %Item%
        y := (py + ph) - h
        GuiControl Move, % Item, % "y" . y
        g[Item].y := y
    }
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

CenterHorizontally() {
    WinGetPos,,, ww,, ahk_id %hChildWnd%

    Selection := GetSelectedItems()

    If (Selection.Length() > 1) {
        x1 := 1000000
        For Each, Item In Selection {
            ControlGetPos cx,,,,, ahk_id %Item%
            If (cx < x1) {
                x1 := cx
            }
        }
        x2 := 0
        For Each, Item In Selection {
            ControlGetPos cx,, cw,,, ahk_id %Item%
            If ((cx + cw) > x2) {
                x2 := cx + cw
            }
        }
        cw := x1 + x2
    } Else {
        ControlGetPos,,, cw,,, % "ahk_id " . Selection[1]
    }

    ww -= cw
    ww /= 2

    If (Selection.Length() > 1) {
        For Each, Item In Selection {
            ControlGetPos, cx,,,,, ahk_id %Item%
            ControlMove,, % ww + cx,,,, ahk_id %Item%
        }
    } Else {
        ControlMove,, % ww,,,, % "ahk_id " . Selection[1]
    }

    Repaint(hChildWnd)
    Properties_UpdateCtlPos()
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

CenterVertically() {
    GetClientSize(hChildWnd, ww, wh)
    Gui %Child%: Default

    Selection := GetSelectedItems()

    If (Selection.Length() > 1) {
        Min := 1000000
        For Each, Item In Selection {
            GuiControlGet c, Pos, %Item%
            If (cy < Min) {
                Min := cy
            }
        }
        Max := 0
        For Each, Item In Selection {
            GuiControlGet c, Pos, %Item%
            If ((cy + ch) > Max) {
                Max := cy + ch
            }
        }
        ch := Min + Max
    } Else {
        GuiControlGet c, Pos, % Selection[1]
    }

    wh := (wh - ch) / 2

    If (Selection.Length() > 1) {
        For Each, Item In Selection {
            GuiControlGet c, Pos, %Item%
            GuiControl Move, %Item%, % "y" . (wh + cy)
        }
    } Else {
        GuiControl Move, % Selection[1], y%wh%
    }

    Repaint(hChildWnd)
    Properties_UpdateCtlPos()
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

HorizontallySpace() {
    Gui %Child%: Default
    MaxIndex := g.Selection.Length()

    Start := 1000000, End := 0, ControlList := ""
    For Index, Item in g.Selection {
        ControlGetPos, x,, w,,, ahk_id %Item%
        If ((x + w) < Start) {
            Start := x + w
            FirstItem := Item
        }
        If (x > End) {
            End := x
            LastItem := Item
        }
        ControlList .= (Index != MaxIndex) ? Item . "|" : Item
    }

    InnerWidth := End - Start
    InnerItemsWidth := 0
    For Each, Item in g.Selection {
        If ((Item != FirstItem) && (Item != LastItem)) {
            ControlGetPos,,, w,,, ahk_id %Item%
            InnerItemsWidth += w
        }
    }
    EmptySpace := InnerWidth - InnerItemsWidth
    Quotient := EmptySpace // (MaxIndex - 1)

    Sort ControlList, D| F SortByX
    Controls := []
    Loop Parse, ControlList, `|
    {
        Controls.Push(A_LoopField)
    }

    For Index, Control in Controls {
        If ((Index != 1) && (Index != MaxIndex)) {
            GuiControlGet p, Pos, % Controls[(Index - 1)]
            GuiControl MoveDraw, % Control, % "x" . (px + pw) + Quotient
        }
    }

    GenerateCode()
    UpdateSelection()
    HideResizers()
}

VerticallySpace() {
    Gui %Child%: Default
    MaxIndex := g.Selection.Length()

    Start := 1000000, End := 0, ControlList := ""
    For Index, Item in g.Selection {
        ControlGetPos,, y,, h,, ahk_id %Item%
        If ((y + h) < Start) {
            Start := y + h
            FirstItem := Item
        }
        If (y > End) {
            End := y
            LastItem := Item
        }
        ControlList .= (Index != MaxIndex) ? Item . "|" : Item
    }

    InnerHeight := End - Start
    InnerItemsHeight := 0
    For Each, Item in g.Selection {
        If ((Item != FirstItem) && (Item != LastItem)) {
            ControlGetPos,,,, h,, ahk_id %Item%
            InnerItemsHeight += h
        }
    }
    EmptySpace := InnerHeight - InnerItemsHeight
    Quotient := EmptySpace // (MaxIndex - 1)

    Sort ControlList, D| F SortByY
    Controls := []
    Loop Parse, ControlList, `|
    {
        Controls.Push(A_LoopField)
    }

    For Index, Control in Controls {
        If ((Index != 1) && (Index != MaxIndex)) {
            GuiControlGet p, Pos, % Controls[(Index - 1)]
            GuiControl MoveDraw, % Control, % "y" . (py + ph) + Quotient
        }
    }

    GenerateCode()
    UpdateSelection()
    HideResizers()
}

SortByX(hCtl1, hCtl2) {
    ControlGetPos, x1,,,,, ahk_id %hCtl1%
    ControlGetPos, x2,,,,, ahk_id %hCtl2%
    Return (x1 > x2) ? 1 : 0
}

SortByY(hCtl1, hCtl2) {
    ControlGetPos,, y1,,,, ahk_id %hCtl1%
    ControlGetPos,, y2,,,, ahk_id %hCtl2%
    Return (y1 > y2) ? 1 : 0
}

MakeSameWidth:
    MakeSame("w")
Return

MakeSameHeight:
    MakeSame("h")
Return

MakeSameSize:
    MakeSame("wh")
Return

MakeSame(m) {
    Gui %Child%: Default
    ControlGetPos,,, w, h,, % "ahk_id " . g.Selection[1]
    Expression := ""
    If (InStr(m, "w")) {
        Expression .= "w" . w
    }
    If (InStr(m, "h")) {
        Expression .= "h" . h
    }
    For Each, Item In g.Selection {
        GuiControl Move, % Item, %Expression%
    }
    GenerateCode()
    UpdateSelection()
    HideResizers()
}

StretchControl() {
    Gui %Child%: Default
    GetClientSize(hChildWnd, ww, wh)

    If (A_ThisMenuItem == "Stretch Horizontally") {
        Expression := "x0 w" . ww
    } Else { ; Stretch Vertically
        y := 0
        TBHeight := 0
        SBHeight := 0

        If (ToolbarExist()) {
            ControlGetPos,,,, TBHeight,, ahk_id %hChildToolbar%
            ControlGet TBStyle, Style,,, ahk_id %hChildToolbar%
            y := ((TBStyle & 0x3) == 3) ? 0 : TBHeight ; CCS_BOTTOM
        }

        If (StatusBarExist()) {
            ControlGetPos,,,, SBHeight, msctls_statusbar321, ahk_id %hChildWnd%
        }

        Expression := "y" . y . " h" . (wh - TBHeight - SBHeight)
    }

    Selection := GetSelectedItems()
    For Each, Item In Selection {
        GuiControl Move, %Item%, %Expression%
    }

    GenerateCode()
    UpdateSelection()
    HideResizers()
}

ShowAdjustPositionDialog:
    GuiControlGet p, %Child%: Pos, %g_Control%

    Gui SizePosDlg: New, LabelSizePosDlg -MinimizeBox OwnerAuto
    Gui Font, s9, Segoe UI
    Gui Color, White

    Gui Add, GroupBox, x9 y2 w270 h107
    Gui Add, Text, x28 y28 w50 h23 +0x200, &X (Left):
    Gui Add, Edit, hWndhEdtX vEdtX x79 y28 w58 h22
    Gui Add, UpDown, gSizePosDlgApply Range-65536-65536 +0x80, %px%
    Gui Add, Text, x150 y28 w50 h23 +0x200, &Y (Top):
    Gui Add, Edit, vEdtY x201 y29 w58 h22
    Gui Add, UpDown, gSizePosDlgApply Range-65536-65536 +0x80, %py%
    Gui Add, Text, x28 y68 w50 h24 +0x200, &Width:
    Gui Add, Edit, vEdtW x79 y68 w58 h22
    Gui Add, UpDown, gSizePosDlgApply Range-65536-65536 +0x80, %pw%
    Gui Add, Text, x150 y68 w50 h24 +0x200, &Height:
    Gui Add, Edit, vEdtH x201 y68 w58 h22
    Gui Add, UpDown, gSizePosDlgApply Range-65536-65536 +0x80, %ph%

    Gui Add, Text, x-1 y118 w291 h48 +Border -Background
    Gui Add, Button, gSizePosDlgReset x8 y130 w86 h24, &Reset
    Gui Add, Button, gSizePosDlgClose x101 y130 w86 h24, &Close
    Gui Add, Button, gSizePosDlgApply x194 y130 w86 h24 +Default, &Apply

    Gui Show, w288 h165, Position and Size
    SetWindowIcon(WinExist("A"), IconLib, 75)
    SetModalWindow(1)
    SendMessage 0xB1, -1, 0,, ahk_id %hEdtX% ; EM_SETSEL
Return

SizePosDlgApply:
    Gui SizePosDlg: Submit, NoHide
    GuiControl %Child%: MoveDraw, %g_Control%, x%EdtX% y%EdtY% w%EdtW% h%EdtH%
    HideResizers()
Return

SizePosDlgReset:
    Gui SizePosDlg: Default
    GuiControl,, EdtX, %px%
    GuiControl,, EdtY, %py%
    GuiControl,, EdtW, %pw%
    GuiControl,, EdtH, %ph%
    GoSub SizePosDlgApply
Return

SizePosDlgEscape:
SizePosDlgClose:
    GenerateCode()
    Properties_UpdateCtlPos()
    SetModalWindow(0)
    Gui SizePosDlg: Destroy
Return

ChangeTitle:
    SetModalWindow(1)

    Title := UnescapeChars(g.Window.Title)
    NewTitle := InputBoxEx("Window Title", "", "Change Title", Title,,, hAutoWnd,,, IconLib, 37)

    If (!ErrorLevel) {
        WinSetTitle ahk_id %hChildWnd%,, %NewTitle%
        g.Window.Title := EscapeChars(NewTitle, True)
        GenerateCode()

        If (Properties_GetClassNN() == "Window") {
            GuiControl, Properties:, EdtText, %NewTitle%
        }
    }

    SetModalWindow(0)
Return

ChangeText:
    ChangeText(A_GuiControl == "BtnText" ? 1 : 0)
Return

ChangeText(Multiline := False) {
    CtlType := g[g_Control].Type

    If (CtlType ~= "TreeView|ActiveX|Separator") {
        Gui Auto: +OwnDialogs
        MsgBox 0x40, Change Text, Not available for %CtlType%.
        Return

    } Else If (CtlType == "Picture") {
        ImagePath := g[g_Control].Text
        ExtraOpts := g[g_Control].Extra
        ImageType := RegExMatch(ExtraOpts, "i)Icon(\d+)", IconIndex)

        If (ChoosePicture(ImagePath, IconIndex, ImageType)) {
            g[g_Control].Text := ImagePath

            If (ImageType) {
                If (InStr(ExtraOpts, "Icon")) {
                    g[g_Control].Extra := RegExReplace(ExtraOpts, "\+?Icon\d+", "+Icon" . IconIndex)
                } Else {
                    g[g_Control].Extra .= Space(ExtraOpts) . "+Icon" . IconIndex
                }
            } Else {
                g[g_Control].Extra := RegExReplace(ExtraOpts, "\+?Icon\d+")
            }

            Icon := (ImageType) ? "*Icon" . IconIndex . " " : ""
            GuiControl %Child%:, %g_Control%, % Icon . ImagePath
            GenerateCode()
        }
        Return
    }

    hParent := GetParent(g_Control)
    If (g[hParent].Type == "ComboBox") {
        g_Control := hParent
    }

    Instruction := Default[CtlType].DisplayName . " Text"
    Content := ""
    ControlText := UnescapeChars(g[g_Control].Text)
    InputControl := "Edit"
    If (Multiline && RegExMatch(CtlType, "^(Edit|Text|Button|CheckBox|Radio|Link|CommandLink)$")) {
        InputOptions := "Multi R3"
    } Else {
        InputOptions := ""
    }

    If (CtlType == "Text") {
        Instruction := "Text:"

    } Else If (CtlType ~= "ComboBox|DropDownList|ListBox") {
        Instruction := CtlType . " Items"
        Content := "Pipe-delimited list of items. To have one of the entries pre-selected, include two pipes after it (e.g. Red|Green||Blue)."

    } Else If (CtlType == "ListView") {
        Instruction := "ListView Columns"
        Content := "Pipe-delimited list of column names (e.g. ID|Name|Value):"

    } Else If (CtlType == "Tab2") {
        Instruction := "Tabs"
        Content := "Separate each tab item with a pipe character (|)."

    } Else If (CtlType ~= "Progress|Slider|UpDown") {
        Instruction := Default[CtlType].DisplayName . " Position"
        InputOptions := "Number"

    } Else If (CtlType == "Hotkey") {
        Instruction := "Hotkey"
        Content := "Modifiers: ^ = Control, + = Shift, ! = Alt.`nSee the <a href=""https://autohotkey.com/docs/KeyList.htm"">key list</a> for available key names."

    } Else If (CtlType == "DateTime") {
        Instruction := "Date Time Picker"
        Content := "Format: (e.g.: LongDate, Time, dd-MM-yyyy)"
        InputControl := "ComboBox"
        PreDefItems := StrReplace("|ShortDate|LongDate|Time", (ControlText != "") ? "|" . ControlText : "")
        ControlText .= "|" . PreDefItems

    } Else If (CtlType == "MonthCal") {
        Instruction := "Month Calendar"
        Content := "To have a date other than today pre-selected, specify it in YYYYMMDD format."

    } Else If (CtlType == "CommandLink") {
        InputOptions := "Multi r3"
    }

    SetModalWindow(1)

    NewText := InputBoxEx(Instruction
        , Content
        , "Change Text"
        , ControlText
        , InputControl
        , InputOptions
        , hAutoWnd
        , "", ""
        , IconLib, 14)

    If (!ErrorLevel) {
        SetControlText(g_Control, NewText)
        GenerateCode()
    }

    SetModalWindow(0)
}

SetControlText(hWnd, Text) {
    If (g[hWnd].Type == "Button" && Text == "...") {
        GuiControl %Child%: Move, %hWnd%, w23 h23
        Properties_UpdateCtlPos()
    }

    ; Unescape newline and tab
    StringReplace Text, Text, ``n, `n, A
    StringReplace Text, Text, ``t, `t, A

    If (g[hWnd].Type == "ListView") {
        Gui %Child%: Default
        Gui ListView, %hWnd%

        While (LV_GetText(foo, 0, 1)) {
            LV_DeleteCol(1)
        }

        Loop Parse, Text, |
        {
            LV_InsertCol(A_Index, "AutoHdr", A_LoopField)
        }
        LV_ModifyCol(1, "AutoHdr")

    } Else If (g[hWnd].Type ~= "Tab2|ListBox") {
        GuiControl %Child%:, %hWnd%, |%Text%

    } Else If (g[hWnd].Type == "DateTime") {
        GuiControl Text, %hWnd%, % (Text == "ShortDate") ? "" : Text

    } Else If (g[hWnd].Type == "CommandLink") {
        CLText := StrSplit(Text, "`n")
        GuiControl %Child%:, %hWnd%, % CLText[1]
        If (CLText.Length() > 1) {
            Note := StrReplace(Text, CLText[1] . "`n")
            SendMessage 0x1609, 0, % "" . Note,, ahk_id %hWnd% ; BCM_SETNOTE
            GuiControl %Child%: Move, %hWnd%, h58
            Properties_UpdateCtlPos()
        }

    } Else {
        GuiControl %Child%:, %hWnd%, %Text%
    }

    If (Properties_GetClassNN() == g[hWnd].ClassNN) {
        GuiControl, Properties:, EdtText, %Text%
    }

    g[hWnd].Text := EscapeChars(Text)
}

EscapeChars(String, Title := False) {
    String := RegExReplace(String, "``(?!n|t)", "````") ; Backtick (lookahead skips `n and `t)
    String := StrReplace(String, " `;", " ```;")        ; Comment
    StringReplace String, String, `%, ```%, A           ; %
    If (Title) {
        StringReplace String, String, `,, ```,, A       ; Comma
    } Else {
        StringReplace String, String, `n, ``n, A        ; Newline
        StringReplace String, String, `t, ``t, A        ; Tab
    }
    Return String
}

UnescapeChars(String) {
    String := StrReplace(String, "``%", "%")
    String := StrReplace(String, " ```;", " `;")
    String := StrReplace(String, "``,", ",")
    ;String := StrReplace(String, "````", "``")
    String := StrReplace(String, "``n", "`n")
    String := StrReplace(String, "``t", "`t")
    Return String
}

ChoosePicture(ByRef ImagePath, ByRef IconIndex, ByRef ImageType := 0) {
    If (!ImageType) {
        Filter := "*.jpg; *.png; *.gif; *.bmp; *.ico; *.icl; *.exe; *.dll; *.cpl; *.jpeg"
        Gui Auto: +OwnDialogs
        FileSelectFile SelectedFile, 1, %ImagePath%, Select Picture File, Picture Files (%Filter%)
        If (ErrorLevel) {
            Return
        }

        SplitPath SelectedFile,,, FileExt
        If (FileExt ~= "i)exe|dll|cpl|icl|scr|ocx|ax") {
            ImagePath := SelectedFile
            ImageType := 1
        } Else {
            ImagePath := SelectedFile
        }
    }

    If (ImageType) {
        If (ChooseIcon(ImagePath, IconIndex, hAutoWnd)) {
            ImagePath := IconPath := StrReplace(ImagePath, A_WinDir . "\System32\")
        } Else {
            Return
        }
    }

    Return 1
}

ShowActiveXDialog() {
    ActiveXComponent := InputBoxEx("ActiveX 组件"
        , "输入可以嵌入窗口的ActiveX对象的标识符.`n在资源管理器中载入文件夹路经军或Internet地址."
        , "ActiveX"
        , "Shell.Explorer||HTMLFile|WMPlayer.OCX"
        , "ComboBox"
        , ""
        , hAutoWnd
        , "", ""
        , IconLib, 46)

    If (!ErrorLevel) {
        Default["ActiveX"].Text := ActiveXComponent
        g_Cross := True
    }
}

ShowCustomClassDialog() {
    ClassName := InputBoxEx("Win32 控件类名"
        , "输入已注册Win32控件的类名."
        , "自定义控件"
        , "Button||ComboBoxEx32|ReBarWindow32|ScrollBar|SysAnimate32|SysPager|SysTabControl32"
        , "ComboBox"
        , ""
        , hAutoWnd
        , "", ""
        , IconLib, 71)

    If (!ErrorLevel) {
        Default["Custom"].Options := "Class" . ClassName

        If (ClassName == "ComboBoxEx32") {
            Default["Custom"].Options .= " +0x3"
        } Else If (ClassName == "SysPager") {
            Default["Custom"].Options .= " +E0x20000"
        }

        g_Cross := True
    }
}

AskToDisconnect() {
    RetVal := g_NT6orLater ? ConfirmDisconnect() : ConfirmDisconnectXP()

    If (RetVal == 101) { ; Disconnect
        SetReadOnly(g_GuiTab, False)
        n := g_GuiTab
        g_GuiTab := 0
        g_GuiVis := 0
        TabEx.SetIcon(n, GetIconForTab(n))

        Gui %Child%: Destroy
        DestroyProperties()
        If (g_DesignMode) {
            GoSub SwitchToEditorMode
        }

    } Else If (RetVal == 102) {
        If (TabIndex := DuplicateTab(TabEx.GetSel())) {
            SetDocumentStatus(TabIndex)
        }
    }
}

ConfirmDisconnect() {
    Instruction := "Would you like to edit the generated code?"
    Content := "In order to edit the generated code, the GUI designer must be dissociated from the current tab."
    Title := "AutoAHK"
    CustomButtons := []
    CustomButtons.Push([101, "Disconnect the GUI designer`nThe generated code can be modified, but the GUI designer will not be active for this tab."])
    CustomButtons.Push([102, "Duplicate tab contents`nCopy the contents of this tab to a new tab, which is not associated with the GUI designer."])
    cButtons := CustomButtons.Length()
    VarSetCapacity(pButtons, 4 * cButtons + A_PtrSize * cButtons, 0)
    Loop %cButtons% {
        iButtonID := CustomButtons[A_Index][1]
        iButtonText := &(b%A_Index% := CustomButtons[A_Index][2])
        NumPut(iButtonID,   pButtons, (4 + A_PtrSize) * (A_Index - 1), "Int")
        NumPut(iButtonText, pButtons, (4 + A_PtrSize) * A_Index - A_PtrSize, "Ptr")
    }
    ExpandedText := "If you have not finished designing the GUI, choose the second option: ""Duplicate tab contents""."

    x64 := A_PtrSize == 8
    NumPut(VarSetCapacity(TDC, x64 ? 160 : 96, 0), TDC, 0, "UInt") ; cbSize
    NumPut(hAutoWnd, TDC, 4, "Ptr") ; hwndParent
    NumPut(0x50, TDC, x64 ? 20 : 12, "Int") ; dwFlags
    NumPut(0x8, TDC, x64 ? 24 : 16, "Int") ; dwCommonButtons
    NumPut(&Title, TDC, x64 ? 28 : 20, "Ptr") ; pszWindowTitle
    NumPut(&Instruction, TDC, x64 ? 44 : 28, "Ptr") ; pszMainInstruction
    NumPut(&Content, TDC, x64 ? 52 : 32, "Ptr") ; pszContent
    NumPut(cButtons, TDC, x64 ? 60 : 36, "UInt") ; cButtons
    NumPut(&pButtons, TDC, x64 ? 64 : 40, "Ptr") ; pButtons
    NumPut(2, TDC, x64 ? 72 : 44, "Int") ; nDefaultButton
    NumPut(&ExpandedText, TDC, x64 ? 100 : 64, "Ptr") ; pszExpandedInformation
    NumPut(270, TDC, x64 ? 156 : 92, "UInt") ; cxWidth

    DllCall("Comctl32.dll\TaskDialogIndirect", "Ptr", &TDC
        , "Int*", Button := 0
        , "Int*", Radio := 0
        , "Int*", Checked := 0)

    Return Button
}

ConfirmDisconnectXP() {
    Text := "Would you like to edit the generated code?`n`nSelect ""Disconnect"" do dissociate the GUI designer from the current tab.`nThe generated code can then be modified,`nbut the GUI designer will no longer be active for this tab.`n`nSelect ""Duplicate"" to duplicate the contents of this tab to a new tab,`nwhich is not associated with the GUI designer."
    Buttons := [[1, "Disconnect"], [3, "Duplicate"], [2, "Cancel"]]

    Result := SoftModalMessageBox(Text, "AutoAHK", Buttons, 3)
    Return {1: 101, 3: 102}[Result]
}

