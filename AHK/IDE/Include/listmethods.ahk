﻿test =
(
  Enter & Pause::
  Pause:: Pause
  ^Esc:: ExitApp
  Left::
  Left Up::
  Left Down::      ;<- false
  ^+l::
  ^+v::
  ^+,::
  
  :B:toralf::
  :B0:toralf::
  :B1:toralf::   ;<- false  
  :O:toralf::
  :O0:toralf::
  :O 0:toralf::   ;<- false
  :SI:toralf::
  :SP:toralf::
  :SE:toralf::
  :SZ:toralf::   ;<- false 
  :*?BCK-1OP1RSIZ:toralf::
  :*?B5C2KaO1P-1R1SLZ1:toralf::    ;<- false
)
msgbox,% Listmethods(test)


;x:=test)

return
ListMethods(scr, a1 = true, a2 = true, a3 = true) { ; by Titan
	; a1 = functions, a2 = labels, a3 = hotkeys (and hotstrings)
	; get comment character:
	c := RegExMatch(scr, "im)^#CommentFlag[ \t]+(.+?)[\s;]", c) ? c1 : ";", s = ","
	; remove single line and multiline comments:
	scr := "`n" . RegExReplace(RegExReplace(scr, "m)\s" . c . ".*$"), "sm)^\s*\/\*.*?^\s*\*\/")
	n = #_@\$\?\[\]\w ; list of characters that can appear in variable names
	scr := RegExReplace(scr, "s)\n[" . n . "]+\s*=\s*\n\(.*?\n\s*\)") ; remove cont'd vars
	r = ; custom regex definition table:
	( LTrim Com ; remove preceeding whitespace and comments
	\n\s*([%n%]+\([%n%\s=_\-,."']*\))[\n\s]*{``Function ; for function definitions
	m)^\s*([%n%\!.\\\/]+):\s*$``Label ; for labels (even inside functions)
	\n\s*((?::[\w\?\*]*:|[#!^+&<>\*\~\$]+)?[\w\t \&]*\w)::``Hotkey ; for hotkeys and hotstrings
	)
	Loop, Parse, r, `n ; parse through the list
		If (p := 1 and a%A_Index%) { ; reset p (start char.) to 1 and only continue if needed
			StringSplit, r, A_LoopField, `` ; split regex from type definition
			Loop ; since Chris hasn't implimented a global flag for regex yet :(
				If p := RegExMatch(scr, r1, t, p += StrLen(t)) ; find match
						v .= p . s . """" . RegExReplace(t1, """", """""") ; double quotes for well-formed CSV
							. """" . s . r2 . "`n" ; concat. as CSV
				Else Break ; stop loop if there are no more matches
		}	
	Return, v
}