﻿; Global variables
Global Version := "3.1.1"
	, g_Sourceurl:="https://autohotkey.oss-cn-qingdao.aliyuncs.com/AutoAHKScript/Scripts/autoahk"
    , g_DarkTheme
	, AppName := "AutoAHK"
	, hAutoWnd
	, hMainToolbar
	, hTab := 0
	, TabEx
	, TabExIL
	, Sci := []
	, SciLexer := A_ScriptDir . (A_PtrSize == 8 ? "\SciLexer-x64.dll" : "\SciLexer-x86.dll")
	, g_SciFontName
	, g_SciFontSize
	, IniFile
	, IconLib := A_ScriptDir . "\Icons\AutoAHK.icl"
	, IconMenu:= A_ScriptDir . "\Icons\AutoAHK.ico"
	, RecentFiles := []
	, hChildWnd := 0
	, Child := 1
	, hSelWnd := 0
    , hTabStyles
	, hGUIToolbar
	, hChildToolbar := 0
	, TB_CHECKBUTTON := 0x402
	, hFindReplaceDlg := 0
	, SearchString := ""
	, g_MatchCase
	, g_WholeWord
	, g_RegExFind
	, g_Backslash
	, g_RadStartingPos
	, g_ChkWrapAround
	, hFindCbx
	, hReplaceCbx
	, hFontDlg
	, hCloneDlg
	, hToolbarEditor := 0
	, m := New MenuBar
	, hMenuEditor := 0
	, hAddMenuItemDlg := 0
	, g_Script
	, g := New GuiClass
	, g_X := 0
	, g_Y := 0
	, g_Control := 0
	, g_TabIndex := 1
	, OpenDir
	, SaveDir
	, CodePage
	, Indent
	, g_TabSize
	, g_Insert := True
	, DesignMode:=0
	, ShowGrid
	, SnapToGrid
	, GridSize
	, g_WordWrap
	, g_SyntaxHighlighting
	, g_LineNumbers
	, g_AutoBrackets
	, SysTrayIcon
	, hToolbox

	, hPropWnd := 0
	, hCbxClassNN := 0
	, g_WinColor
	, hOptionsBtn

	, hBitmapTile
	, hCursorCross
	, hCursorDragMove
	, g_Cross := False
	, g_Parameters := ""
	, g_BackupOnSave
	, g_BackupDir
	, g_BackupDays
	, g_AutoSaveInterval
	, g_AutoSaveInLoco
	, g_AutoSaveInBkpDir
	, CRLF := "`r`n"
	, g_Signature := "; 河许人 " . AppName . " " . Version . CRLF
	, g_Delimiter := "; 不要编辑这行之前的内容!"
	, NoResizers
	, Resizers := []
	, hResizer1
	, hResizer2
	, hResizer3
	, hResizer4
	, hResizer5
	, hResizer6
	, hResizer7
	, hResizer8
	, hGrippedWnd
	, hReszdCtl
	, ResizerSize := 6
	, ResizerColor
	, ResizerBrush
	, Cursors := {}
	, g_Adding := False
	, NoReturn := False
	, g_NT6orLater := DllCall("GetVersion") & 0xFF > 5
	, g_TempFile := GetTempDir() . "\Temp.ahk"
	, g_IconPath := "shell32.dll"
	, g_PicturePath := A_MyDocuments
	, g_GuiTab := 0
	, g_GuiSB := False
	, g_AltAhkPath
	, g_ShowWhiteSpaces := False
	, g_AutoCList
	, g_AutoCEnabled
	, g_AutoCMinLength
	, g_AutoCMaxItems
	, g_AutoCXMLObj
	, g_Calltips
	, g_CalltipParams
	, g_CalltipParamsIndex := 1
	, g_HighlightActiveLine
	, g_HelpMenuXMLObj
	, g_IndentGuides
	, g_HighlightIdenticalText
	, g_CheckTimestamp
	, g_CodeFolding
	, g_IndentWithSpaces
	, g_AutoIndent
	, g_TabCounter := 1
	, g_IniTools
	, g_AhkPath3264
	, g_LButtonDown := 0
	, g_SessionsDir
	, g_LoadLastSession
	, g_RememberSession
	, g_DbgPort
	, g_DbgStatus := 0
	, g_DbgSession
	, g_DbgSocket
	, g_DbgStack
	, g_DbgLocalVariables
	, g_DbgGlobalVariables
	, g_Breakpoints := []
	, g_SymbolMargin
	, g_InitialX
	, g_InitialY
	, g_InitialW
	, g_InitialH
	, g_ToolbarH := 39
	, g_hStatusBar
	, g_StatusBarH
	, g_TabBarPos
	, g_TabBarStyle
	, g_SplitterW := 4
	, g_ErrorMarker := False
	, g_MouseCapture := 0
	, g_AskToSaveOnExit
	, g_MarkerBookmark := 0
	, g_MarkerBreakpoint := 1
	, g_MarkerDebugStep := 2
	, g_MarkerError := 3
	, hVarListWnd := 0
	, g_ReloadVarListOnEveryStep := 1
	, g_ShowIndexedVariables := 1
	, g_ShowObjectMembers := 1
	, g_HideReservedClassMembers := 1
	, g_DbgCaptureStderr := True
	, g_AttachDebugger := False
	, g_HelpFile
	, g_ShowAllFonts
	, g_FontList
	, g_hHiddenEdit
	, g_DarkTheme
	, g_MenuFuncs
	, g_MenuHandler
	, g_CaptureStdErr
	, g_ShowErrorSign
	, g_CaretWidth
	, g_CaretStyle
	, g_CaretBlink
	, g_Encodings := {"CP1252": "UTF-8 without BOM", "UTF-16": "UTF-16 LE", "UTF-8-RAW": "UTF-8 without BOM", "UTF-8": "UTF-8"}
	, g_MaxEncodings := 3
	, g_Lexers := {0: "Plain Text", 200: "AutoHotkey"}
	, g_ShellMenu1
	, g_ShellMenu2
	, g_ShellMenu2Pos := 7
	, g_pIContextMenu
	, g_pIContextMenu2
	, g_pIContextMenu3
	, g_hShellMenu := 0
	, g_hShellWnd := 0
    , g_oStyles :={}
    , g_PropX
, g_PropY
,g_Abort:="AutoAHK是中文网官方IDE（编辑器），由河许人开发，是一款非常棒的autohotkey编辑器，有窗口编辑器、代码生成器等特色功能，欢迎大家使用！"
