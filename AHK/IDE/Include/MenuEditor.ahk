﻿Class MenuBar {
    Code := ""

    Class Item {
        ParentID := 0
        DisplayName := ""
        Command := ""
        Items := []
    }

    Register(ParentID, ID, Text, Label, SubMenu := 0, Hotkey := "", Flags := 0, Icon := "", Index := 1) {
        this[ID] := New m.Item
        this[ID].ParentId := ParentId
        this[ID].DisplayName := Text
        this[ID].Command := Label
        this[ID].SubMenu := SubMenu
        this[ID].Hotkey := this.ConvertHotkey(Hotkey)
        this[ID].AhkHotkey := Hotkey
        this[ID].Text := DisplayName . ((Hotkey != "") ? "`t" . this[ID].Hotkey : "")
        this[ID].Flags := Flags
        this[ID].Icon := Icon
        this[ID].IconIndex := Index
        this[ParentID].Items.Push(ID)
    }

    ConvertHotkey(Hotkey) {
        StringUpper Hotkey, Hotkey, T
        StringReplace Hotkey, Hotkey, +, Shift+
        StringReplace Hotkey, Hotkey, ^, Ctrl+
        StringReplace Hotkey, Hotkey, !, Alt+
        StringReplace Hotkey, Hotkey, Shift+Ctrl, Ctrl+Shift
        Return Hotkey
    }
}

ShowMenuEditor:
    If (!WinExist("ahk_id " . hMenuEditor)) 
    {
        Gui MenuEditor: New, LabelMenuEditor hWndhMenuEditor +Resize +MinSize373x416
        SetWindowIcon(hMenuEditor, IconLib, 58)

        Gui Font, s9, Segoe UI

        Gui Add, Tab3
        , vMenuType gMenuTabHandler x8 y8 w251 h400 AltSubmit %g_ThemeFix%, 菜单栏|上下文菜单|托盘菜单

        Gui Tab, 1
        Gui Add, TreeView, hWndhTV1 vMenuTV1 gTreeViewHandler x15 y36 w236 h364 -E0x200
        Global MenuBarId := TV_Add("菜单栏", 0, "Bold")
        m.Register(0, MenuBarId, "菜单栏", "MenuBar", True)
        Gui Tab
        Gui Tab, 2
        Gui Add, TreeView, hWndhTV2 vMenuTV2 TreeViewHandler x15 y36 w236 h364 -E0x200
        Global ContextMenuId := TV_Add("上下文菜单", 0, "Bold")
        m.Register(0, ContextMenuId, "上下文菜单", "ContextMenu", True)
        Gui Tab
        Gui Tab, 3
        Gui Add, TreeView, hWndhTV3 vMenuTV3 TreeViewHandler x15 y36 w236 h340 -E0x200        
        Global TrayMenuId := TV_Add("托盘菜单", 0, "Bold")
        m.Register(0, TrayMenuId, "托盘菜单", "Tray", True)
        Gui Add, CheckBox, vNoStandard x18 y379 w234 h23, 移除所有标准托盘菜单(&s)
        Gui Tab

        Gui Add, GroupBox, x265 y21 w101 h111 Disabled +0x1000
        Gui Add, Button, gAddMenu x276 y37 w80 h23 Default, 添加菜单(&A)
        Gui Add, Button, gEditMenu x276 y67 w80 h23, 编辑(&E)
        Gui Add, Button, gRemoveMenu x276 y97 w80 h23, 移除(&R)

        Gui Add, GroupBox, x265 y132 w101 h80 Disabled +0x1000
        Gui Add, Button, gMoveItem x276 y148 w80 h23, 上移(&U)
        Gui Add, Button, gMoveItem x276 y178 w80 h23, 下移(&D)

        Gui Add, GroupBox, x265 y276 w101 h51 Disabled +0x1000
        Gui Add, Button, gMenuEditorContextMenu x276 y293 w80 h23, 预览(&P)

        Gui Add, GroupBox, x265 y328 w101 h80 Disabled +0x1000
        Gui Add, Button, gSetMenu x276 y345 w80 h23, 确定(&O)
        Gui Add, Button, gMenuEditorClose x276 y375 w80 h23, 取消(&C)
        Gui Font
        Gui Show, w373 h416, 菜单编辑器

        If (g_NT6orLater) 
        {
            Loop 3 
            {
                DllCall("UxTheme.dll\SetWindowTheme", "Ptr", hTV%A_Index%, "WStr", "Explorer", "Ptr", 0)
            }
        }
    }
    Else 
    {
        Gui MenuEditor: Show
    }
Return

MenuEditorEscape:
MenuEditorClose:
    Gui MenuEditor: Hide
Return

MenuTabHandler:
    Gui MenuEditor: Submit, NoHide
Return

AddMenu:
    Gui MenuEditor: Submit, NoHide
    Gui MenuEditor: +Disabled

    Gui TreeView, MenuTV%MenuType%
    SelectedID := TV_GetSelection()

    If (!m[SelectedID].SubMenu) {
        Gui MenuEditor: +OwnDialogs
        MsgBox 0x30, Menu Editor, 选择的项目不是一个菜单或子菜单.
        Gui MenuEditor: -Disabled
        Return
    }

    If (SelectedID == MenuBarId) {
        GoSub ShowAddMenuDlg
    } Else {
        GoSub ShowAddMenuItemDlg
    }
Return

EditMenu:
    Gui MenuEditor: Submit, NoHide
    Gui MenuEditor: +Disabled

    Gui TreeView, MenuTV%MenuType%
    SelectedId := TV_GetSelection()
    ParentId := TV_GetParent(SelectedId)

    If (SelectedId == MenuBarId || ParentId == MenuBarId || SelectedId == ContextMenuId) {
        GoSub ShowAddMenuDlg
    } Else If (SelectedId == TrayMenuId) {
        Gui MenuEditor: -Disabled
        Return
    } Else {
        GoSub ShowAddMenuItemDlg
    }
Return

ShowAddMenuDlg:
    If (A_GuiControl == "添加菜单(&A)") {
        Title := "添加菜单"
        DisplayName := "文件(&F)"
        MenuName := "文件菜单"
    } Else {
        Title := "编辑菜单"
        Id := TV_GetSelection()
        TV_GetText(DisplayName, Id)
        MenuName := m[Id].Command
    }

    Gui AddMenuDlg: New, +LabelAddMenuDlg -MinimizeBox +OwnerMenuEditor
    Gui Color, White
    Gui Font, s9, Segoe UI
    Gui Add, GroupBox, x8 y4 w284 h99
    Gui Add, Text, x19 y31 w79 h23, 菜单文本:
    Gui Add, Edit, vDisplayName gSuggestMenuName x109 y27 w170 h23, %DisplayName%
    Gui Add, Text, x19 y65 w81 h23, 菜单名字:
    Gui Add, Edit, vMenuName x109 y61 w170 h22, %MenuName%
    Gui Add, Text, x-1 y113 w302 h48 -Background +Border
    Gui Add, Button, gAddMenuOK x125 y126 w80 h24 +Default, 确定
    Gui Add, Button, gAddMenuDlgClose x212 y126 w80 h24, 取消
    Gui Show, w300 h160, %Title%
Return

SuggestMenuName:
    Gui AddMenuDlg: Submit, NoHide
    Suggestion := RegExReplace(DisplayName, "\W")
    GuiControl,, MenuName, % Suggestion . "菜单"
Return

AddMenuOK:
    Gui AddMenuDlg: Submit, NoHide
    WinGetTitle Title, A

    Gui MenuEditor: Default
    If (Title == "添加菜单") {
        Id := TV_Add(DisplayName, MenuBarId)
        m.Register(MenuBarId, Id, DisplayName, MenuName, True)
        TV_Modify(Id, "Select")
    } Else {
        Id := TV_GetSelection()
        TV_Modify(Id, "", DisplayName)
        m[Id].DisplayName := DisplayName
        m[Id].Command := MenuName
    }
AddMenuDlgEscape:
AddMenuDlgClose:
    Gui MenuEditor: -Disabled
    Gui AddMenuDlg: Hide
Return

; Menu Editor OK button
SetMenu:
    If (!WinExist("ahk_id" . hChildWnd)) {
        GoSub NewGUI
    }

    Gui %Child%: Default
    Global MenuCode := ""
    g_MenuFuncs := ""
    g_MenuHandler := False

    ; Menu bar
    m.Code := CreateMenu(m[MenuBarId].Items)
    If (m.Code != "") {
        MenuName := m[MenuBarId].Command
        Gui Menu, % MenuName
        m.Code .= "Gui Menu, " . MenuName . CRLF
    } Else {
        Gui %Child%: Menu
    }
    MenuCode := ""

    ; Context menu
    m.Context := CreateMenu(m[ContextMenuId].Items)
    If (m.Context != "") {
        g.Window.GuiContextMenu := True
        GuiControl Properties:, ChkGuiContextMenu, 1
    }
    m.Code .= m.Context
    MenuCode := ""

    ; Tray menu
    m.Tray := CreateMenu(m[TrayMenuId].Items)
    If (m.Tray != "") {
        Gui MenuEditor: Submit, NoHide
        If (NoStandard) {
            m.Tray := "Menu Tray, NoStandard" . CRLF . m.Tray
        }
    }
    m.Code .= m.Tray
    MenuCode := ""

    GenerateCode()

    Gui MenuEditor: Hide
Return

CreateMenu(Items) {
    For Each, Item in Items {
        Parent := m[m[Item].ParentId].Command

        If (m[Item].DisplayName == "----------") { ; Separator
            Menu, %Parent%, Add
            MenuCode .= "Menu " . Parent . ", Add" . CRLF
            Continue
        }

        If (m[Item].Items.MaxIndex() != "") {
            Try {
                Menu % m[Item].Command, Delete
            }
            CreateMenu(m[Item].Items)
            Command := ":" . m[Item].Command
        } Else {
            Command := m[Item].Command
        }

        If (m[Item].Hotkey != "") {
            If (m[Item].Hotkey == "Escape" || m[Item].Hotkey == "Delete") {
                m[Item].Hotkey := SubStr(m[Item].Hotkey, 1, 3)
            }
            MenuText := m[Item].DisplayName . "`t" . m[Item].Hotkey
            CodeText := m[Item].DisplayName . "``t" . m[Item].Hotkey
        } Else {
            MenuText := CodeText := m[Item].DisplayName
        }

        Options := (m[Item].Flags.Radio) ? "Radio" : ""

        Menu %Parent%, Add, %MenuText%, % (SubStr(Command, 1, 1) == ":") ? Command : "MenuHandler", %Options%
        MenuCode .= "Menu " . Parent . ", Add, " . CodeText . ", " . Command
        MenuCode .= (Options != "") ? ", " . Options : ""
        MenuCode .= CRLF

        If (m[Item].Flags.Disabled) {
            Menu %Parent%, Disable, %MenuText%
            MenuCode .= "Menu " . Parent . ", Disable, " . CodeText . CRLF
        } Else {
            Menu %Parent%, Enable, %MenuText%
        }
        If (m[Item].Flags.Checked) {
            Menu %Parent%, Check, %MenuText%
            MenuCode .= "Menu " . Parent . ", Check, " . CodeText . CRLF
        } Else {
            Menu %Parent%, Uncheck, %MenuText%
        }
        If (m[Item].Flags.Default) {
            Menu %Parent%, Default, %MenuText%
            MenuCode .= "Menu " . Parent . ", Default, " . CodeText . CRLF
        }

        If (m[Item].Icon != "") {
            Menu, %Parent%, Icon, %MenuText%, % m[Item].Icon, % m[Item].IconIndex
            MenuCode .= "Menu " . Parent . ", Icon, " . CodeText . ", " . m[Item].Icon
            If (m[Item].IconIndex != 1) {
                MenuCode .= ", " . m[Item].IconIndex
            }
            MenuCode .=  CRLF
        }

        ; Generate labels/functions
        If !(m[Item].SubMenu) {

            If !(g_MenuHandler && m[Item].Command == "MenuHandler") {
                g_MenuFuncs .= CRLF . m[Item].Command . ":" . CRLF . "Return" . CRLF                
            }

            If (m[Item].Command == "MenuHandler") {
                g_MenuHandler := True
            }
        }
    }

    Return MenuCode
}

; Preview
MenuEditorContextMenu:
    Gui MenuEditor: Submit, NoHide
    Gui TreeView, MenuTV%MenuType%

    If (MenuType == 1) {
        Try {
            Menu % m[MenubarId].Command, Delete
        }
        CreateMenu(m[MenuBarId].Items)
        Try {
            Menu % m[MenuBarId].Command, Show
        }
    } Else If (MenuType == 2) {
        Try {
            Menu % m[ContextMenuId].Command, Delete
        }
        CreateMenu(m[ContextMenuId].Items)
        Try {
            Menu % m[ContextMenuId].Command, Show
        }
    } Else {
        Menu Tray, DeleteAll
        If (NoStandard) {
            Menu Tray, NoStandard
        } Else {
            Menu Tray, Standard
        }
        CreateMenu(m[TrayMenuId].Items)
        Menu % m[TrayMenuId].Command, Show
    }
Return

ShowAddMenuItemDlg:
    ID := TV_GetSelection()

    If (A_GuiControl == "添加菜单(&A)") {
        Title := "Add Menu Item"
        DisplayName := ""
        Command := ""
        SubMenu := False
        Hotkey := ""
        Icon := ""
        IconIndex := ""
    } Else { ; &Edit
        Title := "Edit Menu Item"
        DisplayName := m[Id].DisplayName
        Command := m[Id].Command
        SubMenu := m[Id].SubMenu
        Hotkey := m[Id].AhkHotkey
        Icon := m[Id].Icon
        IconIndex := m[Id].IconIndex
    }

    Disabled := m[Id].Flags.Disabled ? True : False
    Checked := m[Id].Flags.Checked ? True : False
    Radio := m[Id].Flags.Radio ? True : False
    Bold := m[Id].Flags.Default ? True : False

    Gui AddMenuItemDlg: New, +LabelAddMenuItemDlg +hWndhAddMenuItemDlg +OwnerMenuEditor -MinimizeBox
    Gui Color, 0xFEFEFE
    Gui Font, s9, Segoe UI

    Gui Add, GroupBox, x10 y6 w384 h230
    Gui Add, Text, x25 y26 w75 h23 +0x200, 菜单文本:
    Gui Add, ComboBox, hWndhDisplayName vMenuDisplayName x102 y27 w200, <SEPARATOR>
    GuiControl Text, MenuDisplayName, %DisplayName%

    Gui Add, Text, vLabelOrSubMenu x25 y64 w75 h23 +0x200, % (SubMenu) ? "菜单名字:" : "标签:"
    Gui Add, Edit, hWndhCommandEdt vMenuCommand x102 y65 w200 h22, %Command%
    Gui Add, CheckBox, vChkSubMenu gChangeMenuType x310 y65 w75 h23 Checked%SubMenu%, 子菜单

    Gui Add, Text, x25 y102 w75 h23 +0x200, 键盘:
    Gui Add, Hotkey, vMenuHotkey x102 y104 w200 h22, %Hotkey%
    Gui Add, Button, gMenuClearHotkey x308 y103 w75 h23, 清除

    Gui Add, Text, x25 y144 w75 h23 +0x200, 图标路经:
    Gui Add, ComboBox, hWndhCbxMenuIcon vMenuIcon x102 y144 w200, shell32.dll|imageres.dll
    SendMessage 0x0153, -1, 16,, ahk_id%hCbxMenuIcon% ; CB_SETITEMHEIGHT
    GuiControl Text, MenuIcon, %Icon%
    Gui Add, Edit, vMenuIconIndex x308 y144 w48 h22, %IconIndex%
    Gui Add, Button, gChooseMenuIcon x360 y143 w23 h22, ...

    Gui Add, GroupBox, x10 y178 w384 h58, 状态
    Gui Add, CheckBox, vMenuChecked x42 y199 w75 h23 Checked%Checked%, 多选框(&k)
    Gui Add, CheckBox, vMenuRadio x129 y199 w75 h23 Checked%Radio%, 单选框(&R)
    Gui Add, CheckBox, vMenuDefault x214 y199 w75 h23 Checked%Bold%, 默认(&e)
    Gui Add, CheckBox, vMenuDisabled x305 y199 w73 h23 Checked%Disabled%, 禁用(&D)

    Gui Add, Text, x-1 y250 w408 h48 -Background +Border

    If (A_GuiControl == "添加菜单(&A)") {
        Gui Add, Button, gNextMenuItem x142 y262 w80 h24, 下一个(&N)
    }

    Gui Add, Button, gAddMenuItem x229 y262 w80 h24, 确定(&O)
    Gui Add, Button, gAddMenuItemDlgClose x316 y262 w80 h24, 取消(&C)

    GuiControl +Default, % A_GuiControl == "&Add Menu" ? "下一个(&N)" : "确定(&O)"

    Gui Add, Button, hWndhBtnExamples gShowExamplesMenu x308 y26 w75 h23, 例子
    If (g_NT6orLater) {
        Control Style, +0xC,, ahk_id%hBtnExamples% ; BS_SPLITBUTTON
    }

    Gui Show, w405 h297, %Title%
Return

AddMenuItemDlgEscape:
AddMenuItemDlgClose:
    Gui MenuEditor: -Disabled
    Gui AddMenuItemDlg: Hide
Return

; Add Menu Item dialog OK/Next button
AddMenuItem:
NextMenuItem:
    Gui AddMenuItemDlg: Submit, NoHide

    If (MenuCommand == "") {
        If (ChkSubMenu) {
            Edit_ShowBalloonTip(hCommandEdt, "此字段不能留空")
            Return
        } Else {
            MenuCommand := "MenuHandler"
        }
    }

    If (ChkSubMenu && !IsMenuNameAvailable(MenuCommand)) {
        Message := "名字为 """ . MenuCommand . """ 的菜单已经存在."
        Edit_ShowBalloonTip(hCommandEdt, Message, "无效的菜单名", 3)
        Return
    }

    If (A_ThisLabel == "AddMenuItem") {
        Gui MenuEditor: -Disabled
        Gui AddMenuItemDlg: Hide
    }

    If (MenuDisplayName == "") {
        Return
    }

    If (MenuDisplayName == "-" || MenuDisplayName == "<SEPARATOR>") {
        MenuDisplayName := "----------"
    }

    Flags := {"Checked" : MenuChecked
            , "Radio"   : MenuRadio
            , "Default" : MenuDefault
            , "Disabled": MenuDisabled}

    Gui MenuEditor: Default

    WinGetTitle Title, ahk_id %hAddMenuItemDlg%

    If (Title == "Add Menu Item") {
        ParentId := TV_GetSelection()
        Id := TV_Add(MenuDisplayName, ParentId)

        m.Register(TV_GetParent(Id), Id, MenuDisplayName, MenuCommand, ChkSubMenu, MenuHotkey, Flags, MenuIcon, MenuIconIndex)

        If (m[ParentId].Command == "MenuHandler") {
            m[ParentId].Command := RegExReplace(m[ParentId].DisplayName, "\W") . "Menu"
        }

        TV_Modify(ParentId, "Expand")
    } Else {
        Id := TV_GetSelection()
        TV_Modify(Id, "", MenuDisplayName)
        m[Id].DisplayName := MenuDisplayName
        m[Id].Command := MenuCommand
        m[ID].SubMenu := ChkSubMenu
        m[Id].Hotkey := m.ConvertHotkey(MenuHotkey)
        m[Id].AhkHotkey := MenuHotkey
        m[Id].Text := MenuDisplayName . ((MenuHotkey != "") ? "`t" . MenuHotkey : "")
        m[Id].Icon := MenuIcon
        m[Id].IconIndex := MenuIconIndex
        m[Id].Flags := Flags
    }

    ResetMenuItemDlg()

    If (A_ThisLabel == "NextMenuItem") {
        ControlFocus,, ahk_id %hDisplayName%
    }
Return

ChooseMenuIcon:
    Gui AddMenuItemDlg: Submit, NoHide
    IconResource := (MenuIcon != "") ? MenuIcon : g_IconPath
    IconIndex := MenuIconIndex
    If (ChooseIcon(IconResource, IconIndex, hAddMenuItemDlg)) {
        StringReplace IconResource, IconResource, %A_WinDir%\System32\
        GuiControl AddMenuItemDlg: Text, MenuIcon, %IconResource%
        GuiControl AddMenuItemDlg:, MenuIconIndex, %IconIndex%
        g_IconPath := IconResource
    }
Return

RemoveMenu:
    Gui MenuEditor: Submit, NoHide
    Gui TreeView, MenuTV%MenuType%

    Gui MenuEditor: Default

    Id := TV_GetSelection()
    If (Id == 0 || Id == MenuBarId || Id == ContextMenuId || Id == TrayMenuId) {
        ParentId := Id
        Items := m[Id].Items
    } Else {
        ParentId := TV_GetParent(Id)
        Items := m[ParentId].Items
    }

    If (m[Id].Items.MaxIndex() == "") {
        For Each, Item in Items {
            If (Items[A_Index] = Id) {
                m[ParentId].Items.Remove(A_Index)
                If (m[Id].DisplayName == "----------") {
                    If (MenuType == 1) {
                        Try {
                            Menu % m[MenubarId].Command, Delete
                        }
                        CreateMenu(m[MenuBarId].Items)
                    } Else If (MenuType == 2) {
                        Try {
                            Menu % m[ContextMenuId].Command, Delete
                        }
                        CreateMenu(m[ContextMenuId].Items)
                    } Else {
                        Menu Tray, DeleteAll
                        CreateMenu(m[TrayMenuId].Items)
                    }
                } Else {
                    Try {
                        Menu, % m[ParentId].Command, Delete, % m[Id].Text
                    }
                }
            }
        }
    } Else { ; Submenu
        For Index, Item in Items {
            If (m[ParentId].Items[Index] = Id) {
                m[ParentId].Items.Remove(Index)
                Menu, % m[ParentId].Command, Delete, % m[Id].Text
            }
        }
    }

    If (TV_GetParent(Id) == 0) {
        If (MenuType == 1) {
            MenuBarId := TV_Add("Menu Bar", 0, "Bold")
            m.Register(0, MenuBarId, "Menu Bar", "MenuBar", True)
        } Else If (MenuType == 2) {
            ContextMenuId := TV_Add("Context Menu", 0, "Bold")
            m.Register(0, ContextMenuId, "Context Menu", "ContextMenu", True)
        } Else {
            TrayMenuId := TV_Add("Tray Menu", 0, "Bold")
            m.Register(0, TrayMenuId, "Tray Menu", "Tray", True)
        }
    }

    TV_Delete(Id)
Return

MoveItem:
    Gui MenuEditor: Submit, NoHide

    Gui TreeView, MenuTV%MenuType%
    Item := TV_GetSelection()
    TV_GetText(ItemText, Item)
    Parent := TV_GetParent(Item)

    If (A_GuiControl = "上移(&U)") {
        Prev := TV_GetPrev(Item)
        N := TV_GetPrev(Prev)

        If (Parent == 0) {
            Return
        } Else {
            FirstSib := TV_GetChild(Parent)
        }

        If (FirstSib = Item) {
            Return
        }
    } Else If (A_GuiControl = "下移(&D)") {
        N := TV_GetNext(Item)
        If (!N) {
            Return
        }
    }

    TV_Delete(Item)

    If (A_GuiControl = "上移(&U)" && Prev = FirstSib) {
        Id := TV_Add(ItemText, Parent, "First")
    } Else {
        Id := TV_Add(ItemText, Parent, N)
    }

    MaxIndex := m[Id].Items.MaxIndex()

    If (MaxIndex != "") {
        Items := []
        For Each, Item in m[Id].Items {
            Items.Insert(m[Id].Items[MaxIndex - A_Index + 1])
        }

        For Each, Item in Items {
            TV_Add(m[Item].DisplayName, Id, "First")
        }

        TV_Modify(Id, "Expand")
    }

    TV_Modify(Id, "Select")

    MaxIndex := m[Parent].Items.MaxIndex()

    Loop % MaxIndex {
        If (m[Parent].Items[A_Index] == Id) {
            m[Parent].Items.Remove(A_Index)
            Pos := A_Index
            Break
        }
    }

    Temp := []

    NewPos := (A_GuiControl == "上移(&U)") ? (Pos - 1) : Pos

    Loop % m[Parent].Items.MaxIndex() {
        If (NewPos = Pos) {
            Temp.Insert(m[Parent].Items[A_Index])
        }

        If (A_Index == NewPos) {
            Temp.Insert(Id)
        }

        If (NewPos != Pos) {
            Temp.Insert(m[Parent].Items[A_Index])
        }
    }

    m[Parent].Items := Temp
Return

ShowExamplesMenu:
    Gui +LastFound

    Try {
        Menu ExamplesMenu, DeleteAll
    }

    AddMenu("ExamplesMenu", "打开(&O)`tCtrl+O", "LoadMenuExample", "shell32.dll", 5)
    Menu ExamplesMenu, Add, 保存(&S)`tCtrl+S, LoadMenuExample
    Menu ExamplesMenu, Icon, 保存(&S)`tCtrl+S, shell32.dll, % (g_NT6orLater) ? 259 : 7
    Menu ExamplesMenu, Add
    AddMenu("ExamplesMenu", "复制(&C)`tCtrl+C", "LoadMenuExample")
    AddMenu("ExamplesMenu", "全选(&A)`tCtrl+A", "LoadMenuExample")
    Menu ExamplesMenu, Add
    Menu ExamplesMenu, Add, 重载(&R)`tCtrl+R, LoadMenuExample
    If (g_NT6orLater) {
        Menu ExamplesMenu, Icon, 重载(&R)`tCtrl+R, shell32.dll, 239
    }
    Menu ExamplesMenu, Add
        AddMenu("ExamplesSubMenu", "默认", "LoadMenuExample")
        Menu ExamplesSubMenu, Default, 默认
        AddMenu("ExamplesSubMenu", "多选框", "LoadMenuExample")
        Menu ExamplesSubMenu, Check, 多选框
        Menu ExamplesSubMenu, Add, 单选框, LoadMenuExample, Radio
        Menu ExamplesSubMenu, Check, 单选框
        AddMenu("ExamplesSubMenu", "禁用", "LoadMenuExample")
        Menu ExamplesSubMenu, Disable, 禁用
    AddMenu("ExamplesMenu", "状态", ":ExamplesSubMenu")
    Menu ExamplesMenu, Add
    AddMenu("ExamplesMenu", "设置", "LoadMenuExample", "shell32.dll", 166)
    Menu ExamplesMenu, Add
    AddMenu("ExamplesMenu", "帮助(&H)`tF1", "LoadMenuExample", "shell32.dll", 24)
    Menu ExamplesMenu, Add
    AddMenu("ExamplesMenu", "退出(&x)`tEsc", "LoadMenuExample")

    ControlGetPos x, y,, h,, ahk_id %hBtnExamples%
    Menu ExamplesMenu, Show, %x%, % (y + h)
Return

LoadMenuExample:
    ResetMenuItemDlg()

    If (A_ThisMenuItem == "打开(&O)`tCtrl+O") {
        GuiControl Text, MenuDisplayName, 打开(&O)
        GuiControl,, MenuHotkey, ^O
        GuiControl Text, MenuIcon, shell32.dll
        GuiControl,, MenuIconIndex, 4
    } Else If (A_ThisMenuItem == "保存(&S)`tCtrl+S") {
        GuiControl Text, MenuDisplayName, 保存(&S)
        GuiControl,, MenuHotkey, ^S
        GuiControl Text, MenuIcon, shell32.dll
        GuiControl,, MenuIconIndex, % (g_NT6orLater) ? 259 : 7
    } Else If (A_ThisMenuItem == "复制(&C)`tCtrl+C") {
        GuiControl Text, MenuDisplayName, &Copy
        GuiControl,, MenuHotkey, ^C
    } Else If (A_ThisMenuItem == "全选(&A)`tCtrl+A") {
        GuiControl Text, MenuDisplayName, 全选(&A)
        GuiControl,, MenuHotkey, ^A
    } Else If (A_ThisMenuItem == "重载(&R)`tCtrl+R") {
        GuiControl Text, MenuDisplayName, 重载(&R)
        GuiControl,, MenuHotkey, ^R
        If (g_NT6orLater) {
            GuiControl Text, MenuIcon, shell32.dll
            GuiControl,, MenuIconIndex, 239
        }
    } Else If (A_ThisMenuItem == "默认") {
        GuiControl Text, MenuDisplayName, 默认
        GuiControl,, MenuDefault, 1
    } Else If (A_ThisMenuItem == "多选框") {
        GuiControl Text, MenuDisplayName, 多选框
        GuiControl,, MenuChecked, 1
    } Else If (A_ThisMenuItem == "单选框") {
        GuiControl Text, MenuDisplayName, 单选框
        GuiControl,, MenuChecked, 1
        GuiControl,, MenuRadio, 1
    } Else If (A_ThisMenuItem == "设置") {
        GuiControl Text, MenuDisplayName, 设置
        GuiControl Text, MenuIcon, shell32.dll
        GuiControl,, MenuIconIndex, 166
    } Else If (A_ThisMenuItem == "帮助(&H)`tF1") {
        GuiControl Text, MenuDisplayName, 帮助(&H)
        GuiControl,, MenuHotkey, F1
        GuiControl Text, MenuIcon, shell32.dll
        GuiControl,, MenuIconIndex, 24
    } Else If (A_ThisMenuItem == "退出(&x)`tEsc") {
        GuiControl Text, MenuDisplayName, 退出(&x)
        GuiControl,, MenuHotkey, Esc
    }
Return

ResetMenuItemDlg() {
    Gui AddMenuItemDlg: Default
    GuiControl, Text, MenuDisplayName
    GuiControl,, MenuCommand
    GuiControl,, ChkSubMenu, 0
    GuiControl,, MenuHotkey
    GuiControl, Text, MenuIcon
    GuiControl,, MenuIconIndex
    GuiControl,, MenuChecked, 0
    GuiControl,, MenuRadio, 0
    GuiControl,, MenuDefault, 0
    GuiControl,, MenuDisabled, 0
}

IsMenuNameAvailable(MenuName) {
    For Menu in m {
        For Each, Item in m[Menu].Items {
            If (m[Item].SubMenu) {
                If (m[Item].Command = MenuName) {
                    Return False
                } Else {
                    OutputDebug % "*" m[Item].Command . " != " . MenuName
                }
            }
        }
    }
    Return True
}

TreeViewHandler:
    Gui MenuEditor: Submit, NoHide
    Gui TreeView, MenuTV%MenuType%
Return

MenuEditorSize:
    If (A_EventInfo == 1) {
        Return
    }

    AutoXYWH("wh", "SysTabControl321", "SysTreeView321", "SysTreeView322", "SysTreeView323")
    GuiControlGet, hButton1, hWnd, Button1
    ControlGetPos,, cy,, ch,, ahk_id %hButton1%
    hParent := GetParent(hButton1)
    ControlGetPos dx, dy, dw, dh,, ahk_id %hParent%
    GuiControl MoveDraw, %hButton1%, % "y" . (dh - ch - 2)
    AutoXYWHSize := "*x"
    Loop 12 {
        AutoXYWH(AutoXYWHSize, "Button" . (A_Index + 1))
        If (A_Index == 7) {
            AutoXYWHSize := "*xy"
        }
    }
Return

ChangeMenuType:
    Gui AddMenuItemDlg: Default
    GuiControlGet ChkSubMenu
    GuiControl,, LabelOrSubMenu, % ChkSubMenu ? "Menu Name:" : "Label:"
Return

MenuClearHotkey:
    GuiControl,, MenuHotkey
Return
