﻿test =
(
  Run, hh %AHKManualFile%::/docs/misc/Styles.htm#Common   ;<- false 
  Run,hh %AHKManualFile%::/docs/misc/Styles.htm#Common    ;<- false
  ::                                                   ;<- false
  Enter & Pause::
  Pause:: Pause
  ^Esc:: ExitApp
  Left::
  Left Up::
  Left Down::      ;<- false
  ^+l::
  ^+v::
  ^+,::
  
  :B:toralf::
  :B0:toralf::
  :B1:toralf::   ;<- false  
  :O:toralf::
  :O0:toralf::
  :O 0:toralf::   ;<- false
  :SI:toralf::
  :SP:toralf::
  :SE:toralf::
  :SZ:toralf::   ;<- false 
  :*?BCK-1OP1RSIZ:toralf::
  :*?B5C2KaO1P-1R1SLZ1:toralf::    ;<- false
)

Loop, Parse, Test, `n
{
  line := A_LoopField
  If RegExMatch(line, "^\s*[^ \t:]+?( & [^ \t:]+?| [uU][pP])?::")
      String = %String%`nHotkey -> %Line%
  Else If RegExMatch(line, "i)^\s*:(\*0?|\?0?|B0?|C[01]?|K(-1|\d+)|O0?|P\d+|R0?|S[IPE]|Z0?)*:.+?::")
      String = %String%`nHotString -> %Line%
  Else
      String = %String%`nnothing -> %Line%
}
MsgBox, done:%String%