﻿;窗口特定的热键和热串  指定工作范围!----相当有用了!

; 无标题 - 记事本
#IfWinActive 无
!q::
MsgBox, You pressed Alt+Q in Notepad.
Return

#IfWinActive vi
!q::
MsgBox, You pressed Alt+Q in ahk!!!.
Return

; 任何不是无标题 - 记事本的窗口
#IfWinActive
!q::
MsgBox, You pressed Alt+Q in any window.
Return





; 激活现有的 notepad.exe 窗口或打开新窗口----不严格单例!
!w::
if WinExist("ahk_exe notepad.exe") ;作用和#IfWinExist一样吧!
    WinActivate, ahk_exe notepad.exe
else
    Run, notepad.exe
return