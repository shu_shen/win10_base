﻿;创建对象
;方括号语法
MyObject := ["one", "two", "three", 17]
;这将从有时被称为"索引数组"的内容开始. 索引数组是一个表示项目列表的对象, 索引号从 1 开始连续递增. 在本例中, 值 "one" 存储在对象键 1(又叫做索引号1), 值 17 存储在对象键 4(又叫做索引号 4).

;大括号语法
Banana := {"Color": "Yellow", "Taste": "Delicious", "Price": 3}
;这将通过定义有时被称为"关联数组"来开始. 关联数组是数据的集合, 其中每个条目都有自己的名称. 在这个例子中, 值 "Yellow" 存储在对象键 "Color" 中. 同样的, 值 3 存储在对象键 "Price" 中.

;数组函数
MyObject := Array("one", "two", "three", 17)
;这种方式跟方括号语法形式一样, 区别仅仅是采用了函数的形式.

;对象函数
Banana := Object("Color", "Yellow", "Taste", "Delicious", "Price", 3)
;这种方式跟大括号语法形式一样, 区别仅仅是采用了函数的形式.

;update
Banana["Pickled"] := True  ;这个香蕉烂透了!
Banana.Consistency := "Mushy" 

;select
Value := Banana.Color
Value := Banana["Taste"]
MsgBox %Value%

;insert
;方括号表示法和句点表示法  想要直接添加一对键和值, 只需设置一个尚不存在的键即可.
MyObject["NewerKey"] := 3.1415
MyObject.NewKey := "Shiny"

;InsertAt(在..插入) 法
MyObject.InsertAt(Index, Value1, Value2, Value3...)
;Index(索引) 为任意整数键. 这会将所有更高的整数键的索引向上移动插入值的数量, 即使是空缺的也一样(例如, 假设只有键 1 和 100 存在, 当插入一个值到第 50 个键的位置时, 将导致原来位于 100 的键的索引变成 101).

;Push(推送) 法
MyObject.Push(Value1, Value2, Value3...)
;"追加" 值到数组 MyObject 的尾部. 换句话说, 它将插入的值放在 最高整数键 + 1 的位置.


;delete
;用空白填充值
Banana.Consistency := ""
;最简单的删除值的方法就是用空白填充. 你可以将其赋值为 ""(两个连续的双引号), 也就是常说的 空字符串. 这不会删除键, 但是它将使值看起来与从未赋值一样. 可以通过使用 HasKey 方法得知键依然存在, 而且键也会出现在 For 循环中. (我们一会再解释 For 循环)

;删除法
RemovedValue := MyObject.Delete(AnyKey)
;这和接下来的方法将删除键 和 值. MyObject[AnyKey] 先前的值将存储在 RemovedValue 中.

NumberOfRemovedKeys := MyObject.Delete(FirstKey, LastKey)
;允许你删除 FirstKey 和 LastKey 之间的一系列的编号/整数或字符串键. 它给出的值将是被删除的键的数量, 如果你的键之间有间隙, 这是有用的(例如你指定键从 1 到 4, 但是键 2 不存在, 这将设置 NumberOfRemovedKeys 为 3, 因为只有三个键被移除).

;Pop(抛出) 法
MyObject.Pop()
;这将删除最高的整数键, 并返回这个键对应的值. 而且这种形式不会影响其他键的索引顺序(因为没有比它更高的序列了).

;RemoveAt(在..删除) 方法
RemovedValue := MyObject.RemoveAt(Index)
NumberOfRemovedKeys := MyObject.RemoveAt(Index, Length)
;这将移除从 Index 到 Index + Length - 1(包含) 之间的所有键(例如 Index 为 5, Length 为 3, 则将删除键 5, 6, 7 这三个键). 如果 Length 省略则默认为 1. 移除这些键之后, 更高数字或整数的键将向下填充空缺, 所以如果有个值位于 Index + Length 那么它现在的位置就是 Index. 这很像 InsertAt 方法操作多个指定值的操作.
