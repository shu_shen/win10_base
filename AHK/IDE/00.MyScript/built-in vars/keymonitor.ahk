#UseHook
#InstallKeybdHook
global record :=
global lastKey :=

while 1{
    ;ToolTip % A_PriorKey ; 先前的,上次的!最后一次按键!
    if(lastKey != A_Priorkey){
        record .= A_Priorkey
        lastKey := A_Priorkey
    }
    ToolTip,
     (
     %A_ThisHotkey%
    %A_PriorHotkey%
    %A_Priorkey%
    %A_TimeSinceThisHotkey%
    %A_TimeSincePriorHotkey%
    %A_EndChar%
    %lastKey%
    %record%
)

    Sleep 100 ;没有这个的话对cpu来说就是噩梦
}

#e::return
#q::
return

#w::
return

capslock::
return