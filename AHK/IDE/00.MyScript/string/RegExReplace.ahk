MsgBox % RegExReplace("abc123123", "123$", "xyz")  ; Shows "abc123xyz" because the $ allows a match only at the end.
MsgBox % RegExReplace("abc123", "i)^ABC")  ; Shows "123" because a match was achieved via the case-insensitive option.
MsgBox % RegExReplace("abcXYZ123", "abc(.*)123", "aaa$1zzz")  ; Shows "aaaXYZzzz" by means of the $1 backreference.
MsgBox % RegExReplace("abc123abc456", "abc\d+", "", ReplacementCount)  ; Shows "" and stores 2 in ReplacementCount.