MsgBox % RegExMatch("xxxabc123xyz", "abc.*xyz")  ; Shows 4, which is the position where the match was found.
MsgBox % RegExMatch("abc123123", "123$")  ; Shows 7 because the $ requires the match to be at the end.
MsgBox % RegExMatch("abc123", "i)^ABC")  ; Shows 1 because a match was achieved via the case-insensitive option.
MsgBox % RegExMatch("abcXYZ123", "abc(.*)123", SubPat)  ; Shows 1 and stores "XYZ" in SubPat1.
MsgBox % RegExMatch("abc123abc456", "abc\d+", "", 2)  ; Shows 7 instead of 1 due to StartingPosition 2 vs. 1.

if(RegExMatch("xxxabc123xyz", "abc.*xyz") ==1){
    MsgBox, good
}

Haystack := "This is a good book."
FoundPos := RegExMatch(Haystack, "bo{0,}", Match)
MsgBox, % "FoundPos: " FoundPos "`n" "Match: " Match