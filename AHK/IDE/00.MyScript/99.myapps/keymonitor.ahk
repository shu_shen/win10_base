#UseHook
#InstallKeybdHook
global record :=
global lastKey :=
runFlag := 0

while 1{
    if(lastKey="PrintScreen"){
        runFlag := 1
    }
    ;ToolTip % A_PriorKey ; 先前的,上次的!最后一次按键!
    if(lastKey = "CapsLock" or lastKey = "/"){
        runFlag := 0
        record :=
    }
    if(lastKey != A_Priorkey){
        if(lastKey="PrintScreen"){
            record = ":"
        }else{
            record .= A_Priorkey
        }
        lastKey := A_Priorkey
    }
    if(runFlag = 1){
        ToolTip, %record%
    }else{
        ToolTip
    }
    Sleep 100 ;没有这个的话对cpu来说就是噩梦
}

#e::return
#q::
return

#w::
return

capslock::
return