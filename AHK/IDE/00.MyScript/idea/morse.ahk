Morse(timeout = 1000) { ;
   tout := timeout/1000 ;毫秒转秒
   key := RegExReplace(A_ThisHotKey,"[\*\~\$\#\+\!\^]") ;
   ToolTip key=%key%  A_ThisHotKey=%A_ThisHotKey%
   Loop {
      t := A_TickCount
      KeyWait %key%
      Pattern .= A_TickCount-t > timeout ;todo 从现象来看.=是追加的意思!
      ;Pattern := Pattern[A_TickCount-t > timeout]
      ToolTip pattern=%Pattern%
      KeyWait %key%,DT%tout% ;https://www.autohotkey.com/docs/commands/KeyWait.htm
      If (ErrorLevel) ;如果超时才结束返回,否则继续循环!
         Return Pattern
   }
}

BS::MsgBox % "Morse press pattern " Morse()

!z::
   p := Morse()
   If (p = "0")
      MsgBox Short press
   Else If (p = "00")
      MsgBox Two short presses
   Else If (p = "01")
      MsgBox Short+Long press
   Else
      MsgBox Press pattern %p%
Return