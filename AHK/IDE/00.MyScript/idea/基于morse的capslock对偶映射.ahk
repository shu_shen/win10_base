SetNumLockState, on
SetScrollLockState, AlwaysOff
; refrence https://autohotkey.com/board/topic/15574-morse-find-hotkey-press-and-hold-patterns/
; ; 單擊 LControl 視為 caps 其它情況視為 ctrl

~RControl::
    p := Morse()
    If (p = "0"){ ; 單擊切換 CapsLock 狀態
        Send, {ESC}
    }
    Else { ; 長按視為 LControl
        Send, {RControl}
    }
Return

Morse(timeout = 250) {
    tout := timeout/1000
    key := RegExReplace(A_ThisHotKey,"[\*\~\$\#\+\!\^]")
    Loop {
        t := A_TickCount
        KeyWait %key%
        Pattern .= A_TickCount-t > timeout
        KeyWait %key%,DT%tout%
        If (ErrorLevel)
            Return Pattern
    }
}