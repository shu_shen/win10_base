GetKeyState, state, RButton  ; Right mouse button.
GetKeyState, state, Joy2  ; The second button of the first joystick.

GetKeyState, state, LShift
if (state = "D")
    MsgBox LShift key is down.
else
    MsgBox LShift key is up.

GetKeyState, state, CapsLock, T ;  D if CapsLock is ON or U otherwise.
if (state = "D")
    MsgBox CapsLock key is down.
else
    MsgBox Capslock key is up.