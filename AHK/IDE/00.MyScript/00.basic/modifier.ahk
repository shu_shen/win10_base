; modifier 包括ctrl,win,alt,shift
;单独的modifier key不可以作为热键(::前头的)!

; modifier key symbol只能组合使用,单独使用时表示字符串本身
^::
tooltip this is not ctrl!
return

;一个热键可以是 单键, 修饰符集+ 单键(up) , 双键(up)
;双键 使用时 , prefix key 指的是除了修饰符以外的按键,比如tab,capslock
; 如果存在双键的 prefix key 同时单独作为热键时, 相当于自动添加 up

rctrl up::Send {esc}

:*:edge/::
Send {lwin down}1{lwin up}{enter}