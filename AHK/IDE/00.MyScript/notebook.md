# 注意
适度即可,autohotkey功能是很强,可是毕竟不是研究这个的,把vimd搞的会用就行了!
剩下的屏幕,窗口,控件知识感兴趣或者真的很有兴趣就另外再学好了!
autohotkey 的 语言设计有点恶心,capslock大写锁定很多时候也没有那么一无是处!
limit is also a kind of ability!
# 明确需求反而是更重要的一件事情,如果这个没想清楚的话,很多事情都白搭!
hotstring可以做的事情,剪贴版复制

1. vimd降低软件快捷键使用的记忆负担!
2. vimd对于调用外部程序可以直接使用快捷键!
好好发挥自定义热键和热串的效果!
热串模板!!!

# 散记
## 热键修饰标志位
https://www.autohotkey.com/docs/Hotkeys.htm#Tilde

^ # ! +
& 前后缀键 ----自定义热键的精华


## 热键(hotkey)----组合键(同时按下)
1.一个热键的组成可以是下面3种情况:
- 单键(up)
    1. `别注意在ahk中分号是注释的意思,所以要是作为单键或者修饰键的话需要加`转义
    2. 修饰符symbol不能作为单键,要想让修饰键作为单键必须写具体,可以是ctrl,lctrl,rctrl(alt,shift 同理),LWin,RWin(Win是不可以的,奇怪!)
- 修饰符集+ 单键(up) 
    - 修饰符键不可能作为单键使用,换言之,单键是除修饰符键以外的其他键.比如
    ```text
        ^::return ;指字符
        {ctrl}::return ;错误:{} 只可以做为Send的转移字符被解析
     ```
    - 修饰符键不能作为双键的元素,换言之,双键的prefix key和suffix key都只能是非修饰符键
- 双键(up)
    双键 使用时 , prefix key 指的是除了修饰符以外的按键,比如tab,capslock
    如果存在双键的 prefix key 同时单独作为热键时, 相当于自动添加 up
    prefix key不要使用!@<大写字母这些,因为就相当于shift了,我觉得没有必要!
2. 一个ahk文件不能拥有两组相同的hotkey or hotstring
不同的ahk如果有两组相同的hotkey,取决于他们的启动顺序,后发者将会覆盖前者!
## 字符串
::btw:: 和 :*:btw::的 区别:涉及一个endchar的概念!
## Send命令
首先要明白最基础的几个概念:SendEvent,SendInput,SendPlay + SendRaw
SendRaw和其它几个命令的区别在于你在他后面写什么,他会原样输出
其它3种会对特殊字符(如(#!+)#!+)和转义字符{}进行解析
其它3个暂且略过,我们最常用的可能是Send命令,默认相当SendEvent,可以通过SendMode修改成SendInput,SendPlay和SendEvent
## symbol && {} in hotkey&&Send && Command
- hotkey:
    只解析symbol
    `作为转义字符转义分号,但是 `本身不必转义!
- SendXxx
    解析symbol 和 {}
- 其它Command(我目前的了解也许有局限)
    不解析
## 功能
# 全局禁用LWin和RWin键(easy)
# 全局禁用ctrl,alt,shift键(?)
# 全局禁用其它单键(?)
# CapsLock的对偶映射()
   *^f:: tooltip * + win+c
   $+^f:: send +^f  ;
   把f拓展到所有常用键位即可!(也不行,这个难度有点大!)


#源码阅读
1.setAction(actionName, comment) ;vim实例注册行为
  setAction(labelName,comment) ;其实是一个方法,只是如果actionName有对应的标签,那么actionName就是labelName了,这时候就不需要setFunction了
2.setFunction(functionName) ;为行为注册函数

map(hotkey,actionName,winName) ;对指定win 按下 hotkey 会触发 action中定义的function!
map(hotkey,labelName,winName) ;对指定win 按下 hotkey 会触发 label中定义的function!

tips: 只有action在没有绑定function的时候,label跳转才生效

setMode相当于断点!setMode(1) 和 setMode(2)之间的 map才有效!