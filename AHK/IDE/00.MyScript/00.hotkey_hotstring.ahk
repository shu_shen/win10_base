﻿;(1)Hotkey
;https://wyagd001.github.io/zh-cn/docs/KeyList.htm
^j::MsgBox, Hello,world! 
return

;Hotstring
::hs0::Free the whales 
return

::hs1::
MsgBox, You typed btw.
return

::hs2::
Send, You typed hs3 ;vs hs0_:有无Send的区别,只是纯字符的话,无所谓哪个,要是发送一些特殊按键比如{L-Ctrl},那还是需要加Send的
return
