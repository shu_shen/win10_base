;=================================================
;作         者:  河许人
;程序说明：超级消息框设计(借鉴小古的设计)
;环境 版本:   Autohotkey v1.1.22.09
;=================================================

CreateMsgbox:



;~ #NoTrayIcon
;~ #SingleInstance  force
	;~ SetWorkingDir %A_ScriptDir%
	SetBatchLines -1
	ListLines Off
	;#Include %A_ScriptDir%\..\Lib\GuiButtonIcon.ahk
	
TaskDialogFC=
(Join`r`n
;=========================================================================
; TaskDialogEx(主文,副文,标题,按钮,图标,宽度,父窗口,超时)
TaskDialogEx(Main, Extra := "", Title := "提示：", Buttons := 1, Icon := 8, Width := 600, Parent := -1, TimeOut := 0) {
	Static TDCB      := RegisterCallback("TaskDialogCallback", "Fast")
		, TDCSize   := (4 * 8) + (A_PtrSize * 16)
		, TDBTNS    := {OK: 1, YES: 2, NO: 4, CANCEL: 8, RETRY: 16, CLOSE: 32}
		, TDF       := {HICON_MAIN: 0x0002, ALLOW_CANCEL: 0x0008, CALLBACK_TIMER: 0x0800, SIZE_TO_CONTENT: 0x01000000}
		, TDICON    := {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9
		, WARN: 1, ERROR: 2, INFO: 3, SHIELD: 4, BLUE: 5, YELLOW: 6, RED: 7, GREEN: 8, GRAY: 9
		, QUESTION: 0}
		, HQUESTION := DllCall("User32.dll\LoadIcon", "Ptr", 0, "Ptr", 0x7F02, "UPtr")
		, DBUX      := DllCall("User32.dll\GetDialogBaseUnits", "UInt") & 0xFFFF
		, OffParent := 4
		, OffFlags  := OffParent + (A_PtrSize * 2)
		, OffBtns   := OffFlags + 4
		, OffTitle  := OffBtns + 4
		, OffIcon   := OffTitle + A_PtrSize
		, OffMain   := OffIcon + A_PtrSize
		, OffExtra  := OffMain + A_PtrSize
		, OffCB     := (4 * 7) + (A_PtrSize * 14)
		, OffCBData := OffCB + A_PtrSize
		, OffWidth  := OffCBData + A_PtrSize
	; -------------------------------------------------------------------------------------------------------------------
	if ((DllCall("Kernel32.dll\GetVersion", "UInt") & 0xFF) < 6) {
		if TaskDialogUseMsgBoxOnXP()
			return TaskDialogMsgBox(Main, Extra, Title, Buttons, Icon, Parent, Timeout)
		else {
			MsgBox, 16, `%A_ThisFunc`%, You need at least Win Vista / Server 2008 to use `%A_ThisFunc`%().
			ErrorLevel := "You need at least Win Vista / Server 2008 to use " . A_ThisFunc . "()."
			return 0
		}
	}
	; -------------------------------------------------------------------------------------------------------------------
	Flags := Width = 0 ? TDF.SIZE_TO_CONTENT : 0
	if (Title = "")
		Title := A_ScriptName
	BTNS := 0
	if Buttons Is Integer
		BTNS := Buttons & 0x3F
	else
		For Each, Btn In StrSplit(Buttons, ["|", " ", ",", "``n"])
	BTNS |= (B := TDBTNS[Btn]) ? B : 0
	ICO := (I := TDICON[Icon]) ? 0x10000 - I : 0
	if Icon Is Integer
		if ((Icon & 0xFFFF) <> Icon) ; caller presumably passed HICON
			ICO := Icon
	if (Icon = "Question")
		ICO := HQUESTION
	if (ICO > 0xFFFF)
		Flags |= TDF.HICON_MAIN
	AOT := Parent < 0 ? !(Parent := 0) : False ; AlwaysOnTop
	; -------------------------------------------------------------------------------------------------------------------
	PTitle := A_IsUnicode ? &Title : TaskDialogToUnicode(Title, WTitle)
	PMain  := A_IsUnicode ? &Main : TaskDialogToUnicode(Main, WMain)
	PExtra := Extra = "" ? 0 : A_IsUnicode ? &Extra : TaskDialogToUnicode(Extra, WExtra)
	VarSetCapacity(TDC, TDCSize, 0) ; TASKDIALOGCONFIG structure
	NumPut(TDCSize, TDC, "UInt")
	NumPut(Parent, TDC, OffParent, "Ptr")
	NumPut(BTNS, TDC, OffBtns, "Int")
	NumPut(PTitle, TDC, OffTitle, "Ptr")
	NumPut(ICO, TDC, OffIcon, "Ptr")
	NumPut(PMain, TDC, OffMain, "Ptr")
	NumPut(PExtra, TDC, OffExtra, "Ptr")
	if (AOT) || (TimeOut > 0) {
		if (TimeOut > 0) {
			Flags |= TDF.CALLBACK_TIMER
			TimeOut := Round(Timeout * 1000)
		}
		TD := {AOT: AOT, Timeout: Timeout}
		NumPut(TDCB, TDC, OffCB, "Ptr")
		NumPut(&TD, TDC, OffCBData, "Ptr")
	}
	NumPut(Flags, TDC, OffFlags, "UInt")
	if (Width > 0)
		NumPut(Width * 4 / DBUX, TDC, OffWidth, "UInt")
	if !(RV := DllCall("Comctl32.dll\TaskDialogIndirect", "Ptr", &TDC, "IntP", Result, "Ptr", 0, "Ptr", 0, "UInt"))
		return TD.TimedOut ? -1 : Result
	ErrorLevel := "The call of TaskDialogIndirect() failed!``nreturn value: " . RV . "``nLast error: " . A_LastError
	return 0
}
; ================================================================================?======================================
; Call this function once passing 1/True if you want a MsgBox to be displayed instead of the task dialog on Win XP.
; ================================================================================?======================================
TaskDialogUseMsgBoxOnXP(UseIt := "") {
	Static UseMsgBox := False
	if (UseIt <> "")
		UseMsgBox := !!UseIt
	return UseMsgBox
}
; ================================================================================?======================================
; Internally used functions
; ================================================================================?======================================
TaskDialogMsgBox(Main, Extra, Title := "", Buttons := 0, Icon := 0, Parent := 0, TimeOut := 0) {
	Static MBICON := {1: 0x30, 2: 0x10, 3: 0x40, WARN: 0x30, ERROR: 0x10, INFO: 0x40, QUESTION: 0x20}
		, TDBTNS := {OK: 1, YES: 2, NO: 4, CANCEL: 8, RETRY: 16}
	BTNS := 0
	if Buttons Is Integer
		BTNS := Buttons & 0x1F
	else
		For Each, Btn In StrSplit(Buttons, ["|", " ", ",", "``n"])
	BTNS |= (B := TDBTNS[Btn]) ? B : 0
	Options := 0
	Options |= (I := MBICON[Icon]) ? I : 0
	Options |= Parent = -1 ? 262144 : Parent > 0 ? 8192 : 0
	if ((BTNS & 14) = 14)
		Options |= 0x03 ; Yes/No/Cancel
	else if ((BTNS & 6) = 6)
		Options |= 0x04 ; Yes/No
	else if ((BTNS & 24) = 24)
		Options |= 0x05 ; Retry/Cancel
	else if ((BTNS & 9) = 9)
		Options |= 0x01 ; OK/Cancel
	Main .= Extra <> "" ? "``n``n" . Extra : ""
	MsgBox,`% Options, `%Title`%, `%Main`% `%TimeOut`%
	IfMsgBox, OK
		return 1
	IfMsgBox, Cancel
		return 2
	IfMsgBox, Retry
		return 4
	IfMsgBox, Yes
		return 6
	IfMsgBox, No
		return 7
	IfMsgBox, TimeOut
		return -1
	return 0
}
; ================================================================================?======================================
TaskDialogToUnicode(String, ByRef Var) {
	VarSetCapacity(Var, StrPut(String, "UTF-16") * 2, 0)
	StrPut(String, &Var, "UTF-16")
	return &Var
}
; ================================================================================?======================================
TaskDialogCallback(H, N, W, L, D) {
	Static TDM_Click_BUTTON := 0x0466
		, TDN_CREATED := 0
		, TDN_TIMER   := 4
	TD := Object(D)
	if (N = TDN_TIMER) && (W > TD.Timeout) {
		TD.TimedOut := True
		PostMessage, `%TDM_Click_BUTTON`%, 2, 0, , ahk_id `%H`% ; IDCANCEL = 2
	}
	else if (N = TDN_CREATED) && TD.AOT {
		DHW := A_DetectHiddenWindows
		DetectHiddenWindows, On
		WinSet, AlwaysOnTop, On, ahk_id `%H`%
		DetectHiddenWindows, `%DHW`%
	}
	return 0
}

LoadIcon(FullFilePath, IconNumber := 1, LargeIcon := 1)
{
	HIL := IL_Create(1, 1, !!LargeIcon)
	IL_Add(HIL, FullFilePath, IconNumber)
	HICON := DllCall("Comctl32.dll\ImageList_GetIcon", "Ptr", HIL, "Int", 0, "UInt", 0, "UPtr")
	IL_Destroy(HIL)
	return HICON
}
)

	Gui,  11:Add, Edit, x110 y30 w340 h70 Multi vMsgBoxmain   r3, 主要内容
	Gui,  11:Add, Edit, x110 y90 w340 h40 Multi vextra   r5, 次要内容
	Gui,  11:Add, Edit, x110 y180 w340 h20 -Multi  limit15 vtitle, 提示
	Gui,  11:Add, Edit, x110 y210 w340 h20 -Multi  vwidth +number, 400
	Gui,  11:Add, Slider,x110 y240 w310 h20 Range0-20 ReadOnly Right vTimeout gcontroltimeout, 0
	Gui,  11:Add, Edit, x420 y242 w30 h20 -Multi  limit15 ReadOnly vtimeoutshow, 0.0
	Gui,  11:Font, s11 bold cpurple
	Gui,  11:Add, GroupBox, x470 y20 w290 h125 , 按钮组合
	Gui,  11:Font
	Gui,  11:Add, Radio, x490 y40 w100 h20 vbutton1, Ok
	Gui,  11:Add, Radio, x490 y64 w100 h20 vbutton2, Yes
	Gui,  11:Add, Radio, x490 y88 w100 h20 vbutton3 checked, Yes/No
	Gui,  11:Add, Radio, x490 y112 w130 h20 vbutton4, Cancel/Retry/Close

	Gui,  11:Add, Radio, x620 y112  w100 h20 vbutton5, All Buttons
	Gui,  11:Add, Radio, x620 y64 w100 h20 vbutton6, Retry/Cancel
	Gui,  11:Add, Radio, x620 y88 w100 h20 vbutton7, Retry/Close
	Gui,  11:Add, Radio,x620 y40 w130 h20 vbutton8, Yes/No/Close
	Gui,  11:Font
	Gui,  11:Font, s11 bold cpurple
	Gui,  11:Add, GroupBox, x470 y150 w290 h200 , 图标与主题
	Gui,  11:Font
	Gui,  11:Font,bold
	Gui,  11:Add, Radio, x490 y170 w100 h20 vicon1, 警告
	Gui,  11:Add, Radio, x490 y198 w100 h20 vicon2, 错误
	Gui,  11:Add, Radio, x490 y226 w100 h20 vicon3, 提示
	Gui,  11:Add, Radio, x490 y254 w100 h20 vicon4, 守护
	Gui,  11:Add, Radio,x490 y282 w100 h20 vicon10, 问号
	Gui,  11:Font
	Gui,  11:Font,  cblue bold
	Gui,  11:Add, Radio,  x610 y254 w100 h20 vicon5, 蓝色
	Gui,  11:Font, cFF8000
	Gui,  11:Add, Radio, x610 y170 w100 h20 vicon6, 黄色
	Gui,  11:Font,  cred
	Gui,  11:Add, Radio,  x610 y226 w100 h20 vicon7, 红色
	Gui,  11:Font,  cgreen
	Gui,  11:Add, Radio,  x610 y198w100 h20 vicon8 checked, 绿色
	Gui,  11:Font,  cgray
	Gui,  11:Add, Radio,  x610 y282 w50 h20 vicon9, 灰色
	Gui,  11:Font,  cblack
	Gui,  11:Add, Radio,  x490 y314 w72 h20 vicon11 gcustomicon, 自定义：
	Gui,  11:Font
	Gui,  11:Add, Edit, x560 y314 w180 h20 -Multi  ReadOnly vicon_file_path,
	Gui,  11:Font, s11 bold
	Gui,  11:Add, Text, x20 y55   w90 h20, 主要内容：
	Gui,  11:Add, Text, x20 y120 w90 h20, 次要内容：
	Gui,  11:Add, Text, x20 y185 w90 h20, 标题文字：
	Gui,  11:Add, Text, x20 y215 w90 h20 , 窗口宽度：
	Gui,  11:Add, Text, x20 y245 w90 h20 , 超时秒数：
	;~ Gui,  11:Font
	;~ Gui,  11:font,s13
	;~ Gui,  11:Add, Edit, x20 y360 w737 h100   vcontent, 结果代码
	Gui,  11:font
	Gui,  11:Add, Button, x350 y300 w100 h50 hwndIcon3 gcommand, 写入(F2)
	Gui,  11:Add, Button, x10 y300 w110 h50 hwndIcon1 gview, 预览华丽模式(F1)
	Gui,  11:Add, Button, x190 y300 w100 h50 hwndIcon2 gviewXP, 预览XP模式
	;Gui,  11:Add, Button, x350 y280 w100 h50 hwndIcon4 gcopycode, 复制(F3)

	GuiButtonIcon(Icon1, "shell32.dll", 172, "s32 a0 l5")
	GuiButtonIcon(Icon2, "shell32.dll", 3, "s32 a0 l5")
	GuiButtonIcon(Icon3, "shell32.dll", 46, "s32 a0 l5")
	;GuiButtonIcon(Icon4, "shell32.dll", 259, "s32 a0 l5")

	Gui,  11:Add, Text, x270 y360 w710h30 +disabled,该函数不支持xp，在xp系统会显示普通的MsgBox。
	Gui,  11:Show, CEnter w780 h380, 消息框_AutoAHK
	GuiControl, +Default, 生成
	return

controltimeout:
	GuiControl,  11:, timeoutshow, % (Timeout = 0 ? 0 : Round(Timeout / 2, 1))*2
return

GuiClose:
	ExitApp

f1::
view:
	Gui,  11:Submit, NoHide
	gosub transbuttonandicon
	if icon11
	{
		StringSplit,filepath,icon_file_path,`,
		HICON := LoadIcon(filepath1, filepath2)
		;MsgBox,262208,提示, % "您点击的是："  TaskDialogEx(msgboxmain,extra,title,button,hicon,width,-1,timeout)
		TaskDialogEx(MsgBoxmain,extra,title,button,hicon,width,-1,timeout)
		return
	}
	;MsgBox,262208,提示, % "您点击的是："  TaskDialogEx(msgboxmain,extra,title,button,icon,width,-1,timeout)
	TaskDialogEx(MsgBoxmain,extra,title,button,icon,width,-1,timeout)
return

f2::
command:
	Gui, 11:Submit, NoHide
	gosub transbuttonandicon
	ControlSetText,edit7
	if icon11
	{
		StringSplit,filepath,icon_file_path,`,
		HICON= LoadIcon(%filepath1%, %filepath2%)
		myfunction=
		myfunction=#Include <TaskDialogEx>`r`nTaskDialogUseMsgBoxOnXP(true)`r`nTaskDialogEx("%MsgBoxmain%","%extra%","%title%",%button%,%hicon%,%width%,-1,%timeout%)
		;~ msgbox % myfunction
		;GuiControl,11:,content,%myfunction%
		return
	}
	if icon=question
		icon="question"
	myfunction=
	myfunction=TaskDialogUseMsgBoxOnXP(true)`r`nTaskDialogEx("%MsgBoxmain%","%extra%","%title%",%button%,%icon%,%width%,-1,%timeout%)`r`n %TaskDialogFC%
	;GuiControl,11:,content,%myfunction%
	
	Sci[g_GuiTab].GetText(Sci[g_GuiTab].GetLength() + 1, SciText)
	
    If (StartingPos := InStr(SciText, g_Delimiter)) {
        Code .= SubStr(SciText, StartingPos + StrLen(g_Delimiter))
    }

    Sci[g_GuiTab].BeginUndoAction()
    Sci[g_GuiTab].ClearAll()
    Sci[g_GuiTab].SetText("", g_Signature . Header .  "`n".myfunction, 2)
    Sci[g_GuiTab].EndUndoAction()

    If (TabEx.GetSel() != g_GuiTab) {
        TabEx.SetSel(g_GuiTab)
    }

    Header := ""
    Code := ""
    SciText := ""
	
	;~ Sci[1].GetText(Sci[1].GetLength() + 1, SciText)
	;~ if(SubStr(SciText,1,1)=5)
	;~ {
		;~ SciText:=SubStr(SciText,2,0)
	;~ }
	;~ else
	;~ {
		;~ myfunction=%SciText% `n%myfunction%
	;~ }
	;~ Sci[1].BeginUndoAction()
	;~ Sci[1].ClearAll()
	;~ Sci[1].SetText("", myfunction)
	;~ Sci[1].EndUndoAction()
	Gui, 11:Destroy
return

;~ f3::
;~ copycode:
;~ gosub command
;~ ;clipboard:=myfunction

;~ SoundBeep, 750, 100
;~ SoundBeep, 750, 100
;~ return

viewXP:
	Gui, 11:Submit, NoHide
	gosub transbuttonandicon
	TaskDialogUseMsgBoxOnXP(true)
	;MsgBox,262208,提示, % "您点击的是："  TaskDialogMsgBox(msgboxmain,extra,title,button,icon,-1,timeout)
	TaskDialogMsgBox(MsgBoxmain,extra,title,button,icon,-1,timeout)
return



openahkscript:
	Run https://www.autoahk.com
return


reloadcreator:
	Reload
return

customicon:
	Gui,2:add, text, x10 y10 w400 h20, 查找此文件中的图标：
	Gui,2:add, edit, x10 y30 w300 h20 -Multi vfile
	Gui,2:add, button, x325 y30 w80 h20 gselect, 浏览(&B)...
	Gui,2:add, text, x10 y55 w400 h20, 从以下列表中选择一个图标：
	Gui,2:Add, ListView, x10 y70 w400 h300 Icon vMyListView gMyListView, id
	Gui,2:add, Button, x10 y380 w180 h30 Default gok, 确定
	Gui,2:add, Button, x230 y380 w180 h30 gcancel, 取消
	Gui,2: show, , 选择图标
	Gui 2:+LastFound +OwnDialogs
	ControlClick,button1
return


select:
	FileSelectFile, file, , %SystemRoot%\system32\SHELL32.dll, 请选择图标文件, 图标(*.dll;*.ico;*.exe)
	if file
	{
		GuiControl, 2:, file, % file
		ImageListID := IL_Create(,,1)
		LV_SetImageList(ImageListID)
		icon_count := GetIconCount(file)
		LV_Delete()
		loop % icon_count
			IL_Add(ImageListID, file, A_Index)
		loop % icon_count
			LV_Add("Icon" . A_Index, A_Index)
	}
	else
		Gui, 2:Destroy
return

cancel:
	Gui, 2:Destroy
return

ok:
MyListView:
	icon := {}
	FocusedRowNumber := LV_GetNext(0, "F")
	if not FocusedRowNumber
	{
		MsgBox, 4144, 提示, 您未选择任何图标！
		return
	}
	LV_GetText(id, FocusedRowNumber, 1)
	icon["file"] := file
	icon["id"] := id
	; MsgBox, 64, 提示, % "你选择了" """" icon["file"] """" "的第" icon["id"] "个图标"
	GuiControl,11:,icon_file_path,% icon["file"] "," icon["id"]
	Gui,2: destroy
return

GetIconCount(file)
{
	Menu, test, add, test, handle
	loop
	{
		try
		{
			id++
			Menu, test, Icon, test, % file, % id
		}
		catch error
		{
			break
		}
	}
	return id-1
}

handle:
return




transbuttonandicon:
	button=
	icon=
	loop 8
	{
		if button%a_index%=1
		{
			if A_Index = 1
				button = 1
			else if A_Index = 2
				button = 2
			else if A_Index = 3
				button = 6
			else if A_Index = 4
				button = 56
			else if A_Index = 5
				button = 63
			else if A_Index = 6
				button = 24
			else if A_Index = 7
				button = 48
			else if A_Index = 8
				button = 38
			break
		}

	}
	loop 10
	{
		if icon%a_index%=1
		{
			if a_index=10
				icon=question
			else
				icon:=a_index
			break
		}

	}
return


;=========================================================================
; TaskDialogEx(主文,副文,标题,按钮,图标,宽度,父窗口,超时)
TaskDialogEx(Main, Extra := "", Title := "提示：", Buttons := 1, Icon := 8, Width := 600, Parent := -1, TimeOut := 0) {
	Static TDCB      := RegisterCallback("TaskDialogCallback", "Fast")
		, TDCSize   := (4 * 8) + (A_PtrSize * 16)
		, TDBTNS    := {OK: 1, YES: 2, NO: 4, CANCEL: 8, RETRY: 16, CLOSE: 32}
		, TDF       := {HICON_MAIN: 0x0002, ALLOW_CANCEL: 0x0008, CALLBACK_TIMER: 0x0800, SIZE_TO_CONTENT: 0x01000000}
		, TDICON    := {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9
		, WARN: 1, ERROR: 2, INFO: 3, SHIELD: 4, BLUE: 5, YELLOW: 6, RED: 7, GREEN: 8, GRAY: 9
		, QUESTION: 0}
		, HQUESTION := DllCall("User32.dll\LoadIcon", "Ptr", 0, "Ptr", 0x7F02, "UPtr")
		, DBUX      := DllCall("User32.dll\GetDialogBaseUnits", "UInt") & 0xFFFF
		, OffParent := 4
		, OffFlags  := OffParent + (A_PtrSize * 2)
		, OffBtns   := OffFlags + 4
		, OffTitle  := OffBtns + 4
		, OffIcon   := OffTitle + A_PtrSize
		, OffMain   := OffIcon + A_PtrSize
		, OffExtra  := OffMain + A_PtrSize
		, OffCB     := (4 * 7) + (A_PtrSize * 14)
		, OffCBData := OffCB + A_PtrSize
		, OffWidth  := OffCBData + A_PtrSize
	; -------------------------------------------------------------------------------------------------------------------
	if ((DllCall("Kernel32.dll\GetVersion", "UInt") & 0xFF) < 6) {
		if TaskDialogUseMsgBoxOnXP()
			return TaskDialogMsgBox(Main, Extra, Title, Buttons, Icon, Parent, Timeout)
		else {
			MsgBox, 16, %A_ThisFunc%, You need at least Win Vista / Server 2008 to use %A_ThisFunc%().
			ErrorLevel := "You need at least Win Vista / Server 2008 to use " . A_ThisFunc . "()."
			return 0
		}
	}
	; -------------------------------------------------------------------------------------------------------------------
	Flags := Width = 0 ? TDF.SIZE_TO_CONTENT : 0
	if (Title = "")
		Title := A_ScriptName
	BTNS := 0
	if Buttons Is Integer
		BTNS := Buttons & 0x3F
	else
		For Each, Btn In StrSplit(Buttons, ["|", " ", ",", "`n"])
	BTNS |= (B := TDBTNS[Btn]) ? B : 0
	ICO := (I := TDICON[Icon]) ? 0x10000 - I : 0
	if Icon Is Integer
		if ((Icon & 0xFFFF) <> Icon) ; caller presumably passed HICON
			ICO := Icon
	if (Icon = "Question")
		ICO := HQUESTION
	if (ICO > 0xFFFF)
		Flags |= TDF.HICON_MAIN
	AOT := Parent < 0 ? !(Parent := 0) : False ; AlwaysOnTop
	; -------------------------------------------------------------------------------------------------------------------
	PTitle := A_IsUnicode ? &Title : TaskDialogToUnicode(Title, WTitle)
	PMain  := A_IsUnicode ? &Main : TaskDialogToUnicode(Main, WMain)
	PExtra := Extra = "" ? 0 : A_IsUnicode ? &Extra : TaskDialogToUnicode(Extra, WExtra)
	VarSetCapacity(TDC, TDCSize, 0) ; TASKDIALOGCONFIG structure
	NumPut(TDCSize, TDC, "UInt")
	NumPut(Parent, TDC, OffParent, "Ptr")
	NumPut(BTNS, TDC, OffBtns, "Int")
	NumPut(PTitle, TDC, OffTitle, "Ptr")
	NumPut(ICO, TDC, OffIcon, "Ptr")
	NumPut(PMain, TDC, OffMain, "Ptr")
	NumPut(PExtra, TDC, OffExtra, "Ptr")
	if (AOT) || (TimeOut > 0) {
		if (TimeOut > 0) {
			Flags |= TDF.CALLBACK_TIMER
			TimeOut := Round(Timeout * 1000)
		}
		TD := {AOT: AOT, Timeout: Timeout}
		NumPut(TDCB, TDC, OffCB, "Ptr")
		NumPut(&TD, TDC, OffCBData, "Ptr")
	}
	NumPut(Flags, TDC, OffFlags, "UInt")
	if (Width > 0)
		NumPut(Width * 4 / DBUX, TDC, OffWidth, "UInt")
	if !(RV := DllCall("Comctl32.dll\TaskDialogIndirect", "Ptr", &TDC, "IntP", Result, "Ptr", 0, "Ptr", 0, "UInt"))
		return TD.TimedOut ? -1 : Result
	ErrorLevel := "The call of TaskDialogIndirect() failed!`nreturn value: " . RV . "`nLast error: " . A_LastError
	return 0
}
; ================================================================================?======================================
; Call this function once passing 1/True if you want a MsgBox to be displayed instead of the task dialog on Win XP.
; ================================================================================?======================================
TaskDialogUseMsgBoxOnXP(UseIt := "") {
	Static UseMsgBox := False
	if (UseIt <> "")
		UseMsgBox := !!UseIt
	return UseMsgBox
}
; ================================================================================?======================================
; Internally used functions
; ================================================================================?======================================
TaskDialogMsgBox(Main, Extra, Title := "", Buttons := 0, Icon := 0, Parent := 0, TimeOut := 0) {
	Static MBICON := {1: 0x30, 2: 0x10, 3: 0x40, WARN: 0x30, ERROR: 0x10, INFO: 0x40, QUESTION: 0x20}
		, TDBTNS := {OK: 1, YES: 2, NO: 4, CANCEL: 8, RETRY: 16}
	BTNS := 0
	if Buttons Is Integer
		BTNS := Buttons & 0x1F
	else
		For Each, Btn In StrSplit(Buttons, ["|", " ", ",", "`n"])
	BTNS |= (B := TDBTNS[Btn]) ? B : 0
	Options := 0
	Options |= (I := MBICON[Icon]) ? I : 0
	Options |= Parent = -1 ? 262144 : Parent > 0 ? 8192 : 0
	if ((BTNS & 14) = 14)
		Options |= 0x03 ; Yes/No/Cancel
	else if ((BTNS & 6) = 6)
		Options |= 0x04 ; Yes/No
	else if ((BTNS & 24) = 24)
		Options |= 0x05 ; Retry/Cancel
	else if ((BTNS & 9) = 9)
		Options |= 0x01 ; OK/Cancel
	Main .= Extra <> "" ? "`n`n" . Extra : ""
	MsgBox, % Options, %Title%, %Main%, %TimeOut%
	IfMsgBox, OK
		return 1
	IfMsgBox, Cancel
		return 2
	IfMsgBox, Retry
		return 4
	IfMsgBox, Yes
		return 6
	IfMsgBox, No
		return 7
	IfMsgBox, TimeOut
		return -1
	return 0
}
; ================================================================================?======================================
TaskDialogToUnicode(String, ByRef Var) {
	VarSetCapacity(Var, StrPut(String, "UTF-16") * 2, 0)
	StrPut(String, &Var, "UTF-16")
	return &Var
}
; ================================================================================?======================================
TaskDialogCallback(H, N, W, L, D) {
	Static TDM_Click_BUTTON := 0x0466
		, TDN_CREATED := 0
		, TDN_TIMER   := 4
	TD := Object(D)
	if (N = TDN_TIMER) && (W > TD.Timeout) {
		TD.TimedOut := True
		PostMessage, %TDM_Click_BUTTON%, 2, 0, , ahk_id %H% ; IDCANCEL = 2
	}
	else if (N = TDN_CREATED) && TD.AOT {
		DHW := A_DetectHiddenWindows
		DetectHiddenWindows, On
		WinSet, AlwaysOnTop, On, ahk_id %H%
		DetectHiddenWindows, %DHW%
	}
	return 0
}

LoadIcon(FullFilePath, IconNumber := 1, LargeIcon := 1)
{
	HIL := IL_Create(1, 1, !!LargeIcon)
	IL_Add(HIL, FullFilePath, IconNumber)
	HICON := DllCall("Comctl32.dll\ImageList_GetIcon", "Ptr", HIL, "Int", 0, "UInt", 0, "UPtr")
	IL_Destroy(HIL)
	return HICON
}


GuiButtonIcon(Handle, File, Index := 1, Options := "")
{
RegExMatch(Options, "i)w\K\d+", W), (W="") ? W := 16 :
RegExMatch(Options, "i)h\K\d+", H), (H="") ? H := 16 :
RegExMatch(Options, "i)s\K\d+", S), S ? W := H := S :
RegExMatch(Options, "i)l\K\d+", L), (L="") ? L := 0 :
RegExMatch(Options, "i)t\K\d+", T), (T="") ? T := 0 :
RegExMatch(Options, "i)r\K\d+", R), (R="") ? R := 0 :
RegExMatch(Options, "i)b\K\d+", B), (B="") ? B := 0 :
RegExMatch(Options, "i)a\K\d+", A), (A="") ? A := 4 :
Psz := A_PtrSize = "" ? 4 : A_PtrSize, DW := "UInt", Ptr := A_PtrSize = "" ? DW : "Ptr"
VarSetCapacity( button_il, 20 + Psz, 0 )
NumPut( normal_il := DllCall( "ImageList_Create", DW, W, DW, H, DW, 0x21, DW, 1, DW, 1 ), button_il, 0, Ptr )   ; Width & Height
NumPut( L, button_il, 0 + Psz, DW )     ; Left Margin
NumPut( T, button_il, 4 + Psz, DW )     ; Top Margin
NumPut( R, button_il, 8 + Psz, DW )     ; Right Margin
NumPut( B, button_il, 12 + Psz, DW )    ; Bottom Margin
NumPut( A, button_il, 16 + Psz, DW )    ; Alignment
SendMessage, BCM_SETIMAGELIST := 5634, 0, &button_il,, AHK_ID %Handle%
return IL_Add( normal_il, File, Index )
}



return