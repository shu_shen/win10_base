﻿;自定义函数
CreateMyFunc:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
函数名(参数1，参数2，……)
{`;这里写入你的具体执行代码
    
}


	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;创建标签
CreateLabel:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
标签名: 
`;执行代码写入这里

return
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;创建自定义类
CreateClass:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
class 类名 extends 基类名
{
    InstanceVar := Expression		`; 实例变量(实例属性)
    static ClassVar := Expression	`; 静态变量(类属性)

    class NestedClass			`; 嵌套类
    {
        ...
    }

    Method()				`; 方法, 类定义中的函数称作方法
    {
        ...
    }

    Property[]  			`; 属性定义, 方括号是可选的
    {
        get {
            return ...
        }
        set {
            return ... := value		`; value 在此处为关键字, 不可用其他名称
        }
    }
}

	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;创建标签
varcheck:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
if var is float
{ ;is和is not可用
    MsgBox, %var%是浮点数.
}
else if var is integer	
{
    MsgBox, %var% 是整数.
}
else if var is time
{
    MsgBox, %var% 是一个有效的时间格式.
}
else
{
	MsgBox, 还有number（数字）、digit（0-9z组成的数字）、xdigit（十六进制）、alpha（字母）、upper（大写字母）、lower（小写字母）、alnum（0-9，a-z）、space.
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;if表达式
ifexpression:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
if (A_Index > 100)
    return

if (A_TickCount - StartTime > 2*MaxTime + 100)
{
    MsgBox Too much time has passed.
    ExitApp
}

if (Color = "Blue" or Color = "White")
{
    MsgBox The color is one of the allowed values.
    ExitApp
}
else if (Color = "Silver")
{
    MsgBox Silver is not an allowed color.
    return
}
else
{
    MsgBox This color is not recognized.
    ExitApp
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;if表达式
varin:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
/*if Var in MatchList
if Var not in MatchList

if Var contains MatchList
if Var not contains MatchList
*/
if var in exe,bat,com
{
	MsgBox The file extension is an executable type.
}

if var in 1,2,3,5,7,11 ; 避免在列表中添加空格.
{
    MsgBox %var% is a small prime number.
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;if表达式
varbetween:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n

if var not between 0.0 and 1.0
{
    MsgBox %var% is not in the range 0.0 to 1.0, inclusive.
}
if var between %VarLow% and %VarHigh%
{	
    MsgBox %var% is between %VarLow% and %VarHigh%.
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;if表达式
VarEqual:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
/*IfEqual, Var [, Value]          ; if Var = Value
IfNotEqual, Var [, Value]       ; if Var != Value
IfLess, Var [, Value]           ; if Var < Value
IfLessOrEqual, Var [, Value]    ; if Var <= Value
IfGreater, Var [, Value]        ; if Var > Value
IfGreaterOrEqual, Var [, Value] ; if Var >= Value*/
	
	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;if表达式
If[Not]InString:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
Haystack = abcdefghijklmnopqrs
Needle := "abc"
IfInString, Haystack, %Needle%
{
    MsgBox, The string was found.
    return
}
else
{
    Sleep, 1
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;if表达式
If[Not]Exist:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
IfExist, D:\
{
    MsgBox, The drive exists.
}
IfExist, D:\Docs\*.txt
{
    MsgBox, At least one .txt file exists.
}
IfNotExist, C:\Temp\FlagFile.txt
{
    MsgBox, The target file does not exist.
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;if表达式
Simplecode:
	Store=%ClipboardAll%
	Clipboard:=""
	Clipboard:="X？A:B"
	gosub Paste_and_Restore_Stored_Clipboard
return

;loop表达式
Normalloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
Loop, 3
{
    MsgBox, 迭代数是 `%A_Index`%.  ; A_Index 将为 1, 2, 接着 3
    Sleep, 100
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;loop表达式
fileloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
Loop Files, `%A_ProgramFiles`%\*.txt, R  ; 递归子文件夹.
{
    MsgBox, 4, , Filename = `%A_LoopFileFullPath`%`n`nContinue?
    IfMsgBox, No
        break
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;loop表达式
regloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
Loop, Reg, HKEY_CURRENT_USER\Software\Microsoft\Windows, KVR
{
    if (A_LoopRegType = "key")
        value := ""
    else
    {
        RegRead, value
        if ErrorLevel
            value := "*error*"
    }
    MsgBox, 4, , %A_LoopRegName% = %value% (%A_LoopRegType%)`n`nContinue?
    IfMsgBox, NO, break
}

	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;loop表达式
contentloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
Loop, read, C:\Docs\Address List.txt, C:\Docs\Family Addresses.txt
{
    if InStr(A_LoopReadLine, "family")
        FileAppend, `%A_LoopReadLine`%`n
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;loop表达式
parseloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
Colors := "red,green,blue"
Loop, parse, Colors, `,
{
    MsgBox, Color number %A_Index% is %A_LoopField%.
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;loop表达式
whileloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
;当用户按住鼠标左键拖动时, 在拖动区域中会出现工具提示区域的大小
CoordMode, Mouse, Screen

~LButton::
    MouseGetPos, begin_x, begin_y
    while GetKeyState("LButton")
    {
        MouseGetPos, x, y
        ToolTip, `% begin_x ", " begin_y "`n" Abs(begin_x-x) " x " Abs(begin_y-y)
        Sleep, 10
    }
    ToolTip
return
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;loop表达式
forloop:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
colours := Object("red", 0xFF0000, "blue", 0x0000FF, "green", 0x00FF00)
;实例1
; 上面的表达式可以直接代替下面的"colours":
for k, v in colours
    s .= k "=" v "`n"
MsgBox `% s
;实例2
for window in ComObjCreate("Shell.Application¬").Windows
    windows .= window.LocationName¬ " :: " window.LocationURL¬ "`n"
MsgBox `% windows
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;try表达式
trystructure:

		Store=%ClipboardAll%
		Clipboard:=""
		Clipboard=
	(  Join`r`n
try  ; 尝试执行的代码.
{
    HelloWorld()
    MakeToast()
}
catch e  ; 处理由上面区块产生的首个错误/异常.
{
    MsgBox, An exception was thrown!`nSpecifically: %e%
    Exit
}

HelloWorld()  ; 总是成功.
{
    MsgBox, Hello, world!
}

MakeToast()  ; 总是失败.
{
    ; 立即跳到 try 区块的错误处理程序:
    throw A_ThisFunc " is not implemented, sorry"
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

normalstructure_请在这里写入按扭的名字:
    Store:=ClipboardAll  ;****Store clipboard ****
    Clipboard=
    (  Join`r`n
请在这里写入代码片断
)
    gosub Paste_and_Restore_Stored_Clipboard
Return
normalstructure_ifstructure_ifis:
    Store:=ClipboardAll  ;****Store clipboard ****
    Clipboard=
    (  Join`r`n
if var is float
    MsgBox, `%var`% is a floating point number.
else if var is integer
    MsgBox, `%var`% is an integer.
if var is time
    MsgBox, `%var`% is also a valid date-time.
)
    gosub Paste_and_Restore_Stored_Clipboard
Return