commandscraping_graphicscommandcode_PixelGetColor:
Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
(  Join`r`n
!a::
  MouseGetPos, MouseX, MouseY
  PixelGetColor, color, `%MouseX`%, `%MouseY`%, RGB  
  Clipboard := "#" . SubStr(color, 3)
  MsgBox, 当前颜色是 ``n `%Clipboard`%
return

)
	gosub Paste_and_Restore_Stored_Clipboard
Return

commandscraping_wincode_WinGetActiveTitle:
Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
(  Join`r`n
;一键关闭当前窗口
~LButton & escape::
    WinGetActiveTitle,t
    WinClose,`%t`%
Return

)
	gosub Paste_and_Restore_Stored_Clipboard
return

commandscraping_wincommandcode_统一快捷键（WinGetClass）:
    Store:=ClipboardAll  ;****Store clipboard ****
    Clipboard=
    (  Join`r`n
~MButton::
  WinGetClass,sClass,A
  ;//MsgBox class=`%sClass`%
  ;//FM=7-zip window, TFcFormMain=FreeCommander
  if (sClass="TFcFormMain" || sClass="FM" || sClass="MultiCommander MainWnd") {
    Send, {BS}
  } else if (sClass="CabinetWClass") {
    Send, !{up}
  } else if (sClass="MozillaWindowClass" || sClass="Chrome_WidgetWin_1") {
    WinGetTitle, sTitle, A
    ;//MsgBox title=`%sTitle`%
    if (InStr(sTitle, "Gmail") > 0) {
      Send {j}
    } else if (InStr(sTitle, "Twitter") > 0) {
      Send {j}
    }
  } 
  return
  


~+MButton::
  WinGetClass,sClass,A
  ;//MsgBox $`%sClass`%$
  if (sClass = "MozillaWindowClass" || sClass="Chrome_WidgetWin_1") {
    WinGetTitle, sTitle, A
    if (InStr(sTitle, "Gmail") > 0 || InStr(sTitle, "Facebook") > 0) {
      Send {k}
    } else if (InStr(sTitle, "Twitter") > 0) {
      Send {k}
    }
  }
  return
)
    gosub Paste_and_Restore_Stored_Clipboard
Return