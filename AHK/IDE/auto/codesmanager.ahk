﻿; 河许人 AutoAHK 2.0.1
#NoEnv
#SingleInstance Force
SetWorkingDir %A_ScriptDir%
Gui Add, DropDownList, x10 y20 w200 vselectcodelib gselectcodelib, 库名||常用结构|函数库|命令库|web库|word库|execl库|sqlite库|其他库
Gui Add, DropDownList, x220 y20 w200 hWndselectcodesublib vselectcodesublib,子库名||选择结构|循环结构
Gui Add, DropDownList, x430 y20 w200 ,图标||
Gui Add, Edit, x10 y50 w620 h21 vlabelnametoadd,请在这里写入按扭的名字
Gui Add, Edit, x10 y75 w620 h370 vcodetoadd,请在这里写入代码片断
Gui Add, Button, x550 y450 w75 h23 g确定插入,确定添加
Gui Show, , 代码片断管理窗口
Return

GuiEscape:
GuiClose:
    ExitApp

; 不要编辑这行之前的内容!
确定插入:
Gui,Submit,NoHide


if(selectcodelib=="库名")
{
    MsgBox,请选择类库！
}
Else if(selectcodelib=="常用结构")
{
    Gui,Submit,NoHide
    if(selectcodesublib=="子库名")
    {
        prefix:="normalstructure_"
        libname:="normalstructure"
        gosub,addtolib
    }
    Else if(selectcodesublib=="选择结构")
    {
        prefix:="normalstructure_ifstructure_"
        libname:="ifstructure"
        gosub,addtolib
    }
    Else if(selectcodesublib=="循环结构")
    {
        prefix:="normalstructure_loopstructure_"
        libname:="loopstructure"
        gosub,addtolib
    }
}
Else if(selectcodelib=="函数库")
{
    Gui,Submit,NoHide
    if(selectcodesublib=="子库名")
    {
        prefix:="functionscraping_"
        libname:="functionscraping"
        gosub,addtolib  
    }
    Else if(selectcodesublib=="dllcall")
    {
        prefix:="functionscraping_dllcallcode_"
        libname:="DllCallcode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="PostMessage")
    {
        prefix:="functionscraping_PostMessagecode_"
        libname:="PostMessagecode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="win")
    {
        prefix:="functionscraping_wincode_"
        libname:="wincode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="com")
    {
        prefix:="functionscraping_comcode_"
        libname:="comcode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="科学计算")
    {
        prefix:="functionscraping_computecode_"
        libname:="computecode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="文件")
    {
        prefix:="functionscraping_filecode_"
        libname:="filecode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="字符串")
    {
        prefix:="functionscraping_stringcode_"
        libname:="stringcode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="图形图像")
    {
        prefix:="functionscraping_graphicscode_"
        libname:="graphicscode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="其它")
    {
        prefix:="functionscraping_funcionothercode_"
        libname:="funcionothercode"
        gosub,addtolib 
    }
}
Else if(selectcodelib=="命令库")
{
    Gui,Submit,NoHide
    if(selectcodesublib=="子库名")
    {
        prefix:="commandscraping_"
        libname:="commandscraping"
        gosub,addtolib  
    }
    Else if(selectcodesublib=="win")
    {
        prefix:="commandscraping_wincommandcode_"
        libname:="wincommandcode"
        gosub,addtolib 
    }
    Else if(selectcodesublib=="图形图像")
    {
        prefix:="commandscraping_graphicscommandcode_"
        libname:="graphicscommandcode"
        gosub,addtolib 
    }
}
Else if(selectcodelib=="web库")
{
    FileAppend,%codestoaddlib%,%A_ScriptDir%\auto\web.ahk
}
Else if(selectcodelib=="word库")
{
    FileAppend,%codestoaddlib%,%A_ScriptDir%\auto\word.ahk
}
Else if(selectcodelib=="execl库")
{
    FileAppend,%codestoaddlib%,%A_ScriptDir%\auto\execl.ahk
}
Else if(selectcodelib=="sqlite库")
{
    FileAppend,%codestoaddlib%,%A_ScriptDir%\auto\sqlite.ahk
}
Else if(selectcodelib=="其他库")
{
    FileAppend,%codestoaddlib%,%A_ScriptDir%\auto\other.ahk
}
Return

;选库之后，切换子库
selectcodelib:
Gui,Submit,NoHide
;MsgBox,%selectcodelib%
if(selectcodelib=="常用结构")
{
    GuiControl,,selectcodesublib,|子库名||选择结构|循环结构|其它
}
Else if(selectcodelib=="函数库")
{
    GuiControl,,selectcodesublib,|子库名||dllcall|PostMessage|win|com|科学计算|界面|文件|字符串|图形图像|其它
}
Else if(selectcodelib=="命令库")
{
    GuiControl,,selectcodesublib,|子库名||win|图形图像
}
Else if(selectcodelib=="web库")
{
    GuiControl,,selectcodesublib,|子库名||点|页面/导航|获取|设值|事件|高级|其它|帮助链接
}
Else if(selectcodelib=="word库")
{
    GuiControl,,selectcodesublib,|子库名||word|
}
Else if(selectcodelib=="execl库")
{
    GuiControl,,selectcodesublib,|子库名||execl|
}
Else if(selectcodelib=="sqlite库")
{
    GuiControl,,selectcodesublib,|子库名||sqlite|
}
Else if(selectcodelib=="其他库")
{
    GuiControl,,selectcodesublib,|子库名||other|
}
Return

addtolib:
        Gui,Submit,NoHide
        onlylabelname:=labelnametoadd
        labelnametoadd:=prefix . labelnametoadd ;根据下拉列表获取前缀
        codestoaddlib=
        (Join`r`n

%labelnametoadd%:
    Store:=ClipboardAll  ;****Store clipboard ****
    Clipboard=
    (  Join``r``n
%codetoadd%
`)
    gosub Paste_and_Restore_Stored_Clipboard
Return
        )
        codestoaddtomenulib=
        ( Join``r``n

Menu,%libname%,Add,%onlylabelname%,%labelnametoadd% 
        ) ; 库名对应
        FileAppend,%codestoaddlib%,%A_ScriptDir%\%selectcodelib%.ahk  ;库文件对应
        fileappend,%codestoaddtomenulib%,%a_scriptdir%\..\include\menu.ahk 
        MsgBox,添加成功！
return