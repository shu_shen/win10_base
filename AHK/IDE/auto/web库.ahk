﻿
;******************************

^`::Menu, autoscripting, Show  ; right mouse and windows
;^LButton::Menu, autoscripting, Show  ; right mouse and windows
	;~ Browser_Forward::Reload
	;******************************


	;******************************
	;***********Pointer*******************
	;******************************
	;Pointer***********create IE com*******************
WebScraping_Pointer_Create_IE:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb := ComObjCreate(""InternetExplorer.Application"") `;创建IE对象`r`npwb.visible:=true  `; 设置IE对象为可见"
	gosub Paste_and_Restore_Stored_Clipboard
return

;Pointer***********pointer wbGet*******************
WebScraping_Pointer_wbGet:
	Store:=ClipboardAll  ;****Store clipboard ****
	clipboard := "pwb := WBGet()"
	gosub Paste_and_Restore_Stored_Clipboard
return
;******************Function*************************************
WebScraping_Pointer_wbGet_FUNCTION:
	Clipboard=
	(  Join`r`n
		;************Pointer to Open IE Window******************
WBGet(WinTitle="ahk_class IEFrame", Svr#=1) {               ;// based on ComObjQuery docs
   static msg := DllCall("RegisterWindowMessage", "str", "WM_HTML_GETOBJECT")
			, IID := "{0002DF05-0000-0000-C000-000000000046}"   ;// IID_IWebBrowserApp
		;//     , IID := "{332C4427-26CB-11D0-B483-00C04FD90119}"   ;// IID_IHTMLWindow2
   SendMessage msg, 0, 0, Internet Explorer_Server`%Svr#`%, `%WinTitle`%

   if (ErrorLevel != "FAIL") {
      lResult:=ErrorLevel, VarSetCapacity(GUID,16,0)
      if DllCall("ole32\CLSIDFromString", "wstr","{332C4425-26CB-11D0-B483-00C04FD90119}", "ptr",&GUID) >= 0 {
         DllCall("oleacc\ObjectFromLresult", "ptr",lResult, "ptr",&GUID, "ptr",0, "ptr*",pdoc)
         return ComObj(9,ComObjQuery(pdoc,IID,IID),1), ObjRelease(pdoc)
      }
   }
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;Pointer***********Title and URL*******************
WebScraping_Pointer_Get_IE_Title_URL:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard = pwb:=setWbCom("LinkedIn", "www.linkedin.com") ;get pointer to specific window title and domain
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Pointer_Get_IE_Title_URL_FUNCTION:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
`;***********Function*******************
setWbCom(name=false, url=false) {
		;   Set web browser COM pointer        ;   eserv_flag sets this.wb_eserv
    if (!name AND !url) {
		;    Clear COM object
        return false
    }
		;   Set defaults.  No promises.
    wb:=false
    For wb in ComObjCreate( "Shell.Application" ).Windows {
        Try {
            If ((InStr(wb.locationName, name) AND name) OR (InStr(wb.locationURL, url) AND url)) && InStr(wb.FullName, "iexplore.exe") {
                return wb
            }
        } Catch, e {
            if (e AND this.debug) {
                FileAppend, `% "``r``nCOM Error: " . e, `% this.debug_file
            }
        }
    }
    if (debug) {
        this.errorWindow("Failed to find web browser.``r``nName:``t" . name . "``r``nURL:``t" . url)
    }
    return false
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;******************************
;***********page / URL*******************
;******************************
;page***********Location url*******************
WebScraping_Page_Location_URL:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="var:=pwb.LocationURL `;grab current url"
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Page_Scroll_to_Element_on_Page:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.getElementByID(""XXXXX"").scrollIntoView(1) `;Scroll to element on page"
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Page_Location_Name:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="var:=pwb.LocationName `;grab page Title"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********Navigate*******************
WebScraping_Page_Navigate:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.Navigate(""www.autoahk.com"") `;导航至网页"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********wait until page loads*******************
WebScraping_Page_Wait_Page_Load:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="while pwb.busy or pwb.ReadyState != 4 `;等待页面加载`n`tSleep, 100"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********wait until page loads*******************
WebScraping_Page_Wait_IE_Load:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
(  Join`r`n
waitIE(ie,s=10,iTitle=30)
{ `;等待ie加载完毕的函数，s询问的时间间隔，ititle是询问的次数，两数相乘就是等待的时间
	i = 0
	loop
	{
		Sleep,`%s`%
		i := ie.ReadyState = 4 ? i+1 : 0
	} Until (i > iTitle )
}
)
	gosub Paste_and_Restore_Stored_Clipboard
return

;page;**********Wait for element to be present before moving forward****
WebScraping_Page_Wait_for_Element:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
while (pwb.document.getElementsByClassName("sc-view blue-pastel").length<3) ;change number to your item in the array
	Sleep, 200
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********refresh page*******************
WebScraping_Page_Reload:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.refresh `;Reload page"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********History count*******************
WebScraping_Page_History_Count:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="History_CT:=pwb.document.parentWindow.history.length `;Count of History"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********Go backward in history*******************
WebScraping_Page_Backward:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.parentWindow.history.go(-1) `;Go Backward one page"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********Go forward in history*******************
WebScraping_Page_Forward:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.parentWindow.history.go(1) `;Go Forward one page"
	gosub Paste_and_Restore_Stored_Clipboard
return

;page***********Navigate over loop*******************
WebScraping_Page_Loop:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
var=
`(
89147

94947
30606
75019
73112
22315
97520

94134
94931
91711
91106
95665
28203
`)

		;***********loop over vars*******************
Loop, parse, Var, ``n, ``r ;loop over Var line by line
{
IfEqual, A_LoopField,,continue ;Skip loop if blank
IfEqual, A_index,5,break ;Break if index =value
		;~ MsgBox,,Loop index and values, `% A_index a_tab A_LoopField

pwb := WBGet()  ;included in loop just in case gets dissconnected
URL:="http://zipwho.com/?mode=zip&zip=" . A_LoopField ; concatenate url and current row

		;***********Navigate to a page*******************
pwb.Navigate(URL) ;Navigate to URL
while pwb.busy or pwb.ReadyState != 4 ;Wait for page to load
	Sleep, 100

		;***********Grab a data point*******************
Value:=pwb.document.all.details_table.all.tags("TD").item[1].InnerTEXT  ;Unique ID -no dashes
Agg_Values.=A_LoopField a_tab Value "``r``n"  ;Store in new Aggregate variable
}

		;***********Now display extracted data*******************
gui, font, s12 cBlue q5, Book Antiqua
Gui,Add,Edit,w900 h600 -Wrap, `%Agg_Values`%
gui,show
return

		;************Pointer to Open IE Window******************
WBGet(WinTitle="ahk_class IEFrame", Svr#=1) {               ;// based on ComObjQuery docs
   static msg := DllCall("RegisterWindowMessage", "str", "WM_HTML_GETOBJECT")
			, IID := "{0002DF05-0000-0000-C000-000000000046}"   ;// IID_IWebBrowserApp
		;//     , IID := "{332C4427-26CB-11D0-B483-00C04FD90119}"   ;// IID_IHTMLWindow2
   SendMessage msg, 0, 0, Internet Explorer_Server`%Svr#`%, `%WinTitle`%

   if (ErrorLevel != "FAIL") {
      lResult:=ErrorLevel, VarSetCapacity(GUID,16,0)
      if DllCall("ole32\CLSIDFromString", "wstr","{332C4425-26CB-11D0-B483-00C04FD90119}", "ptr",&GUID) >= 0 {
         DllCall("oleacc\ObjectFromLresult", "ptr",lResult, "ptr",&GUID, "ptr",0, "ptr*",pdoc)
         return ComObj(9,ComObjQuery(pdoc,IID,IID),1), ObjRelease(pdoc)
      }
   }
}
return
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;***********wait until exists*******************
;~ WebScraping_Wait_Until_Exists:
;~ Store:=ClipboardAll  ;****Store clipboard ****
;~ Clipboard=
;~ (  Join`r`n
;~ )
;~ Gosub Paste_and_Restore_Stored_Clipboard
;~ return

;******************************
;***********GET*******************
;GET*********Get Unique ID on page*********************
WebScraping_Get_ID_Unique:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.all.XXXXXXX.Value `;Unique ID -no dashes"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get ID with dashes in name*******************
WebScraping_Get_ID_Unique_Dashes:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.getElementByID(""XXX"").Value `;Unique ID"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get Name (returns array)*******************
WebScraping_Get_Name_Array:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.GetElementsByName(""XXXX"").item[0].Value `;Object Name- Get array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get by Class name (returns array)*******************
WebScraping_Get_ClassName:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.getElementsByClassName(""classnameXXX"").item[0].Value `;Get classname and Array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get by Tag name (returns array)*******************
WebScraping_Get_TagName:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.GetElementsByTagName(""tagnameXXX"").item[0].Value `;Get Tagname and Array value"
	gosub Paste_and_Restore_Stored_Clipboard
return


;GET***********Get dropdowns*******************
WebScraping_Get_DropDowns:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Var:=pwb.document.GetElementsByTagName("tagnameXXX").item[0].selectedIndex ;Set Tagname and Array value
Var:=pwb.document.GetElementsByTagName("tagnameXXX").item[0].Value ;Set Tagname and Array value
	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;GET***********Get check boxes*******************
WebScraping_Get_CheckBoxes:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.GetElementsByTagName(""tagnameXXX"").item[0].checked  `;Set Tagname and Array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get Attributes*******************
WebScraping_Get_Attributes:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.GetElementsByTagName(""Input"").item[0].getAttribute(""type"") `;gets the value of an attribute"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get all text on page*******************
WebScraping_Get_All_Text_on_Page:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Var:=pwb.document.documentElement.innerText `;Get All text on page"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get all HTML from page*******************
WebScraping_Get_All_HTML_on_Page:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="clipboard:=pwb.document.documentElement.outerhtml `;Get HTML from page (great for supporting people)"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET***********Get links and text on page*******************
WebScraping_Get_Links_on_Page:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Links := pwb.Document.Links ; collection of hyperlinks on the page
Loop `% Links.Length ; Loop through links
If ((Link := Links[A_Index-1].InnerText) != "") { ; if the link is not blank
   (OuterHTML := Links[A_Index-1].OuterHTML)  ; Grab outerHTML for each link
      Link:=StrReplace(Link,"``n")
      Link:=StrReplace(Link,"``r")
      Link:=StrReplace(Link,"``t")
      OuterHTML:=StrReplace(OuterHTML,"``n")
      OuterHTML:=StrReplace(OuterHTML,"``r")
      OuterHTML:=StrReplace(OuterHTML,"``t")

Msg .= A_Index-1 A_Space A_Space A_Space Link A_Tab OuterHTML "``r``n" ; add items to the msg list
}
gui, font, s12 cBlue q5, Book Antiqua
Gui,Add,Edit,w1000 h600 -Wrap, `%msg`%
gui,show
return
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;******************************
;***********Get MISC*******************
;******************************
;GET-Loop***********Get loop count and begin looping *******************
WebScraping_Get_Misc_Loop:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
loop `% (elements := pwb.document.getElementByID("results").childnodes).length ;stores elements for looping and gets length in same line
	MsgBox `% elements[a_index-1].InnerTEXT

		;***********second example*******************
loop `% (elements := pwb.document.getElementsByTagName("address")).length  ;Stores elements for loop and gets length in same line
      if (elements[A_index-1].getAttribute("data-test-id") = "prospect.address")
         Address := elements[A_index-1].innertext

		;******Third example with While loop***Note a_index-1 is in first row, not each individual one*
While(ele:=pwb.document.getElementsByClassName("")[a_index-1]){ ;store reference to element in ele While looping over elements
:=ele.getElementByID("").InnerTEXT ;ID
:=ele.getElementsByClassName("").item[0].InnerTEXT ;Class Name
:=ele.getElementsByName("").item[0].InnerText ;Name
:=ele.getElementsByTagName("").item[0].InnerTEXT ;TagName
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Get_Misc_Loop_over_Table:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
		;*********** loop over table*****Maestrith helped significantly with the listview portion*******
pwb := WBGet() ;connect to current IE window (Make sure WBGet function is in your library or this script)
Gui,DD:destroy
loop, `% Pwb.Document.All.Tags("TABLE").length ;get count of all tables on page
    Table_List.=A_index-1 "|" ;prep for dropdown list

gui,DD:add, dropdownlist,w200 r10 vTable_Nb gSubmit_All, `%Table_List`%
gui,DD:show
return

Submit_all:
Gui,DD:Submit
Gui,DD:destroy

		;***********now extract data*******************
Data:=[]
loop, `% Pwb.Document.All.Tags("TABLE")[Table_Nb].Rows.Length-1 {
	Row:=Pwb.Document.All.Tags("TABLE")[Table_Nb].Rows[A_Index-1]
	rows:="" ;clear out rows
	loop, `% row.cells.length{
		rows.= row.cells[A_Index-1].innerTEXT a_tab
	}
	if(A_Index=1)
		Headers:=RegExReplace(rows,"\t","|")
	else
		Data.Push(StrSplit(rows,"`t")) ;add rows to data object
}

Gui,Add,ListView,h900 w1200,`%Headers`%
for a,b in Data
	LV_Add("",b*) ;use variadic function to add columns
Loop,`% LV_GetCount("Column")
	LV_ModifyCol(A_Index,"AutoHDR") ;adjust column width based on data
gui, show
Table_List:=""
return
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET-Misc***********Get text of URL after question mark*******************
WebScraping_Get_Misc_Search:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Search:=pwb.document.location.Search `;gets substring of URL following question mark"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET-Misc***********Get Path*******************
WebScraping_Get_Misc_Path:
	Clipboard:="Path:=pwb.document.location.pathname `;returns pathname"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET-Misc***********Get Host*******************
WebScraping_Get_Misc_Host:
	Clipboard:="Host:=pwb.document.location.hostname `;returns host"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET-Misc***********Get After hash*******************
WebScraping_Get_Misc_Hash:
	Clipboard:="Hash:=pwb.document.location.hash `;retreives everyting from the # on"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET-Misc***********Get User Agent*******************
WebScraping_Get_Misc_UserAgent:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="User_Agent := pwb.document.parentWindow.navigator.userAgent `;Get User Agent"
	gosub Paste_and_Restore_Stored_Clipboard
return

;GET-Misc***********Get Protocol*******************
WebScraping_Get_Misc_Protocol:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="Protocol:=pwb.document.location.protocol `;retreives the protocol (http, https, etc)"
	gosub Paste_and_Restore_Stored_Clipboard
return

;******************************
;***********Set*******************
;******************************
;SET***********Set Unique ID*******************
WebScraping_Set_ID_Unique:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.all.XXXXXXX.Value :=""XXX"" `;Unique ID -no dashes"
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Set Unique ID with dashes in name *******************
WebScraping_Set_ID_Unique_Dashes:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.getElementByID(""XXX"").Value :=""XXX"" `;Unique ID-with dashes"
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Set Name (Need to set array value)*******************
WebScraping_Set_Name_Array:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.GetElementsByName(""XXXX"").item[0].Value :=""XXX"" `;Object Name- Set array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Set value using ClassName (set array Value) *******************
WebScraping_Set_ClassName_Array:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.getElementsByClassName(""classnameXXX"").item[0].Value :=""XXX"" `;Set Classname and Array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Set value using tag name (set array Value) *******************
WebScraping_Set_TagName:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.GetElementsByTagName(""tagnameXXX"").item[0].Value :=""XXX"" `;Set Tag name and Array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Set value of Dropdown *******************
WebScraping_Set_DropDowns:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
pwb.document.GetElementsByTagName("tagnameXXX").item[0].selectedIndex :=0 ;Set Tagname and Array value
pwb.document.GetElementsByTagName("tagnameXXX").item[0].Value :="XXX" ;Set Tagname and Array value
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Select check box *******************
WebScraping_Set_CheckBoxes:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
pwb.document.GetElementsByTagName("tagnameXXX").item[0].checked :=1 ;Set Tagname and Array value
pwb.document.GetElementsByTagName("tagnameXXX").item[0].checked :=0 ;Set Tagname and Array value
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET***********Set Attributes*******************
WebScraping_Set_Attributes:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.GetElementsByTagName(""Input"").item[0].SetAttribute(""size"") := 50 `;sets an attribute value for an element"
	gosub Paste_and_Restore_Stored_Clipboard
return

;SET-Misc***********Set User Agent *******************
WebScraping_Set_Misc_UserAgent:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.Navigate(""http://whatsmyuseragent.com"",0,0,0,""User-Agent: Mozilla/5.0 (Joe's Agent; Test) AppleWebKit/536.26 (KHTML, like Gecko)"") `;set UserAgent"
	gosub Paste_and_Restore_Stored_Clipboard
return

;******************************
;***********Trigger*******************
;******************************
;********************Mouse***********************************
WebScraping_Trigger_Mouse_Down:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
MouseDown:= pwb.document.createEvent("MouseEvent") ;Create Mouse Down Event
MouseDown.initMouseEvent("mousedown",true,false,,,,,,,,,,,,0) ;Initialize the Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return
WebScraping_Trigger_Mouse_Up:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
MouseUp  := pwb.document.createEvent("MouseEvent")  ;Mouse Up
MouseUp.initMouseEvent("mouseup",true,false,,,,,,,,,,,,,0) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Trigger_Mouse_Click:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
MouseClick  := pwb.document.createEvent("MouseEvent")  ;Mouse Click
MouseClick.initMouseEvent("click",true,false,,,,,,,,,,,,,0) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;********************Mouse over***********************************
WebScraping_Trigger_Mouse_MouseOver:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
MouseOver  := pwb.document.createEvent("MouseEvent")  ;Mouse Over
MouseOver.initMouseEvent("mouseover",true,false,,,,,,,,,,,,,0) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;********************Double click***********************************
WebScraping_Trigger_Mouse_DoubleClick:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
MouseDblClick  := pwb.document.createEvent("MouseEvent")  ;Mouse Double Click
MouseDblClick.initMouseEvent("dblclick",true,false,,,,,,,,,,,,,0) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;********************keyboard Down***********************************
WebScraping_Trigger_Keyboard_Down:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
KeyboardDown  := pwb.document.createEvent("KeyboardEvent")  ;Create Keydown event
KeyboardDown.initKeyboardEvent("keydown", true, true, pwb.document.defaultView, false, false, false, false, k, k) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;********************keyboard up***********************************
WebScraping_Trigger_Keyboard_Up:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
KeyboardUp  := pwb.document.createEvent("KeyboardEvent")  ;Create Keydown eventdsddsdsd
KeyboardUp.initKeyboardEvent("keyup", true, true, pwb.document.defaultView, false, false, false, false, k, k) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;********************keyboard Press***********************************
WebScraping_Trigger_KeyboardPress:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
KeyboardPress  := pwb.document.createEvent("KeyboardEvent")  ;Create Keydown eventdsddsdsd
KeyboardPress.initKeyboardEvent("keypress", true, true, pwb.document.defaultView, false, ele, false, false, k, k) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return


;********************Miscellaneous***********************************
;********************Cut***********************************
WebScraping_Trigger_Misc_Cut:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Cut := pwb.document.createEvent("ClipboardEvent")  ;Create KeyboardPress Event
Cut.initMouseEvent("cut",true,false,,,,,,,,,,,,0) ;Initialize the event

	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;********************Copy***********************************
WebScraping_Trigger_Misc_Copy:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;********************Paste***********************************
WebScraping_Trigger_Misc_Paste:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Paste := pwb.document.createEvent("ClipboardEvent")  ;Create KeyboardPress Event
Paste.initMouseEvent("cut",true,false,,,,,,,,,,,,0) ;Initialize the event

	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;********************Input***********************************
WebScraping_Trigger_Misc_Input:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Input := pwb.document.createEvent("Event")  ;Create Keydown eventdsddsdsd
Input.initEvent("input", true, true) ;Initialize Event


	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;********************Change***********************************
WebScraping_Trigger_Misc_Change:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Change := pwb.document.createEvent("Event")  ;Create Keydown eventdsddsdsd
Change.initEvent("change", true, true) ;Initialize Event

		;**Set your element here**

	)
	gosub Paste_and_Restore_Stored_Clipboard
return
;********************Focus***********************************
WebScraping_Trigger_Misc_Focus:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
Focus := pwb.document.createEvent("FocusEvent")  ;Create Keydown eventdsddsdsd
Focus.initEvent("focus", true, true) ;Initialize Event
		;**Set your element here**

	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;*********************Focus in**********************************
WebScraping_Trigger_Misc_Focus_In:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
FocusIn := pwb.document.createEvent("FocusEvent")  ;Create Keydown eventdsddsdsd
FocusIn.initEvent("focusin", true, true) ;Initialize Event
		;**Set your element here**

	)
	gosub Paste_and_Restore_Stored_Clipboard
return








WebScraping_Click_BY_ID:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.GetElementByID(""XXXbutton"").Click() `; 通过ID点击"
	gosub Paste_and_Restore_Stored_Clipboard
return


WebScraping_Click_BY_name:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.GetElementsByName(""XXX"").item[0].click() `; 通过name点击"
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Click_BY_classname:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.GetElementsByClassName(""XXX"").item[0].click() `; 通过classname点击"
	gosub Paste_and_Restore_Stored_Clipboard
return

;CLICK***********Click link*******************
WebScraping_Click_Click:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.all.tags(""tagnameXXX"")[8].Click() `; Acts like Clicking the link"
	gosub Paste_and_Restore_Stored_Clipboard
return

;CLICK***********click link with specific text*******************
WebScraping_Click_Click_Link_Specific_Text:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
ClickLink(pwb,Text:="text to click")

ClickLink(PXL,Text=""){
ComObjError(false)
Links := PXL.Document.Links
Loop `% Links.Length
   If (Links[A_Index-1].InnerText = Text ) { ; search for Text
      Links[A_Index-1].Click() ;click it when you find it
      break
   }
ComObjError(True)
}
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;CLICK***********Fire event after clicking*******************
WebScraping_Click_Fire_Event_Change:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.all.tags(""tagnameXXX"").item[8].fireEvent(""onchange"")  `; Sometimes needed in conjunction with changing value"
	gosub Paste_and_Restore_Stored_Clipboard
return

WebScraping_Click_Fire_Event_Click:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.all.tags(""tagnameXXX"").item[8].fireEvent(""onClick"")  `; Sometimes needed in conjunction with Click()"
	gosub Paste_and_Restore_Stored_Clipboard
return

;CLICK***********Focus in Input field*******************
WebScraping_Click_Focus:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="pwb.document.all.tags(""tagnameXXX"").item[8].focus() `; Acts like Clicking the link"
	gosub Paste_and_Restore_Stored_Clipboard
return
;*******************************************************











;******************************
;***********Advanced*******************
;******************************
;***********Frames- One level*******************
Advanced_Frame_One_level_Length:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames.length `; returns count of main frames"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_One_level_Name_or_ID_URL:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames(""XXX_Name_or_ID_XXXX"").document.location.href `; Use if one level & has Name or ID"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_One_level_Name_or_ID_Get_Text:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames(""XXX_Name_or_ID_XXXX"").document.all.tags(""BODY"").item[0].InnerText `;Use if One level & has Name or ID"
	gosub Paste_and_Restore_Stored_Clipboard
return


Advanced_Frame_One_level_NO_Name_or_ID_URL:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames[0].document.location.href `; Use if one level & does NOT have a Name or ID"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_One_level_NO_Name_or_ID_Get_Text:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames[0].document.all.tags(""BODY"").item[0].InnerText `;Use if One level & does NOT have a Name or ID"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_One_level_NO_Name_or_ID_Access_Denied:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
		;****** access denied means you need to query the object for the document first****
		;****** Read more here:  https://autohotkey.com/board/topic/91443-comie-error-0x80070005-access-is-denied-with-paypal/  *****
Frame_NB:=0 ;determine what frame you want (but note IWB2 learner tool may be wrong.  :(

pwb := WBGet()
frame:= ComObjActive(9,ComObjQuery(pwb.document.parentwindow.frames[Frame_NB],"{332C4427-26CB-11D0-B483-00C04FD90119}","{332C4427-26CB-11D0-B483-00C04FD90119}"),1).document.documentElement ;Get pointer to pointer similar to pwb.document.  ;querying the Comobject of the iframe's contentWindow one gets a pointer to its interface. This pointer needs to be wrapped with ComObjectActive()
SciTE_Output(frm.OuterHTML) ;Show all html from frame
MsgBox `% frame.GetElementsByTagName("Body")[0].innertext ;get Text from first Body tag on 4th frame
MsgBox `% frame.GetElementsByTagName("Body")[0].InnerHTML ;get InnerHTML from first body tag on 4th frame
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;******************************
;***********Frames- Multi Level*******************
;******************************
Advanced_Frame_Multi_level_Name_or_ID_URL:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames(""XXX_Name_or_ID_XXXX"")[0].document.location.href `; Use for multi-level & change array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_Multi_level_Name_or_ID_Get_Text:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames(""XXX_Name_or_ID_XXXX"")[0].document.all.tags(""BODY"")[0].InnerText `; Use for multi-level & change array value"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_Multi_level_NO_Name_or_ID_URL:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames[2][0].document.location.href `; Use for Multi-level & NO name or ID"
	gosub Paste_and_Restore_Stored_Clipboard
return

Advanced_Frame_Multi_level_NO_Name_or_ID_Get_Text:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard:="MsgBox % pwb.document.parentWindow.frames[2][0].document.all.tags(""BODY"").item[0].InnerText `; Use for Multi-level & NO name or ID"
	gosub Paste_and_Restore_Stored_Clipboard
return


;~ MsgBox % Pwb.document.parentWindow.frames("view")[0].document.location.href

;******************************
;***********Miscellaneous*******************
;******************************
;************Maximize IE WIndow******************
WebScraping_Misc_Maximize_IE_Window:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=WinMaximize, `% "ahk_id " pwb.HWND
	gosub Paste_and_Restore_Stored_Clipboard
return

;MISC***********Hide toolbars*******************
WebScraping_Misc_Hide_Toolbars:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
pwb.addressbar:=0 ;Hide Addressbar
pwb.Toolbar:=0    ;Hide Toolbar
pwb.Statusbar:=0  ;Hide Statusbar
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;MISC***********Show toolbars*******************
WebScraping_Misc_Show_Toolbars:
	Store:=ClipboardAll  ;****Store clipboard ****
	Clipboard=
	(  Join`r`n
pwb.addressbar:=1 ;Show Addressbar
pwb.Toolbar:=1    ;Show Toolbar
pwb.Statusbar:=1  ;Show Statusbar
	)
	gosub Paste_and_Restore_Stored_Clipboard
return

;******************************
;***********Advanced*******************
;***********to do*******************
;~ 1) example getting using ID first, then getTag name-  in one line
;~ 2) Example get pointer to ID, then refernce pointer later
;~ 4) Wait for exist
;***********Wait for exists*******************
;~ Wait_for_Exist:
;~ while Pwb.document.parentWindow.frames[1].document.all =  ;wait for DOM to exist
;~ Sleep, 100

;******************************
;***********Subroutines*******************
;******************************
;*******Store Clipboard- ****************
Store_Clipboard_Copy_Selected_Text:
	Store:=ClipboardAll  ;Store full version of Clipboard
	clipboard :="" ; Empty the clipboard
	Send, ^c  ;Depending on your OS and Admin level- you might want to check this
	ClipWait, 1
return

;***********Restore clipboard*******************
;~ Paste_and_Restore_Stored_Clipboard:  ;~ MsgBox % clipboard
	;~ Sleep, 50
	;~ Send, ^v ;Depending on your OS and Admin level- you might want to check this
	;~ Sleep, 50
	;~ Clipboard:=Store  ;Restore clipboard to original contents
;~ return

Helpful_Links:
	Gui, Helpful:Destroy
	;~ Gui, Help:Add, Text,x10 y10, This program allows computers that share a mutual folder to share the clipboard.
	;~ Gui, Help:Add, Text,x10 y+15, After launching the script on both computers FROM THE SAME DIRECTORY, Copy/paste as you would normally however use the Windows Key INSTEAD of the Control Key.
	;~ Gui, Help:Add, Text,x10 y+15, To Copy hold down the windows key and press C (Alternatively you can press Alt and F1)
	;~ Gui, Help:Add, Text,x10 y+15, To Paste hold down the windows key and press V (Alternatively you can press Alt and F2)
	Gui, Helpful:Font,CBlue Underline
	Gui, Helpful:Add,Text,y+5 GWebsite_autoahk, 智能热键网
	Gui, Helpful:Add,Text,y+10 GWebsite_autoweb, 网页自动化
	;******************************
	Gui, Helpful:Font,Bold cBlack Norm
	Gui, Helpful:Add,Text,y+20, 其他资源
	Gui, Helpful:Font,CBlue Underline
	Gui, Helpful:Add,Text,y+10 GWebsite_W3Schools, W3C Schools
	;~ Gui, Helpful:Add,Text,y+10 GWebsite_DottorO, DottorO
	;~ Gui, Helpful:Add,Text,y+10 GWebsite_iWebBrowswer2, iWeb2 Browser
	;~ Gui, Helpful:Add,Text,y+10 GWebsite_YouTube, YouTube Demos
	;~ Gui, Helpful:Add,Text,y+10 GWebsite_AHK_Tutorial, Webscraping tutorials

	hCurs:=DllCall("LoadCursor","UInt",NULL,"Int",32649,"UInt") ;IDC_HAND
	OnMessage(0x200, "MsgHandler")

	Gui, Helpful:Show,w150 , Helpful links
return

;***********About me*******************
About:
	Gui,About:Destroy
	Gui,About:Font,Bold
	Gui,About:Add,Text,x10 y10,网页自动化辅助编程工具  v1.0
	Gui,About:Font,Norm
	Gui,About:Add,Text,x10 y30,激活菜单:CTRL+鼠标左键

	;~ Gui,About:Font
	;~ Gui,About:Font,CBlue Underline
	;~ Gui,About:Add,Text,y+10 GWebsite_LinkedIN, Joe Glines on LinkedIN
	;~ hCurs:=DllCall("LoadCursor","UInt",NULL,"Int",32649,"UInt") ;IDC_HAND
	;~ OnMessage(0x200, "MsgHandler")
	;~ Gui,About:Font
	Gui,About:Show,, About
return

Website_autoahk:
	Run,https://www.autoahk.com
	gosub GuiClose
return

Website_autoweb:
	Run,https://www.autoahk.com/archives/12482
	gosub GuiClose
return

Website_W3Schools:
	Run, https://www.w3school.com.cn/htmldom/index.asp
	gosub GuiClose
return

Website_DottorO:
	Run, http://help.dottoro.com/ljhrmrfb.php
	gosub GuiClose
return

Website_LinkedIN:
	Run,http://www.linkedin.com/in/joeglines/
return

Website_iWebBrowswer2:
	Run, http://www.autoHotkey.com/board/topic/84258-iwb2-learner-iwebbrowser2/
return

Website_YouTube:
	Run, https://youtu.be/UfqumFBUrwI
return

Website_AHK_Tutorial:
	Run, http://the-automator.com/web-scraping-with-autoHotkey/
return

Exit:
	ExitApp
return

Reload:
	Reload
return

Edit:
	Edit
return

GuiClose:
	Gui,About:Destroy
	OnMessage(0x200,"")
	DllCall("DestroyCursor","Uint",hCur)
return