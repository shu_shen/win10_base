﻿;管理员运行
if not A_IsAdmin
{
   Run *RunAs "%A_ScriptFullPath%" ;当前脚本的全路径 D:\code\Git_WorkSpace\win10_base\AHK\vim-desktop-master\vim.ahk
   ExitApp
}
;高进程
Process Priority,,High
#NoEnv
#SingleInstance, Force
#MaxHotkeysPerInterval 200

CoordMode, Tooltip, Screen ; Place ToolTips at absolute screen coordinates.
CoordMode, Mouse, Screen
Coordmode, Menu, Window
SetControlDelay, -1 ;Sets the delay(milliseconds) that will occur after each control-modifying command.
SetKeyDelay, -1
;Detecthiddenwindows, on
FileEncoding, utf-8 ; 防止文件乱码
SendMode Input ;将Send的默认值SendEvent改成SendInput

Menu, Tray, Icon, %A_ScriptDir%\vimd.ico ;当前脚本所在目录的完整路径
Menu, Tray, NoStandard
Menu, Tray, Add, 热键 &K, <VimDConfig_Keymap>
Menu, Tray, Add, 插件 &P, <VimDConfig_Plugin>
Menu, Tray, Add, 配置 &C, <VimDConfig_EditConfig>
Menu, Tray, Add, ;添加一条分割线
Menu, Tray, Add, 禁用 &S, <Suspend>
Menu, Tray, Add, 重启 &R, <Reload>
Menu, Tray, Add, 退出 &X, <Exit>
Menu, Tray, Default, 热键 &K
Menu, Tray, Click, 1

VimdRun()

return
; 用户自定义配置
#Include *i %A_ScriptDir%\custom.ahk
#Include *i %A_ScriptDir%\01.win.ahk
#Include *i %A_ScriptDir%\02.ctrl.ahk
#Include *i %A_ScriptDir%\03.alt.ahk
#Include *i %A_ScriptDir%\04.`;.ahk
;#Include *i %A_ScriptDir%\05.capslock.ahk
;#Include *i %A_ScriptDir%\06.tab.ahk
; 自带配置,逐步研究
#Include %A_ScriptDir%\core\Main.ahk
#Include %A_ScriptDir%\core\class_vim.ahk
#Include %A_ScriptDir%\core\VimDConfig.ahk
#Include %A_ScriptDir%\lib\class_EasyINI.ahk
#Include %A_ScriptDir%\lib\Acc.ahk
#Include %A_ScriptDir%\lib\GDIP.ahk
#Include %A_ScriptDir%\lib\Logger.ahk
#Include %A_ScriptDir%\plugins\plugins.ahk ;指定包含的插件有哪些?

;test--ss
