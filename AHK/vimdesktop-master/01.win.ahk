﻿; 我发现windows很多组合键对我来说是没有用的
; 我只说有用的:win+X; win+L; win+D;
; win+V启动记忆剪切板,可是ditto可以做的更好,而且说实话,我目前对这个应用场景使用频度并不是很高.

;win+c和win+v替代ctrl+insert和 shift+insert
#c::
Send, {Ctrl Down}{Insert}{Ctrl Up}
return

#v::
Send, {Shift Down}{Insert}{Shift Up}
return

;win+n:最小化窗口;win+m:最大化窗口
#n::
Send, #{Down}
Send, {Down}
KeyWait, n
Send, #{Up}
return

#m::

return

;win+e:启动tc
;wuxiao???#e::run "D:\Program Files\TotalCMD64\TotalCMD64.exe"

;win+f:替代everything的调出
#f::
run D:\Program Files\TotalCMD64\Tools\Everything.exe
Return

;win+h

;win+s 复制并且搜索!!! 要是有可选项那就更棒了!//todo
#s::
clipboard :=
Send, {ctrl down}{insert}{ctrl up}
clipwait
if(1==Instr(clipboard,"http")){ ; 要做trim操作!
    run %clipboard%
}else if(RegExMatch(clipboard,"^[CDEFGH]:")){
    run explore %clipboard%
}else{
    run https://www.google.com/search?q=%Clipboard%
}
return

;win+i  gvim文字编辑配置
#i::
    tmpfile=%A_ScriptDir%\ahk_text_edit_in_vim.txt
    gvim=C:\tools\vim\vim82\gvim.exe
    WinGetTitle, active_title, A
    clipboard =
        ; 清空剪贴板
    send ^a
        ; 发送 Ctrl + A 选中全部文字
    send ^c
        ; 发送 Ctrl + C 复制
    clipwait
        ; 等待数据进入剪贴板
    FileDelete, %tmpfile%
    FileAppend, %clipboard%, %tmpfile%
    runwait, %gvim% "%tmpfile%" +
    fileread, text, %tmpfile%
    clipboard:=text
        ; 还原读取的数据到剪贴板
    winwait %active_title%
        ; 等待刚才获取文字的窗口激活
    send ^v
        ; 发送 Ctrl + V 粘贴
return

;鼠标按键模拟 win+h表示中键,win+j表示左键,win+k表示右键
#h::
return
#j::

return
#k::

return

;win+左键: snipaste截图
;once win up ,cortna 出现
#~LButton::
MouseGetPos, xpos, ypos

;方案一
;KeyWait LWin ;等待win is released
;sleep 200 ;win键释放以后还要等等才可以出现GUI界面
;if WinActive("ahk_exe SearchUI.exe")
    ;WinClose
;方案二(推荐)
WinWaitActive Cortana (小娜)
WinClose
WinWaitClose Cortana (小娜)
;方案三todo(win的响应)----

run D:\Program Files\Snipaste_2.3Beta_64bit_Green\Snipaste.exe snip --area %xpos% %ypos% 10 10
return

;~RButton & Win::MsgBox You pressed the left mouse button while holding down the right.

;win+p 来回切换pin&unpin
#p::
Winset, AlwaysOnTop, , A
return