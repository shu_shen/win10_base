﻿#include *i %A_ScriptDir%\plugins\BeyondCompare4\BeyondCompare4.ahk
#include *i %A_ScriptDir%\plugins\Everything\Everything.ahk
#include *i %A_ScriptDir%\plugins\Explorer\Explorer.ahk
#include *i %A_ScriptDir%\plugins\Foobar2000\Foobar2000.ahk
#include *i %A_ScriptDir%\plugins\General\General.ahk
#include *i %A_ScriptDir%\plugins\MicrosoftExcel\MicrosoftExcel.ahk
#include *i %A_ScriptDir%\plugins\MsEdge\MsEdge.ahk
#include *i %A_ScriptDir%\plugins\TCCompare\TCCompare.ahk
#include *i %A_ScriptDir%\plugins\TCDialog\TCDialog.ahk
#include *i %A_ScriptDir%\plugins\TotalCommander\TotalCommander.ahk
#include *i %A_ScriptDir%\plugins\WinMerge\WinMerge.ahk
/*
[ExtensionsTime]
BeyondCompare4=20200222024510
Everything=20200409162352
Explorer=20200407212818
Foobar2000=20200222024510
General=20200222024510
MicrosoftExcel=20200222024510
MsEdge=20200408095522
TCCompare=20200222024510
TCDialog=20200222024510
TotalCommander=20200409120316
WinMerge=20200222024510
*/
