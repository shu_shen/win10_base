; 插件名和目录名一致，插件要放到 plugins/MsEdge/MsEdge.ahk 位置。
; 放入插件后，重新运行 vimd 会自动启用插件。
; 标签名请添加 MsEdge_ 前缀，避免和其他插件冲突。

; 该标签名需要和插件名一致
MsEdge:
    ; 定义注释（可选）
    vim.SetAction("<MsEdge_NormalMode>", "进入normal模式")
    vim.SetAction("<MsEdge_InsertMode>", "进入insert模式")
    vim.SetAction("<MsEdge_NewTab>", "新建tab")
    vim.SetAction("<MsEdge_CloseTab>", "关闭tab")

    vim.setAction("<MsEdge_SwitchTabToLeft>","to left tab")

    ; 请务必调用 vim.SetWin 并保证 MsEdge 和文件名一致，以避免名称混乱影响使用
    ;vim.SetWin("MsEdge", "ahk_class名")
    ; 或：
    vim.SetWin("MsEdge", "Chrome_WidgetWin_1", "msedge.exe")
    ; 如果 class 和 exe 同时填写，以 exe 为准

    ; insert模式（如果无需 insert 模式，可去掉）
    vim.SetMode("insert", "MsEdge")
    tooltip insertmode

    vim.Map("<esc>", "<MsEdge_NormalMode>", "MsEdge")

    ; normal模式（必需）
    vim.SetMode("normal", "MsEdge")
    tooltip normalmode
    vim.Map("i", "<MsEdge_InsertMode>", "MsEdge")

    vim.Map("a", "<MsEdge_NewTab>", "MsEdge")
    vim.Map("b", "<MsEdge_CloseTab>", "MsEdge")

    vim.Map("hh", "<MsEdge_SwitchTabToLeft>", "MsEdge")
    ; 可选
    vim.BeforeActionDo("MsEdge_BeforeActionDo", "MsEdge")
return

; 对符合条件的控件使用insert模式，而不是normal模式
; 此段代码可以直接复制，但请修改AHK_CLASS的值和RegExMatch的第二个参数
MsEdge_BeforeActionDo()
{
    ControlGetFocus, ctrl, AHK_CLASS MsEdge
    ; MsgBox % ctrl
    if RegExMatch(ctrl, "Edit2")
        return true
    return false
}

<MsEdge_NormalMode>:
    vim.SetMode("normal", "MsEdge")
return

<MsEdge_InsertMode>:
    vim.SetMode("insert", "MsEdge")
return

<MsEdge_NewTab>:
    Send, ^n
return

<MsEdge_CloseTab>:
    Send, ^p
return

<MsEdge_SwitchTabToLeft>:
    Send, ^+{tab}
return