;我沙拉查词"打开独立词典窗口"快捷键就是alt+q
$!q::
clipboard := ""
Send {Ctrl Down}{Insert}{Ctrl Up}
ClipWait, 0.1
;tooltip %clipboard%
Send {LAlt Down}{q}
sleep 200
Send {LAlt Up}
Send {Shift Down}{Insert}{Shift Up}
Send {Enter}
return
; 指针移动
LAlt & k::
Send {Up}
return
LAlt & j::
Send {Down}
return
LAlt & h::
Send {Left}
return
LAlt & l::
Send {right}
return
; 光标移动
;RAlt & k::
;mousemove, 0,15,0,R
;return
;RAlt & j::
;mousemove, 0,-15,0,R
;return
;RAlt & h::
;mousemove, -15,0,0,R
;return
;RAlt & l::
;mousemove, 15,0,0,R
;return
